var config = {
    apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
    authDomain: "jyg-custom.firebaseapp.com",
    databaseURL: "https://jyg-custom.firebaseio.com",
    projectId: "jyg-custom",
    storageBucket: "jyg-custom.appspot.com",
    messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
cacheBlocked();
function cacheBlocked() {
	try{ localStorage.setItem("cacheBlocked",false);
	}catch(e) { alert("현재 사용중이신 브라우저의 설정이 쿠키 사용안함으로 설정되어 있어 정상적인 사이트 이용이 불가능합니다.\n설정에서 쿠키 '사용함'으로 변경 후 다시 시도해주시기 바랍니다.");}
}
var firebase_init = true;
function getFirebase_init() {return firebase_init;}
firebase.auth().onAuthStateChanged(function(custom) {
	firebase_init = false;
		var acustom = firebase.auth().currentUser;
		if(acustom) {
			init_cart_listener(acustom.uid,false);
		}else {
			alert("로그인이 필요한 화면입니다.");
			location.href = "index.php";
		}
});
var fireflag={json:false,customer:false};
var json; function get_json() {return json;} function set_json(val) {json=val;}
var customer; function get_customer() {return customer;} function set_customer(val) {customer=val;}
var datelist; function get_datelist() {return datelist;} function set_datelist(val) {datelist=val;}
var selecteddate; function get_selecteddate() {return selecteddate;} function set_selecteddate(val) {selecteddate=val;}
var goodkey; function get_goodkey() {return goodkey;} function set_goodkey(val) {goodkey=val;}

var phpjson = getmypage_json();
if(phpjson) {
	json = phpjson;
	fireflag.json=true;
}
var phpcustomer = getmypage_init();
var phpuid = getmypage_uid();
if(phpcustomer && phpuid) {
	customer = phpcustomer;
	fireflag.customer=true;
}

init_json_listener();
function init_json_listener() {
	firebase.database().ref('json').on('value', function(jsonsnap) {
		console.log("json refresh");
		json = jsonsnap.val();
		fireflag.json=true;
		init_index_goods();
		asyncsupport();
	});
	function asyncsupport() {
		if(fireflag.customer) {
			if(firebase.auth().currentUser) {
				init_cart_listener(firebase.auth().currentUser.uid,true);
			}
		}
	}
	function init_index_goods() {
		var goods_info = json.goods_info.goods_info;
		json.goods_list=[];
		json.goods_length = 0;
		for(var goodkey in goods_info) {
			if(goods_info[goodkey].standardindex) {
				json.goods_length++;
				json.goods_list[goods_info[goodkey].standardindex-1]=goods_info[goodkey];
				json.goods_list[goods_info[goodkey].standardindex-1].key = goodkey;
			}
		}
	}
}
function init_cart_listener(currentUID,asyncflag) {
	if(!asyncflag) {
		if(fireflag.customer) {
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();
				print_mypage();
			}
		}
		firebase.database().ref('customers/'+currentUID).on('value', function(customersnap) {
			customer = customersnap.val();
			fireflag.customer=true;
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();
				print_mypage();
			}
		});
	}else {
		init_cart_goods();
		print_cart_goods();
		print_cart_prices();
		print_mypage();
	}
	function init_cart_goods() {
		customer.calcprice={
			sum:0,
			point:0,
			shipping:2500,
			echo:0,
			total:0,
			net:0,
			count:0
		};
		
		init_available_dates();
		
		function init_available_dates() {
			datelist = {};
			for(var i in customer.carts) {
				if(i!=='current' && i!=='timestamp') {
					for(var j in json.goods_info.goods_info[i].dayoff) {
						if(json.goods_info.goods_info[i].dayoff[j]) {
							datelist[j] = true;
						}
					}
				}
			}
		}
		init_shipping_date();
		function init_shipping_date() {
			if(realtime_date(customer.carts)) {
				selecteddate = customer.carts.current;
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(customer.carts.current);
					firebase.database().ref('customers/'+currentUID+'/carts/current').set(selecteddate);
				}
			}
			else if(!selecteddate) {
				var startdate = new Date();
				startdate.setHours(startdate.getHours()+9);
				if(startdate.getUTCHours()>16) {startdate.setDate(startdate.getDate()+1);}
				selecteddate = getNextDate(startdate.toISOString());
			}else {
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(selecteddate);
				}
			}
			function realtime_date(targetCart) {
				var pastDate = new Date();
				pastDate.setDate(pastDate.getDate()+1);
				if(!firebase.auth().currentUser) {return false;}
				var targetUID = firebase.auth().currentUser.uid;
				if(!targetUID) { return false;}
				if(targetCart) {
					if(targetCart.current!=0) {
						if(targetCart.current.toString().indexOf('(')!=-1) {
							if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
								firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
								return false;
							}
							return true;
						}
					}
				}return false;
			}
			function getNextDate(target) {
				var returndate = new Date(target.substring(0,10));
				var findflag = false;
				for(var i=0;i<50;i++) {
					returndate.setDate(returndate.getDate()+1);
					if(!datelist[returndate.toISOString().substring(0,10)]) { findflag=true; break; }
				}
				if(!findflag) { return target; }
				return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
			}
			function getKorDay(target) {
				if(target===0) {return '일';}
				if(target===1) {return '월';}
				if(target===2) {return '화';}
				if(target===3) {return '수';}
				if(target===4) {return '목';}
				if(target===5) {return '금';}
				if(target===6) {return '토';}
				return '일';
			}
		}
	}
	function print_cart_goods() {
		var cartHTML = '';
		var itemCount = 0;
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					if(customer.carts[i][j]) {
						var option = Number(j.toString().substring(1))-1;
						var key = i;
						var count = customer.carts[i][j].count;
						var itemHTML = '<div class="cart_item">'+
										'<div class="citem-name"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						itemCount++;
						cartHTML+=itemHTML;
						customer.calcprice.sum+=json.goods_info.goods_info[key].standardprice*count;
						customer.calcprice.count+=count;
					}
				}
			}
		}
		document.getElementById('cart_list').innerHTML = cartHTML;
		function goodsName(target) {
			return json.goods_info.goods_info[target].standardname;
		}
		function goodsOption(target,option) {
			return json.goods_info.goods_info[target].options[option].name;
		}
		function goodsPrice(target,count) {
			return numberWithWon(json.goods_info.goods_info[target].standardprice*count);
		}
	}
	function print_cart_prices() {
		if(customer.calcprice.count<2) {customer.calcprice.echo = 0;}
		else if(customer.calcprice.count<7) {customer.calcprice.echo=(customer.calcprice.count-1)*500;}
		else {customer.calcprice.echo=2500;}
		customer.calcprice.total = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo;
		document.getElementById('cart_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('cart_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('cart_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('cart_total').innerHTML = numberWithWon(customer.calcprice.total);
		
		document.getElementById('cart_shippingdate').innerHTML = selecteddate;
		
		if(customer.calcprice.count==0) {
			document.getElementById('cart_empty').style.display="block";
		} else {
			document.getElementById('cart_empty').style.display="none";
		}
	}
	function print_mypage() {
		if(!customer) {return;}
		document.getElementById('mypage_welcome').innerHTML = customer.username+"님,<br>안녕하세요!";
		print_order();
		print_point();
		print_coupon();
		print_card();
		print_bank();
		print_code();
		print_address();
		print_cms();
		print_personal();
		function print_code() {
			if(document.getElementById('mypage_mycode') && document.getElementById('code-gene') && document.getElementById('code-copy')) {
				if(customer.usercode) {
					document.getElementById('mypage_mycode').textContent = customer.usercode;
					document.getElementById('code-gene').style.display = 'none';
					document.getElementById('code-copy').style.display = 'inline-block';
				}else {
					document.getElementById('mypage_mycode').textContent = '';
					document.getElementById('code-gene').style.display = 'inline-block';
					document.getElementById('code-copy').style.display = 'none';
				}
				if(customer.promocode) {
					document.getElementById('mypage_codepoint').value = customer.promocode.recommender.input_code;
					$('#mypage_codepoint').attr('disabled', true);
					document.getElementById('mypage_codeinput').style.display="none";
				}
				else {
					document.getElementById('mypage_codepoint').value = '';
					$('#mypage_codepoint').attr('disabled', false);
					document.getElementById('mypage_codeinput').style.display="block";
				}
			}
		}
		function print_order() {
			var headHTML;
			if(window.location.pathname == '/mypage.php')  {
				headHTML = '<thead><tr><td>주문번호</td><td>주문요약</td><td>주문총액</td><td>배송희망일</td><td>후기</td></tr></thead>';
			}
			else if(window.location.pathname == '/mypage_m.php') {
				headHTML = '<thead><tr><td>주문정보</td><td>후기</td></tr></thead>';
			}
			var bodyHTML = '<tbody>';
			var dCount = 0;
			var hCount = 0;
			var iArray = new Array();
			var hArray = new Array();
			var existOrder = false;
			
			if(window.location.pathname == '/mypage.php')  {
				makebodyHTML_web();
			}
			else if(window.location.pathname == '/mypage_m.php') {
				makebodyHTML_mobile();
			}
			
			for(var j=0; j<hCount; ++j) {
				if(customer.order_records[iArray[j]].histories[hArray[j]].status == 'ordered' ||
				  customer.order_records[iArray[j]].histories[hArray[j]].status == 'waiting' ||
				  customer.order_records[iArray[j]].histories[hArray[j]].status == 'shipping') {
					dCount++;
				}
			}
			if(document.getElementById('mypage_d') != null) document.getElementById('mypage_d').innerHTML = dCount;
			
			
			bodyHTML+='</tbody>';
			
			if(existOrder) {
				document.getElementById('mypage_order_table').innerHTML = headHTML+bodyHTML;
			}else {
			}
			
			function makebodyHTML_web() {
				for(var i in customer.order_records) {
					if(i!='current') {
						if(customer.order_records[i]) {
							existOrder = true;
							var orderCode = '<td class="table_select" onClick="open_order_detail('+i+')">'+getCode(i,customer.order_records[i])+'</td>';
							var orderName = '<td>'+getGood(i,customer.order_records[i])+'</td>';
							var orderPrice = '<td>'+numberWithWon(customer.order_records[i].totalPrice)+'</td>';
							var orderStatus = '<td>'+customer.order_records[i].informations.shippingDate+'</td>';
							var orderReview = '<td>'+'</td>';
							if(customer.order_records[i].reviews) {
								orderReview = '<td style="color:#CCCCCC">'+'작성완료'+'</td>';
							}else if((new Date() >= new Date(customer.order_records[i].informations.shippingDate.substring(0,10).replace(/-/g,'/'))) && typeof customer.order_records[i].informations.shippingCode !== "undefined"){
								orderReview = '<td class="table_select" onClick="open_order_detail('+i+')">'+'작성가능'+'</td>';
							}
							bodyHTML='<tr>'+orderCode+orderName+orderPrice+orderStatus+orderReview+'</tr>'+bodyHTML;
						}
						iArray.push(i);
						hArray.push(customer.order_records[i].histories.length-1);
						hCount++;

					}
				}
			}
			function makebodyHTML_mobile() {
				for(var i in customer.order_records) {
					if(i!='current') {
						if(customer.order_records[i]) {
							existOrder = true;
							/**
							var orderCode = '<td class="table_select" onClick="open_order_detail('+i+')">'+getCode(i,customer.order_records[i])+'</td>';
							var orderName = '<td>'+getGood(i,customer.order_records[i])+'</td>';
							var orderPrice = '<td>'+numberWithWon(customer.order_records[i].totalPrice)+'</td>';
							var orderStatus = '<td>'+customer.order_records[i].informations.shippingDate+'</td>';**/
							
							var orderMobile = '<td class="table_order_mobile">'+
								'<a class="table_select" onClick="open_order_detail('+i+')">'+getCode(i,customer.order_records[i])+'</a>'+
								'<span style="float:right">'+numberWithWon(customer.order_records[i].totalPrice)+'</span>'+'<br>'+
								'<span onClick="open_order_detail('+i+')">'+getGood(i,customer.order_records[i])+'</span>'+'<br>'+
								'<span style="margin-top:4px; display:block;">'+'도착희망일: '+customer.order_records[i].informations.shippingDate+'</span>'+
								'</td>';
							
							var orderReview = '<td>'+'</td>';
							if(customer.order_records[i].reviews) {
								orderReview = '<td style="color:#CCCCCC">'+'작성완료'+'</td>';
							}else if((new Date() >= new Date(customer.order_records[i].informations.shippingDate.substring(0,10).replace(/-/g,'/'))) && typeof customer.order_records[i].informations.shippingCode !== "undefined"){
								orderReview = '<td class="table_select" onClick="open_order_detail('+i+')">'+'작성가능'+'</td>';
							}
							bodyHTML='<tr>'+orderMobile+orderReview+'</tr>'+bodyHTML;
						}
						iArray.push(i);
						hArray.push(customer.order_records[i].histories.length-1);
						hCount++;

					}
				}
			}
			function getCode(index,target) {
				var returncode='';
				if(index<10) {returncode='00'+index;}
				else if(index<100) {returncode='0'+index;}
				else {returncode=index;}
				if(target.histories) { returncode += '-'+target.histories[0].timestamp.substring(2,10); }
				return returncode;
			}
			function getStatus() {
			}
			function getGood(index,target) {
				var returnGood='';
				var goodsCount=0;
				if(target.goods) {
					for(var i in target.goods) { goodsCount++; }
					returnGood = goodsName(target.goods[0].productID);
				}
				if(goodsCount==0) { returnGood = '상품정보를 불러올 수 없습니다.'; }
				else if(goodsCount==1) { returnGood += ' 1개'; }
				else { returnGood += ' 포함 (총 '+goodsCount+'개)'; }
				return returnGood;
				function goodsName(key) {
					if(json.goods_info.goods_info[key].standardname) {
						return json.goods_info.goods_info[key].standardname;
					}else {
						return json.goods_info.goods_info[key].name;
					}
				}
			}
		}
		function print_point() {
			var headHTML;
			if(window.location.pathname == '/mypage.php')  {
				headHTML = '<thead><tr><td>날짜</td><td>내역</td><td>지급금액</td><td>사용금액</td></tr></thead>';
			}
			else if(window.location.pathname == '/mypage_m.php') {
				headHTML = '<thead><tr><td>내역</td><td style="min-width: 55px;">지급액</td><td style="min-width: 55px;">사용액</td></tr></thead>';
			}
			var bodyHTML = '<tbody>';
			var existPoint = false;
			
			if(window.location.pathname == '/mypage.php')  {
				makebodyHTML_web();
			}
			else if(window.location.pathname == '/mypage_m.php') {
				makebodyHTML_mobile();
			}
			
			bodyHTML+='</tbody>';
			if(existPoint) {
				document.getElementById('mypage_point_table').innerHTML = headHTML+bodyHTML;
			}else {
			}
			document.getElementById('mypage_point_own').innerHTML = customer.rewards.points.current.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			if(document.getElementById('mypage_p') != null) document.getElementById('mypage_p').innerHTML = customer.rewards.points.current.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
			function makebodyHTML_web() {
				for(var i in customer.rewards.points_history) {
					if(customer.rewards.points_history[i]) {
						if(customer.rewards.points_history[i].changed!=0) {
							existPoint = true;
							var pointDate = '<td>'+customer.rewards.points_history[i].timestamp.substring(0,10)+'</td>';
							var pointText = '<td>'+customer.rewards.points_history[i].title+'</td>';
							var pointPlus = '<td class="table_pluspoint">'+calculatePlus(customer.rewards.points_history[i].changed)+'</td>';
							var pointMinus = '<td class="table_minuspoint">'+calculateMinus(customer.rewards.points_history[i].changed)+'</td>';
							bodyHTML='<tr>'+pointDate+pointText+pointPlus+pointMinus+'</tr>'+bodyHTML;
						}
					}
				}
			}
			function makebodyHTML_mobile() {
				for(var i in customer.rewards.points_history) {
					if(customer.rewards.points_history[i]) {
						if(customer.rewards.points_history[i].changed!=0) {
							existPoint = true;
							var pointDate = '<td style="text-align:left; color:#777">'+customer.rewards.points_history[i].timestamp.substring(0,10)+'<br>';
							var pointText = '<span style="color:#333">'+customer.rewards.points_history[i].title+'</span>'+'</td>';
							var pointPlus = '<td class="table_pluspoint">'+calculatePlus(customer.rewards.points_history[i].changed)+'</td>';
							var pointMinus = '<td class="table_minuspoint">'+calculateMinus(customer.rewards.points_history[i].changed)+'</td>';
							bodyHTML='<tr>'+pointDate+pointText+pointPlus+pointMinus+'</tr>'+bodyHTML;
						}
					}
				}
			}
			function calculatePlus(price) {
				price = price * -1;
				if(Number(price)<0) {return '';}
				if(window.location.pathname == '/mypage.php')
					return numberWithWon(Number(price));
				else if(window.location.pathname == '/mypage_m.php')
					return Number(price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			function calculateMinus(price) {
				price = price * -1;
				if(Number(price)>0) {return '';}
				if(window.location.pathname == '/mypage.php')
					return numberWithWon(Number(price));
				else if(window.location.pathname == '/mypage_m.php')
					return Number(price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
		}
		function print_coupon() {
			if(!customer.coupons) { return; }
			if(!customer.coupons.get) { return; }
			var headHTML;
			if(window.location.pathname == '/mypage.php')  {
				headHTML = '<thead><tr><td>발급일</td><td>상세</td><td>사용기한</td><td>상태</td></tr></thead>';
			}
			else if(window.location.pathname == '/mypage_m.php') {
				headHTML = '<thead><tr><td>쿠폰정보</td><td>상태</td></tr></thead>';
			}
			var bodyHTML = '<tbody>';
			var existCoupon = false;
			
			if(window.location.pathname == '/mypage.php')  {
				makebodyHTML_web();
			}
			else if(window.location.pathname == '/mypage_m.php') {
				makebodyHTML_mobile();
			}
			
			bodyHTML+='</tbody>';
			if(existCoupon) {
				document.getElementById('mypage_coupon_table').innerHTML = headHTML+bodyHTML;
			}else {
			}
			
			function makebodyHTML_web() {
				for(var i in customer.coupons.get) {
					existCoupon = true;
					var couponDate = '<td class="table_couponDate">'+customer.coupons.get[i].info.publishdate.substring(0,10)+'</td>';
					var couponName = '<td class="table_couponName">'+customer.coupons.get[i].info.name+'<br>'+customer.coupons.get[i].info.description+'</td>';
					var duedate = customer.coupons.get[i].due.use_end.substring(0,4)+'-'+
							customer.coupons.get[i].due.use_end.substring(4,6)+'-'+
							customer.coupons.get[i].due.use_end.substring(6,8);
					var duehour = customer.coupons.get[i].due.use_end.substring(9,11)+'시 ';
					var dueminute = customer.coupons.get[i].due.use_end.substring(11,13)+'분';
					var couponDue = '<td class="table_couponDate">'+duedate+'<br>'+duehour+dueminute+'</td>';
					var couponStatus='';
					if(customer.coupons.get[i].used) {
						var usedDate = customer.coupons.get[i].used;
						couponStatus='<td class="table_couponStatus_done">'+usedDate.substring(0,10)+'<br>'+'사용완료'+'</td>';
					}
					else {
						couponStatus='<td class="table_couponStatus">'+'사용가능'+'</td>';
						var currentEventTime = geteventTime();
						if(customer.coupons.get[i].due.use_end<currentEventTime) {
							couponStatus='<td class="table_couponStatus">'+'만료'+'</td>';
						}
					}
					bodyHTML='<tr>'+couponDate+couponName+couponDue+couponStatus+'</tr>'+bodyHTML;
				}
			}
			function makebodyHTML_mobile() {
				for(var i in customer.coupons.get) {
					existCoupon = true;
					var couponDate = '<td class="table_couponInfo">';
					var couponName = customer.coupons.get[i].info.name+'<br>'+customer.coupons.get[i].info.description+'<br>';
					var duedate = customer.coupons.get[i].due.use_end.substring(0,4)+'-'+
							customer.coupons.get[i].due.use_end.substring(4,6)+'-'+
							customer.coupons.get[i].due.use_end.substring(6,8);
					var duehour = customer.coupons.get[i].due.use_end.substring(9,11)+'시 ';
					var dueminute = customer.coupons.get[i].due.use_end.substring(11,13)+'분';
					var usedDate = '';
					var couponStatus='';
					if(customer.coupons.get[i].used) {
						usedDate = '<span class="table_couponUsedDate">'+'사용: '+customer.coupons.get[i].used.substring(0,10)+'</span>';
						couponStatus='<td class="table_couponStatus_done">'+'완료'+'</td>';
					}
					else {
						couponStatus='<td class="table_couponStatus">'+'가능'+'</td>';
						var currentEventTime = geteventTime();
						if(customer.coupons.get[i].due.use_end<currentEventTime) {
							couponStatus='<td class="table_couponStatus">'+'만료'+'</td>';
						}
					}
					var couponDue = '<span class="table_couponDate">'+'기한: '+duedate+' '+duehour+dueminute+'</span>'+
						usedDate+'</td>';
					bodyHTML='<tr>'+couponDate+couponName+couponDue+couponStatus+'</tr>'+bodyHTML;
				}
			}
		}
		function print_card() {
			var headHTML = '<thead><tr><td>등록일자</td><td>별명</td><td>변경</td></tr></thead>';
			var bodyHTML = '<tbody>';
			var existCard = false;
			for(var i in customer.cards) {
				if(i!='current') {
					if(customer.cards[i]) {
						existCard = true;
						var cardDate = customer.cards[i].AuthDate;
						if(!cardDate) {
							cardDate = customer.cards[i].tran_date.toString().substring(0,8);
						}
						var cardDate = '<td>'+cardDate+'</td>';
						var cardName = '<td>'+customer.cards[i].res_msg+'</td>';
						var cardRemove = '<td>'+'<a style="cursor:pointer; color: black; text-decoration: underline;" onClick="remove_card('+i+')">삭제하기</a>'+'</td>';
						bodyHTML='<tr>'+cardDate+cardName+cardRemove+'</tr>'+bodyHTML;
					}
				}
			}
			
			bodyHTML+='</tbody>';
			if(existCard) {
				document.getElementById('mypage_card_table').innerHTML = headHTML+bodyHTML;
			}else {
				document.getElementById('mypage_card_table').innerHTML = '<thead><tr><td>카드없음</td></tr></thead><tbody><tr><td>등록된 카드가 없습니다.</td></tr></tbody>'
			}
		}
		function print_bank() {
			if(customer.bank) {
				document.getElementById('mypage_bankname').value = customer.bank.name;
				document.getElementById('mypage_banknumber').value = customer.bank.number;
			}
		}
		function print_address() {
			if(customer.address) {
				document.getElementById('mypage_addressname').value = customer.address.name;
				document.getElementById('mypage_addressphone').value = customer.address.phone;
				document.getElementById('mypage_addresspostcode').value = customer.address.postcode;
				document.getElementById('mypage_addressaddress').value = customer.address.address;
				document.getElementById('mypage_addressroom').value = customer.address.room;
			}else {}
		}
		function print_cms() {
			if(customer.cms && document.getElementById('mypage_cms_payerName')) {
				document.getElementById('mypage_cms_payerName').value = customer.cms.name;
				document.getElementById('mypage_cms_payerNumber').value = '******';
				document.getElementById('mypage_cms_phone').value = customer.cms.phone;
				document.getElementById('mypage_cms_bankname').value = customer.cms.bank;
				document.getElementById('mypage_cms_paymentNumber').value = customer.cms.code;
			}
		}
		function print_personal() {}
	}
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}

function open_order_detail(target) {
	window.scrollTo(0,0);
	uploadReview(target);
	if(customer && json) {
		if(customer.order_records[target]) {
			print_orderdetail();
			open_mypage('mypage_detail');
		}
	}
	function print_orderdetail() {
		var detail = customer.order_records[target];
		print_address();
		print_payment();
		print_goods();
		function print_address() {
			document.getElementById('record_shippingdate').innerHTML = detail.informations.shippingDate;
			var record_status = '<a href="http://service.epost.go.kr/trace.RetrieveRegiPrclDeliv.postal?sid1='+detail.informations.shippingCode+'" target="_blank">'+detail.informations.shippingCode+'</a>';
			if(!detail.informations.shippingCode) {
				record_status = '송장번호 미발급';
			}
			document.getElementById('record_status').innerHTML = record_status;
			document.getElementById('record_name').innerHTML = detail.informations.name;
			document.getElementById('record_phone').innerHTML = detail.informations.phone;
			if(window.location.pathname == '/mypage.php') {
				document.getElementById('record_addresssum').innerHTML = '('+detail.informations.postcode+') '+
					detail.informations.address+' '+detail.informations.room;
			}
			else if(window.location.pathname == '/mypage_m.php') {
				var addrArray = (detail.informations.address).split('(');
				if(addrArray[1] !== null && addrArray[1] !== undefined) {
					document.getElementById('record_addresssum').innerHTML = '('+detail.informations.postcode+')<br>'+
						addrArray[0]+'<br>('+addrArray[1]+'<br>'+detail.informations.room;
				}
				else {
					document.getElementById('record_addresssum').innerHTML = '('+detail.informations.postcode+')<br>'+
						addrArray[0]+'<br>'+detail.informations.room;
				}
			}
		}
		function print_payment() {
			var directTotal=0;
			var directShippingFree=0;
			var noticetext='감사합니다.';
			var isWeight = getWeight();
			var addtext="";
			if(!isWeight) {
				addtext="약 ";
			}
			document.getElementById('record_t_discount_box').style.display = 'none';
			document.getElementById('recode_refund_box').style.display = 'none';
			document.getElementById('recode_deposit_box').style.display = 'none';
			if(!detail.payment.calprice) {
				document.getElementById('record_sum').innerHTML = NW(directTotal);
				document.getElementById('record_direct').innerHTML = NW(0);
				document.getElementById('record_point').innerHTML = NW(detail.informations.pointUsed);
				document.getElementById('record_shipping').innerHTML = NW(directShippingFree);
				document.getElementById('record_echo').innerHTML = NW(0);
				if(document.getElementById('record_shipping_sum')) {
					document.getElementById('record_t_method').innerHTML = '정육각페이';
					document.getElementById('record_t_sum').innerHTML = NW(directTotal);
					document.getElementById('record_t_direct').innerHTML = NW(0);
					document.getElementById('record_t_point').innerHTML = NW(detail.informations.pointUsed);
					document.getElementById('record_t_shipping').innerHTML = NW(directShippingFree);
					document.getElementById('record_t_echo').innerHTML = NW(0);
					document.getElementById('record_shipping_sum').innerHTML = NW(directShippingFree);
					document.getElementById('record_t_net').innerHTML = NW(detail.totalPrice);
				}
				document.getElementById('record_net').innerHTML = NW(detail.totalPrice);
			}
			else {
				if(detail.payment.prefix=="batch") {
					document.getElementById('record_sum').innerHTML = addtext+NW(detail.payment.calprice.sum);
					document.getElementById('record_direct').innerHTML = NW(0);
					document.getElementById('record_point').innerHTML = NW(detail.payment.calprice.point);
					document.getElementById('record_shipping').innerHTML = NW(detail.payment.calprice.shipping);
					document.getElementById('record_echo').innerHTML = NW(-1*detail.payment.calprice.echo);
					if(document.getElementById('record_shipping_sum')) {
						document.getElementById('record_t_method').innerHTML = '신용카드(신선페이)';
						document.getElementById('record_t_sum').innerHTML = addtext+NW(detail.payment.calprice.sum);
						document.getElementById('record_t_direct').innerHTML = NW(0);
						document.getElementById('record_t_point').innerHTML = NW(detail.payment.calprice.point);
						document.getElementById('record_t_shipping').innerHTML = NW(detail.payment.calprice.shipping);
						document.getElementById('record_t_echo').innerHTML = NW(-1*detail.payment.calprice.echo);
						document.getElementById('record_shipping_sum').innerHTML = NW(detail.payment.calprice.shipping - detail.payment.calprice.echo);
						//document.getElementById('record_t_net').innerHTML = addtext+NW(detail.payment.calprice.net);
						document.getElementById('record_t_net').innerHTML = addtext+NW(detail.totalPrice);
					}
					document.getElementById('record_net').innerHTML = NW(detail.payment.calprice.net);
					if(!isWeight) {
						noticetext='상품생산 및 무게측정을 기다리고 있습니다.<br>가격이 확정되면 등록하신 카드로 결제됩니다.';
					}
					
					if(detail.payment.totalPrice!=detail.payment.price) {
						console.log(Number(detail.payment.price)-Number(detail.payment.totalPrice),detail.payment.price,detail.payment.totalPrice);
					}
				}else if(detail.payment.prefix=="cms") {
					document.getElementById('record_sum').innerHTML = addtext+NW(detail.payment.calprice.sum);
					document.getElementById('record_direct').innerHTML = NW(0);
					document.getElementById('record_point').innerHTML = NW(detail.payment.calprice.point);
					document.getElementById('record_shipping').innerHTML = NW(detail.payment.calprice.shipping);
					document.getElementById('record_echo').innerHTML = NW(-1*detail.payment.calprice.echo);
					if(document.getElementById('record_shipping_sum')) {
						document.getElementById('record_t_method').innerHTML = '계좌자동이체(신선페이)';
						document.getElementById('record_t_sum').innerHTML = addtext+NW(detail.payment.calprice.sum);
						document.getElementById('record_t_direct').innerHTML = NW(0);
						document.getElementById('record_t_point').innerHTML = NW(detail.payment.calprice.point);
						document.getElementById('record_t_shipping').innerHTML = NW(detail.payment.calprice.shipping);
						document.getElementById('record_t_echo').innerHTML = NW(-1*detail.payment.calprice.echo);
						document.getElementById('record_shipping_sum').innerHTML = NW(detail.payment.calprice.shipping - detail.payment.calprice.echo);
						document.getElementById('record_t_net').innerHTML = addtext+NW(detail.payment.calprice.net);
					}
					document.getElementById('record_net').innerHTML = NW(detail.payment.calprice.net);
					if(!isWeight) {
						noticetext='상품생산 및 무게측정을 기다리고 있습니다.<br>가격이 확정되면 등록하신 계좌에서 결제됩니다.';
					}
				}else {
					document.getElementById('record_sum').innerHTML = addtext+NW(detail.payment.calprice.sum);
					document.getElementById('record_direct').innerHTML = NW(detail.payment.calprice.direct);
					document.getElementById('record_point').innerHTML = NW(detail.payment.calprice.point);
					document.getElementById('record_shipping').innerHTML = NW(detail.payment.calprice.shipping);
					document.getElementById('record_echo').innerHTML = NW(-1*detail.payment.calprice.echo);
					if(document.getElementById('record_shipping_sum')) {
						document.getElementById('record_t_method').innerHTML = '무통장입금';
						document.getElementById('record_t_sum').innerHTML = addtext+NW(detail.payment.calprice.sum);
						document.getElementById('record_t_direct').innerHTML = NW(detail.payment.calprice.direct);
						document.getElementById('record_t_point').innerHTML = NW(detail.payment.calprice.point);
						document.getElementById('record_t_shipping').innerHTML = NW(detail.payment.calprice.shipping);
						document.getElementById('record_t_echo').innerHTML = NW(-1*detail.payment.calprice.echo);
						document.getElementById('record_shipping_sum').innerHTML = NW(detail.payment.calprice.shipping - detail.payment.calprice.echo);
						document.getElementById('record_t_net').innerHTML = addtext+NW(detail.payment.calprice.net);
					}
					document.getElementById('record_net').innerHTML = NW(detail.payment.calprice.net);
					if(detail.payment.calprice.refund){
						//document.getElementById('record_refund').innerHTML = '환급 '+NW(detail.payment.calprice.refund);
						//document.getElementById('recode_refund_box').style.display = 'block';
					} else {
						//document.getElementById('record_refund').innerHTML='';
					}
					if(!detail.payment.calprice.deposit && !detail.payment.price) {
						noticetext='무통장 입금대기중입니다.<br>입금계좌: 기업은행 02-1800-0658';
						//document.getElementById('record_deposit').innerHTML='';
					}else {
						//document.getElementById('record_direct').innerHTML="";
						//document.getElementById('record_deposit').innerHTML=NW(detail.payment.price);
						//document.getElementById('recode_deposit_box').style.display = 'block';
						if(!isWeight) {
							noticetext='상품생산 및 무게측정을 기다리고 있습니다.<br>가격이 확정되면 차액이 환급됩니다.';
						}
					}
				}
			}
			
			if(document.getElementById('write_review') != null ) {
				document.getElementById('write_review').outerHTML = '';
			}
			if(document.getElementById('view_review') != null ) {
				document.getElementById('view_review').outerHTML = '';
			}
			if((new Date() >= new Date(detail.informations.shippingDate.substring(0,10).replace(/-/g,'/'))) && typeof detail.informations.shippingCode !== "undefined"){
				if(typeof detail.reviews == "undefined") {
					if(document.getElementById('write_review_box') && window.location.pathname == '/mypage.php') {
						document.getElementById('write_review_box').innerHTML = '상품정보<button class="record_reviewwrite_btn" type="button" id="write_review" data-toggle="modal" data-target="#myModal">후기 작성하기</button>';
					}else if(document.getElementById('write_review_box')) {
						document.getElementById('write_review_box').innerHTML = '<button class="record_reviewwrite_btn" type="button" id="write_review" data-toggle="modal" data-target="#myModal">후기작성</button>';
					}
					//document.querySelector('.record_payment').insertAdjacentHTML('beforeend','<button style="display:none" type="button" id="write_review" data-toggle="modal" data-target="#myModal">후기 작성</button>');
//					document.querySelector('.record_payment').innerHTML += '<button type="button" id="write_review" data-toggle="modal" data-target="#myModal">후기 작성</button>';
				} else {
					if(document.getElementById('write_review_box') && window.location.pathname == '/mypage.php') {
						document.getElementById('write_review_box').innerHTML = '상품정보<a class="record_reviewwrite_btn" href="review.php">후기보기</a>';
					}else if(document.getElementById('write_review_box')) {
						document.getElementById('write_review_box').innerHTML = '<a class="record_reviewwrite_btn" href="review.php">후기보기</a>';
					}
					//document.querySelector('.record_payment').insertAdjacentHTML('beforeend','<a href="review.php" id="view_review">후기 보기</a>');
//					document.querySelector('.record_payment').innerHTML += '<a href="review.php" id="view_review">후기 보기</a>';
				}
			}
			
			if(document.getElementById('record_notice_box')) {
				if(noticetext=='감사합니다.') {
					document.getElementById('record_notice_box').style.backgroundColor='#FFFFFF';
					document.getElementById('record_notice_box').style.height='0px';
					document.getElementById('record_notice').innerHTML = '';
					//document.getElementById('record_notice_box').style.display='none';
				}else {
					document.getElementById('record_notice_box').style.backgroundColor='#CCCCCC';
					document.getElementById('record_notice_box').style.height='60px';
					document.getElementById('record_notice').innerHTML = noticetext;
				}
			}
			
			if(document.getElementById('write_review') != null ) {
				document.getElementById('write_review').onclick = function() {
					document.getElementById('dropdownMenu1').innerHTML = '카테고리 <span class="caret"></span>';
					document.getElementById('review-title').value = '';
					document.getElementById('review-content').value = '';
					document.getElementById('fileButton').value = '';
					document.getElementById('source_image').src = '';
					document.getElementsByClassName('upload-name')[0].value = '파일선택';
				}
			}
			function getWeight() {
				var returnflag = true;
				var countflag=0;
				for(var i in detail.goods) {
					if(detail.goods[i]) {
						if(!detail.goods[i].productSerial) { returnflag = false; }
						directTotal = directTotal + Number(detail.goods[i].productPrice);
						if(detail.goods[i].productID.indexOf('even')!=-1) {countflag++;}
						countflag++;
					}
				}
				if(countflag<2) {directShippingFree=2500;}
				return returnflag;
			}
			function NW(x) {
				return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
			}
		}
		function print_goods() {
			var headHTML = '<thead><tr><td>상품명</td><td>옵션</td><td>수량</td><td>무게</td><td>가격</td></tr></thead>';
			var bodyHTML = '<tbody>';
			var existOrder = false;
			var parsedetail = applyCouponToDetail();
			if(detail.goods) {
				if(window.location.pathname == '/mypage.php') {
					headHTML = '<thead><tr><td>상품명</td><td>옵션</td><td>수량</td><td>무게</td><td>가격</td></tr></thead>';
					mypage_pc(parsedetail);
				}
				else {
					headHTML = '<thead><tr><td>상품정보</td><td>수량</td><td>무게/가격</td></tr></thead>';
					mypage_mobile(parsedetail);
				}
			}
			
			bodyHTML+='</tbody>';
			if(existOrder) {
				document.getElementById('mypage_detail_table').innerHTML = headHTML+bodyHTML;
			}
			function applyCouponToDetail() {
				var returndetail = JSON.parse(JSON.stringify(detail));
				if(returndetail.coupon) {
					//console.log(customer.coupons.get[returndetail.coupon.code]);
					var targetCoupon = customer.coupons.get[returndetail.coupon.code];
					if(targetCoupon.reward.type=="freeGoods") {
						for(var i in returndetail.goods) {
							if(returndetail.goods[i].productID==targetCoupon.reward.freeGoods.pid) {
								returndetail.goods[i].freeGoods = true;
								break;
							}
						}
					}
				}
				console.log(returndetail);
				return returndetail;
			}
			function mypage_pc(mydetail) {
				for(var i in mydetail.goods) {
					if(i!='current') {
						if(mydetail.goods[i]) {
							existOrder = true;
							
							var printWeight=mydetail.goods[i].productWeight;
							var printPrice=numberWithWon(mydetail.goods[i].productPrice);
							if(mydetail.goods[i].productSerial) {
								if(mydetail.goods[i].productPrice==-2 || mydetail.goods[i].productPrice=="-2") {
									printWeight=goodsStandardWeight(mydetail.goods[i].productID);
									printPrice=goodsStandardPrice(mydetail.goods[i].productID);
								}else {
									printWeight=numberWithGram(mydetail.goods[i].productWeight);
									printPrice=numberWithWon(mydetail.goods[i].productPrice);
								}
							}else {
								printWeight=goodsStandardWeight(mydetail.goods[i].productID);
								printPrice=goodsStandardPrice(mydetail.goods[i].productID);
							}
							if(mydetail.goods[i].freeGoods) {
								printPrice='무료쿠폰 적용';
							}
							
							var detailName = '<td>'+goodsName(mydetail.goods[i].productID)+'</td>';
							var detailOption = '<td>'+goodsOption(mydetail.goods[i].productID,detail.goods[i].productOption)+'</td>';
							var detailCount='<td>1</td>';
							var detailWeight = '<td>'+printWeight+'</td>';
							var detailPrice = '<td>'+printPrice+'</td>';
							bodyHTML='<tr>'+detailName+detailOption+detailCount+detailWeight+detailPrice+'</tr>'+bodyHTML;
						}
					}
				}
			}
			function mypage_mobile(mydetail) {
				for(var i in mydetail.goods) {
					if(i!='current') {
						if(mydetail.goods[i]) {
							existOrder = true;
							
							var printWeight=mydetail.goods[i].productWeight;
							var printPrice=numberWithWon(mydetail.goods[i].productPrice);
							if(mydetail.goods[i].productSerial) {
								if(mydetail.goods[i].productPrice==-2 || mydetail.goods[i].productPrice=="-2") {
									printWeight=goodsStandardWeight(mydetail.goods[i].productID);
									printPrice=goodsStandardPrice(mydetail.goods[i].productID);
								}else {
									printWeight=numberWithGram(mydetail.goods[i].productWeight);
									printPrice=numberWithWon(mydetail.goods[i].productPrice);
								}
							}else {
								printWeight=goodsStandardWeight(mydetail.goods[i].productID);
								printPrice=goodsStandardPrice(mydetail.goods[i].productID);
							}
							
							if(mydetail.goods[i].freeGoods) {
								printPrice='무료쿠폰 적용';
							}
							
							
							var detailName = '<td>'+goodsName(mydetail.goods[i].productID)+'<br>';
							var detailOption = goodsOption(mydetail.goods[i].productID,mydetail.goods[i].productOption)+'</td>';
							var detailCount='<td>1</td>';
							var detailWeight = '<td>'+printWeight+'<br>';
							var detailPrice = printPrice+'</td>';
							bodyHTML='<tr>'+detailName+detailOption+detailCount+detailWeight+detailPrice+'</tr>'+bodyHTML;
						}
					}
				}
			}
			function goodsName(key) {
				return json.goods_info.goods_info[key].name;
			}
			function goodsStandardPrice(key) {
				if(detail.histories) {
					var tempDate = new Date(detail.histories[0].timestamp);
					tempDate.setUTCHours(tempDate.getUTCHours()+9);
					var tempNum = Number(tempDate.getFullYear())*10000+Number(Number(tempDate.getUTCMonth())+1)*100+Number(tempDate.getUTCDate());
					var tempTime = tempDate.getUTCHours();
					if(tempNum<=20170524 && tempTime<16) {
						if(json.goods_info.goods_info[key].standardpricekor_prior) {
							return '약 '+json.goods_info.goods_info[key].standardpricekor_prior;
						}
					}
				}
				return '약 '+json.goods_info.goods_info[key].standardpricekor;
			}
			function goodsStandardWeight(key) {
				return '약 '+json.goods_info.goods_info[key].standardweightkor;
			}
			function goodsOption(key,opt) {
				opt = Number(opt)-1;
				return json.goods_info.goods_info[key].options[opt].name;
			}
			function numberWithWon(x) {
				return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
			}
			function numberWithGram(x) {
				return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "g";
			}
		}
	}
}

function clipUrl(){
	copyCode = document.getElementById('mypage_mycode').textContent;
	document.getElementById('clip_target').value = copyCode;
	var clipField = document.querySelector('#clip_target');
	clipField.select();
	document.execCommand('copy'); 
	alert ( "코드가 복사되었습니다. \'Ctrl+V\'를 눌러 붙여넣기 해주세요." );
}

function set_cmsbank(selectedCode,selectedBank) {
	document.getElementById('mypage_cms_bankname').value = selectedBank;
	document.getElementById('mypage_cms_bank').value = selectedCode;
}

var imageSRC;
function uploadReview(idx) {

    var output_format = "jpg";
    var fileButton = document.getElementById('fileButton');
	var pid = '';
	var reviewfb = {};
	var orderfb = {};
	var pointfb = {};
	var imgCheck = false;
	for(var k=0;k<document.getElementsByClassName('category-menu').length;k++) {
		document.getElementsByClassName('category-menu')[k].onclick = function(e) {
			document.getElementById('dropdownMenu1').innerHTML = this.textContent+' <span class="caret"></span>';
			pid = e.target.id;
		}
	}
    fileButton.addEventListener('change', function(e) {
        var file = e.target.files[0];
		document.getElementsByClassName('upload-name')[0].value = file.name;
        reader = new FileReader();
        reader.onload = function(event) {
            var i = document.getElementById("source_image");
            i.src = event.target.result;
            i.onload = function() {
				i.style.width = '';
				i.style.height = '';
				i.style.position = '';
				i.style.top = '';
				i.style.marginTop = '';
                image_width = i.width;
                image_height = i.height;
				console.log(image_width,image_height,image_width/image_height);
                if (image_width/image_height>2.3) {
                    i.style.width = "100%";
					i.style.position = "relative";
					//i.style.top = "50%";
					//i.style.marginTop = '-'+(i.height*(300/i.width))/2+'px';
                } else {
                    i.style.height = "100%";
                }
                i.style.display = "block";
                var source_image = document.getElementById('source_image');
                var result_image = document.getElementById('result_image');
                if (source_image.src == "") {
                    alert("이미지를 업로드 해주세요");
                    return false;
                }
                var quality = parseInt(30);
                var time_start = new Date().getTime();
                //result_image.src = compress(source_image, quality, output_format).src;
				var storageRef = firebase.storage().ref('reviews/'+firebase.auth().currentUser.uid+'_'+idx);
				var encodedImg = compress(source_image, quality, output_format).src;
				var parsedImg = encodedImg.split(',');
				imgCheck = true;
				var task = storageRef.putString(parsedImg[1], 'base64');
				task.on('state_changed', 
					function progress(snapshot) {

					},

					function error(err) {

					},

					function complete() {
						var downloadURL = task.snapshot.downloadURL;
						reviewfb = {
							images: {
								"0": downloadURL
							}
						};
						reviewfb.mainImage = downloadURL;
						orderfb.jyg = downloadURL;
						
					}
				);

				

                var duration = new Date().getTime() - time_start;
            }
        };
        if (file.type == "image/png") {
            output_format = "png";
        }
        reader.readAsDataURL(file);
        return false;
    });
	
	
	reviewfb.imagecount = "1";
	document.getElementById('review_fb').onclick = function() {
		var retUID = firebase.auth().currentUser.uid;
		var maxCount = 4;
		var setCount = 0;
		var reviewReward = 0;
		if(pid != '' && document.getElementById('review-title').value != "" && document.getElementById('review-content').value != "" && retUID) {
			var today = new Date();
			var toYear = today.getFullYear();
			var toMonth = today.getMonth() + 1;
			var toDate = today.getDate();
			var pname = customer.username.replace(customer.username.substring(1,2),'*');
			if(toMonth < 10) toMonth = '0'+toMonth;
			if(toDate < 10) toDate = '0'+toDate;

			orderfb.facebook = "none";
			orderfb.instagram = "none";
			orderfb.naver = "none";
			orderfb.jyg_detail = document.getElementById('review-content').value;
			
			
			if(!imgCheck) {
				reviewReward = 200;
				orderfb.jyg = "none";
				pointfb = {
					changed: "-200",
					timestamp: new Date().toISOString(),
					title: "일반후기 적립금"
				};
				firebase.database().ref('customers/' + retUID + '/rewards/points').set({
					current: customer.rewards.points.current + 200
				}).then(function () {complete_set();});
			} else {
				reviewReward = 500;
				pointfb = {
					changed: "-500",
					timestamp: new Date().toISOString(),
					title: "사진후기 적립금"
				};
				firebase.database().ref('customers/' + retUID + '/rewards/points').set({
					current: customer.rewards.points.current + 500
				}).then(function () {complete_set();});
			}
			
			firebase.database().ref('customers/' + retUID + '/order_records/'+idx+'/reviews').set(orderfb).then(function () {complete_set();});
			
			var updatesP = {};
			updatesP['customers/'+retUID+'/rewards/points_history/'+customer.rewards.points_history.length] = pointfb;
			firebase.database().ref().update(updatesP).then(function () {complete_set();});
			
			var madepid = getpid(pid);
			function getpid(setpid) {
				if(setpid=="ctporkbelly") {return "porkbelly-fresh";}
				else if(setpid=="ctporkneck") {return "porkneck-fresh";}
				else if(setpid=="ctchicken") {return "chicken-whole";}
				else if(setpid=="ctegg") {return "egg-fresh";}
				return "undefined";
			}
			reviewfb.code = "0";
			reviewfb.pid = madepid;
			reviewfb.comment_count = 0;
			reviewfb.postDate = toYear + '-' + toMonth + '-' + toDate;
			reviewfb.writerID = retUID;
			reviewfb.writerName = pname;
			reviewfb.published = false;
			reviewfb.top = false;
			reviewfb.reference = "none";
			reviewfb.href = "none";
			reviewfb.imagecount = "1";
			reviewfb.title = document.getElementById('review-title').value;
			reviewfb.summary = document.getElementById('review-content').value;
			reviewfb.post = document.getElementById('review-content').value;

			var reviewKey = toYear + '-' + toMonth + '-' + toDate + '_' + retUID + '_' + idx + '_0';
			var updatesC = {};
			updatesC['reviews/reviews/' + reviewKey] = reviewfb;
			firebase.database().ref().update(updatesC).then(function () {complete_set();});
			
			
			document.getElementById('write_review').outerHTML = '';
			function complete_set() {
				setCount++;
				if(setCount==maxCount) {
					alert("후기가 정상적으로 등록되어 "+reviewReward+"원이 적립되었습니다.\n최적화 작업이 완료되면 후기페이지에서 공개됩니다.");
					location.reload();
				}
			}
			//document.getElementById('write_review').outerHTML = '<a href="review.php" id="view_review">후기 보기</a>';
		}
		else {
			alert('제목과 카테고리, 내용을 모두 입력해주세요.');
		}
	}
}


function compress(image, quality, format) {
    var typeImg = "image/jpeg";
    "undefined" != typeof format && "png" == format && (typeImg = "image/png");
    var canvas = document.createElement("canvas");
    canvas.width = image.naturalWidth, canvas.height = image.naturalHeight;
    var compress = (canvas.getContext("2d").drawImage(image, 0, 0), canvas.toDataURL(typeImg, quality / 100)),
        compressImg = new Image;
    return compressImg.src = compress, compressImg
}

