var config = {
    apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
    authDomain: "jyg-custom.firebaseapp.com",
    databaseURL: "https://jyg-custom.firebaseio.com",
    projectId: "jyg-custom",
    storageBucket: "jyg-custom.appspot.com",
    messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
cacheBlocked();
function cacheBlocked() {
	try{ localStorage.setItem("cacheBlocked",false);
	}catch(e) { alert("현재 사용중이신 브라우저의 설정이 쿠키 사용안함으로 설정되어 있어 정상적인 사이트 이용이 불가능합니다.\n설정에서 쿠키 '사용함'으로 변경 후 다시 시도해주시기 바랍니다.");}
}
var firebase_init = true;
function getFirebase_init() {return firebase_init;}
firebase.auth().onAuthStateChanged(function(custom) {
	firebase_init = false;
		var acustom = firebase.auth().currentUser;
		if(acustom) {
			init_cart_listener(acustom.uid,false);
		}else {
			document.getElementById('payment-loader').style.display='block';
			alert("로그인이 필요한 화면입니다.");
			location.href = "index.php";
		}
});
var fireflag={json:false,customer:false};
var selectedcoupon=null; function get_selectedcoupon() { return selectedcoupon; }
function set_selectedcoupon(val) {
	selectedcoupon = val;
	init_cart_listener(firebase.auth().currentUser.uid,true);
}
var json; function get_json() {return json;} function set_json(val) {json=val;}
var customer; function get_customer() {return customer;} function set_customer(val) {customer=val;}
var datelist; function get_datelist() {return datelist;} function set_datelist(val) {datelist=val;}
var dateofflist, dateoffmax, dateoffflag;
var selecteddate; function get_selecteddate() {return selecteddate;} function set_selecteddate(val) {selecteddate=val;}
var goodkey; function get_goodkey() {return goodkey;} function set_goodkey(val) {goodkey=val;}
init_json_listener();
function init_json_listener() {
	firebase.database().ref('json').on('value', function(jsonsnap) {
		console.log("json refresh");
		json = jsonsnap.val();
		fireflag.json=true;
		init_index_goods();
		asyncsupport();
	});
	function asyncsupport() {
		if(fireflag.customer) {
			if(firebase.auth().currentUser) {
				init_cart_listener(firebase.auth().currentUser.uid,true);
			}
		}
	}
	function init_index_goods() {
		var goods_info = json.goods_info.goods_info;
		json.goods_list=[];
		json.goods_length = 0;
		for(var goodkey in goods_info) {
			if(goods_info[goodkey].standardindex) {
				json.goods_length++;
				json.goods_list[goods_info[goodkey].standardindex-1]=goods_info[goodkey];
				json.goods_list[goods_info[goodkey].standardindex-1].key = goodkey;
			}
		}
	}
}
function get_automate_today(compare_date) {
	var inputDate = new Date();
	inputDate.setUTCHours(inputDate.getUTCHours()+17);
	inputDate.setUTCDate(inputDate.getUTCDate()+1);
	var targetDate  = inputDate.toISOString().substring(0,10);
	if(compare_date!=targetDate) {
		return false;
	}
	var target_Day = getKorDay(inputDate.getUTCDay());
	return read_json_public(targetDate+" ("+target_Day+")");
	
	function read_json_public(target_json) {
		var target = "https://jyg-custom.firebaseio.com/automate_order/summary/"+target_json+".json";
		var xhttp = new XMLHttpRequest();
		xhttp.open("GET", target, false); xhttp.send();
		if (xhttp.readyState == 4 && xhttp.status == 200) { return JSON.parse(xhttp.responseText);}
		return null;
	}
	function getKorDay(target) {
		if(target===0) {return '일';}if(target===1) {return '월';}if(target===2) {return '화';}
		if(target===3) {return '수';}if(target===4) {return '목';}if(target===5) {return '금';}
		if(target===6) {return '토';}return '일';
	}
}
function isPartialSoldout(compare_date) {
	var automate_today = get_automate_today(compare_date);
	var automate_max = getpayment_net();
	var automate_over = false;
	if(automate_today) {
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					automate_today[i][j.substring(1)].net = Number(automate_today[i][j.substring(1)].total)+Number(customer.carts[i][j].count);
					if(automate_max[i]) {
						if(automate_today[i][j.substring(1)].net>automate_max[i]) {
							automate_over = true;
							return automate_over;
						}
					}
				}
			}
		}
		return false;
	}
	return false;
}
function isPartialDayoff(targetDate,target_Day) {
	if(dateofflist[targetDate].count!=dateoffmax && !dateoffflag) {
		var blockedGood = '';
		for(var i in dateofflist[targetDate]) {
			if(i!='count') {
				blockedGood+=json.goods_info.goods_info[i].standardname+' ';
			}
		}
		blockedGood+='이 매진되어 배송일이 늦춰졌습니다. 해당상품을 제외하시면 더 빠르게 상품을 받으실 수 있습니다.';
		alert(blockedGood);
		dateoffflag=true;
	}
}
function init_cart_listener(currentUID,asyncflag) {
	if(!asyncflag) {
		firebase.database().ref('customers/'+currentUID).on('value', function(customersnap) {
			console.log("customer refresh");
			customer = customersnap.val();
			fireflag.customer=true;
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();
				print_coupons();
				print_payment_info();
				print_order_address();
				print_payment_card();
			}
		});
	}else {
		init_cart_goods();
		print_cart_goods();
		print_cart_prices();
		print_coupons();
		print_payment_info();
		print_order_address();
		print_payment_card();
	}
	function init_cart_goods() {
		if(!customer.calcprice) {
			customer.calcprice={
				sum:0,
				shipping:2500,
				echo:0,
				direct:0,
				total:0,
				net:0,
				count:0
			};
		}else {
			customer.calcprice.sum=0;
			customer.calcprice.shipping=2500;
			customer.calcprice.echo=0;
			customer.calcprice.direct=0;
			customer.calcprice.total=0;
			customer.calcprice.net=0;
			customer.calcprice.count=0;
		}
		
		init_available_dates();
		
		function init_available_dates() {
			datelist = {};
			dateofflist = {};
			dateoffmax=-1;
			for(var i in customer.carts) {
				if(i!=='current' && i!=='timestamp') {
					for(var j in json.goods_info.goods_info[i].dayoff) {
						if(json.goods_info.goods_info[i].dayoff[j]) {
							datelist[j] = true;
							if(dateofflist[j]) {
								dateofflist[j].count++;
								dateofflist[j][i]=true;
							}else {
								dateofflist[j]={count:1};
								dateofflist[j][i]=true;
							}
							if(dateofflist[j]) {
								if(dateofflist[j].count>dateoffmax) {
									dateoffmax = dateofflist[j].count;
								}
							}
						}
					}
				}
			}
		}
		init_shipping_date();
		function init_shipping_date() {
			if(realtime_date(customer.carts)) {
				selecteddate = customer.carts.current;
				var partial_result = isPartialSoldout(selecteddate.substring(0,10));
				if(datelist[selecteddate.substring(0,10)] || partial_result) {
					selecteddate = getNextDate(customer.carts.current);
					alert("장바구니에 담긴 상품의 재고가 부족합니다.\n"+customer.carts.current+"배송이 불가하여 "+selecteddate+"로 지정합니다.");
					firebase.database().ref('customers/'+currentUID+'/carts/current').set(selecteddate);
				}
			}
			else if(!selecteddate) {
				var startdate = new Date();
				startdate.setHours(startdate.getHours()+9);
				if(startdate.getUTCHours()>16) {startdate.setDate(startdate.getDate()+1);}
				selecteddate = getNextDate(startdate.toISOString());
			}else {
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(selecteddate);
				}
			}
			function realtime_date(targetCart) {
				var pastDate = new Date();
				pastDate.setDate(pastDate.getDate()+1);
				if(!firebase.auth().currentUser) {return false;}
				var targetUID = firebase.auth().currentUser.uid;
				if(!targetUID) { return false;}
				if(targetCart) {
					if(targetCart.current!=0) {
						if(targetCart.current.toString().indexOf('(')!=-1) {
							if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
								firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
								return false;
							}
							return true;
						}
					}
				}return false;
			}
			function getNextDate(target) {
				var returndate = new Date(target.substring(0,10));
				var findflag = false;
				for(var i=0;i<50;i++) {
					returndate.setDate(returndate.getDate()+1);
					if(!datelist[returndate.toISOString().substring(0,10)]) {
						if(isPartialSoldout(returndate.toISOString().substring(0,10))) {}
						else { findflag=true; break; }
					}else {
						isPartialDayoff(returndate.toISOString().substring(0,10),getKorDay(returndate.getUTCDay()));
					}
				}
				if(!findflag) { return target; }
				return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
			}
			function getKorDay(target) {
				if(target===0) {return '일';}
				if(target===1) {return '월';}
				if(target===2) {return '화';}
				if(target===3) {return '수';}
				if(target===4) {return '목';}
				if(target===5) {return '금';}
				if(target===6) {return '토';}
				return '일';
			}
		}
	}
	function print_cart_goods() {
		var cartHTML = '';
		var paymentcartHTML = '';
		var itemCount = 0;
		var emptyCart = true;
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					if(customer.carts[i][j]) {
						emptyCart = false;
						var key = i;
						var count = customer.carts[i][j].count;
						itemCount++;
						customer.calcprice.sum+=json.goods_info.goods_info[key].standardprice*count;
						customer.calcprice.count+=count;
					}
				}
			}
		}
		var couponCustomerJson = applyCouponToCart();
		var couponCustomer = couponCustomerJson.customerVal;
		var couponHTML = '';
		customer.couponSelected = null;
		if(couponCustomerJson.appliedCoupon) {
			customer.couponSelected = selectedcoupon;
			console.log(customer.couponSelected);
			if(couponCustomerJson.appliedType=="freeGoods") {
				couponHTML='<div class="cart_item cart_item_pay" style="background-color:#dbcece">'+
				'<div class="citem-name pay_item_border"><span style="color:#90312B">'+goodsName(couponCustomerJson.appliedKey)+'</span></div>'+
				'<div class="citem-option pay_item_border">'+goodsOption(couponCustomerJson.appliedKey,couponCustomerJson.appliedOption)+'</div>'+
				'<div class="citem-select">'+
				'<span class="citem_tag">수량</span>'+
				'<span class="citem-quantity">'+couponCustomerJson.appliedCount+'</span>'+
				'<span class="citem-price citem-payment">'+'무료'+'</span>'+
				'</div>'+
				'</div>';
				customer.calcprice.sum-=json.goods_info.goods_info[couponCustomerJson.appliedKey].standardprice*couponCustomerJson.appliedCount;
			}
		}
		
		for(var i in couponCustomer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in couponCustomer.carts[i]) {
					if(couponCustomer.carts[i][j]) {
						emptyCart = false;
						var option = Number(j.toString().substring(1))-1;
						var key = i;
						var count = couponCustomer.carts[i][j].count;
						var itemHTML = '<div class="cart_item">'+
										'<div class="citem-name"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						var payitemHTML = '<div class="cart_item cart_item_pay">'+
										'<div class="citem-name pay_item_border"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option pay_item_border">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price citem-payment">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						cartHTML+=itemHTML;
						paymentcartHTML+=payitemHTML;
					}
				}
			}
		}
		if(emptyCart) {
			//alert('결제할 상품이 없습니다. 메인화면으로 이동합니다.');
			//location.href="index.php";
		}
		paymentcartHTML+=couponHTML;
		document.getElementById('payment_cart_list').innerHTML = paymentcartHTML;
		document.getElementById('cart_list').innerHTML = cartHTML;
		function goodsName(target) {
			return json.goods_info.goods_info[target].standardname;
		}
		function goodsOption(target,option) {
			return json.goods_info.goods_info[target].options[option].name;
		}
		function goodsPrice(target,count) {
			return numberWithWon(json.goods_info.goods_info[target].standardprice*count);
		}
		function applyCouponToCart() {
			var x = customer;
			var returncustomer={customerVal:{},appliedCoupon:false};
			returncustomer.customerVal= JSON.parse(JSON.stringify(x));
			if(selectedcoupon) {
				if(customer.coupons.get[selectedcoupon].condition_use.type=="goodsCount") {
					if(customer.coupons.get[selectedcoupon].condition_use.goodsCount.except) {
						if(customer.carts[customer.coupons.get[selectedcoupon].condition_use.goodsCount.except]) {
							returncustomer.customerVal.carts[customer.coupons.get[selectedcoupon].condition_use.goodsCount.except].a1.count--;
							if(returncustomer.customerVal.carts[customer.coupons.get[selectedcoupon].condition_use.goodsCount.except].a1.count==0) {
								returncustomer.customerVal.carts[customer.coupons.get[selectedcoupon].condition_use.goodsCount.except].a1 = null;
							}
							returncustomer.appliedCoupon = true;
							returncustomer.appliedType = customer.coupons.get[selectedcoupon].reward.type;
							returncustomer.appliedKey = customer.coupons.get[selectedcoupon].reward.freeGoods.pid;
							returncustomer.appliedOption = customer.coupons.get[selectedcoupon].reward.freeGoods.pop-1;
							returncustomer.appliedCount = customer.coupons.get[selectedcoupon].reward.freeGoods.count;
						}
					}
				}
			}
			return returncustomer;
		}
	}
	function print_cart_prices() {
		if(!customer.calcprice.point) {
			customer.calcprice.point=0;
			document.getElementById('payment_pointUsed').value=0;
			if(document.getElementById('payment_pointUsed_store').value!=0) {
				document.getElementById('payment_pointUsed').value = document.getElementById('payment_pointUsed_store').value;
				customer.calcprice.point = Number(document.getElementById('payment_pointUsed_store').value);
			}
		}
		if(customer.calcprice.count<2) {customer.calcprice.echo = 0;}
		else if(customer.calcprice.count<7) {customer.calcprice.echo=(customer.calcprice.count-1)*500;}
		else {customer.calcprice.echo=2500;}
		if(document.getElementById('radio_method_2')) {
			if(document.getElementById('radio_method_2').checked) {
				customer.calcprice.direct = Math.floor((customer.calcprice.sum)/100) * 10;
			}else {customer.calcprice.direct=0;}
		}
		
		customer.calcprice.total = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo;
		document.getElementById('cart_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('cart_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('cart_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('cart_total').innerHTML = numberWithWon(customer.calcprice.total);
		
		customer.calcprice.net = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo-customer.calcprice.point+customer.calcprice.direct;
		document.getElementById('payment_pointown').innerHTML = numberWithWon(Number(customer.rewards.points.current))+')';
		document.getElementById('payment_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('payment_point').innerHTML = numberWithWon(-1*customer.calcprice.point);
		document.getElementById('payment_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('payment_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('payment_direct').innerHTML = numberWithWon(customer.calcprice.direct);
		document.getElementById('payment_net').innerHTML = numberWithWon(customer.calcprice.net);
		
		
		document.getElementById('cart_shippingdate').innerHTML = selecteddate;
		
		document.getElementById('payment_shippingdate').innerHTML = selecteddate;
		
		if(customer.calcprice.count==0) {
			document.getElementById('cart_empty').style.display="block";
		} else {
			document.getElementById('cart_empty').style.display="none";
		}
	}
	function print_coupons() {
		if(!customer.coupons) {return;}
		if(!customer.coupons.get) {return;}
		if(selectedcoupon) {
			if(!coupon_validation(selectedcoupon)) {
				set_selectedcoupon(null);
			}else {
				document.getElementById('payment_coupon').value = customer.coupons.get[selectedcoupon].info.name;
			}
		}else {
			document.getElementById('payment_coupon').value = '미선택';
		}
		var divHTML = '';
		for(var i in customer.coupons.get) {
			if(!customer.coupons.get[i].used) {
				var validationStatus = coupon_validation(i);
				var couponStatus = '사용가능';
				var couponIcon = 'img/mypage/coupon_own.png';
				var couponClass = '';
				var couponClick = customer.coupons.get[i].info.code;
				if(!validationStatus) {
					couponStatus = '사용불가';
					couponIcon = 'img/mypage/coupon_used.png';
					couponClass = '_used';
					couponClick = 'blocked';
				}

				divHTML+='<div class="dialog_couponlist_coupon'+couponClass+'" onClick="apply_coupon('+"'"+couponClick+"'"+')">'+
					'<div class="dialog_coupon_img"><img src="'+couponIcon+'"></div>'+
					'<div class="dialog_coupon_description">'+
					customer.coupons.get[i].info.name+'<br>'+
					customer.coupons.get[i].info.description+'<br>'+
					'<span>'+customer.coupons.get[i].due.use_end.substring(0,8)+' 까지</span></div>'+
					'<div class="dialog_coupon_select">'+couponStatus+'</div>'+
					'</div>';
			}
		}
		if(document.getElementById('dialog_couponlist')) {
			document.getElementById('dialog_couponlist').innerHTML = divHTML;
		}
		
		function coupon_validation(targetCoupon) {
			var currentCouponTime = geteventTime();
			if(currentCouponTime<customer.coupons.get[targetCoupon].due.download_start || currentCouponTime>customer.coupons.get[targetCoupon].due.download_end) {
				return false;
			}
			if(customer.coupons.get[targetCoupon].condition_use.type=="goodsCount") {
				var compareCount = customer.calcprice.count;
				var targetExist = false;
				if(customer.coupons.get[targetCoupon].condition_use.goodsCount.except) {
					if(customer.carts[customer.coupons.get[targetCoupon].condition_use.goodsCount.except]) {
						compareCount--;
						targetExist = true;
					}else {
						return false;
					}
				}
				if(compareCount>=customer.coupons.get[targetCoupon].condition_use.goodsCount.min && compareCount<=customer.coupons.get[targetCoupon].condition_use.goodsCount.max) {
					return true;
				}
			}
			return false;
		}
	}
	function print_payment_info() {
		document.getElementById('payment_customername').innerHTML = customer.username;
		document.getElementById('payment_customerphone').innerHTML = customer.userphone;
		document.getElementById('payment_customeremail').innerHTML = customer.useremail;
	}
	function print_order_address() {
		if(!customer.address) {
			$("#radio_address_2").prop("checked", true);
			document.getElementById('radio_address_1').style.display = 'none';
			document.getElementById('radio_address_1-span').style.display = 'none';
			document.getElementById('saved_address').style.display = 'none';
			document.getElementById('input_address').style.display = 'block';
		}
		else {
			document.getElementById('radio_address_1').style.display = 'inline-block';
			document.getElementById('radio_address_1-span').style.display = 'inline-block';
			document.getElementById('order_name').innerHTML = customer.address.name;
			document.getElementById('order_phone').innerHTML = customer.address.phone;
			document.getElementById('order_postcode').innerHTML = "("+customer.address.postcode+")";
			document.getElementById('order_address').innerHTML = customer.address.address;
			document.getElementById('order_room').innerHTML = customer.address.room;
			document.getElementById('saved_address').style.display = 'block';
			document.getElementById('input_address').style.display = 'none';
		}
	}
	function print_payment_card() {
		var isCardEmpty = true;
		var cardHTML = '';
		var lastCard;
		if(customer.cards) {
			for(var i in customer.cards) {
				if(i!='current' && customer.cards[i]) {
					if(customer.cards[i].res_msg) {
						var itemHTML = '<option value="'+i+'">'+customer.cards[i].res_msg+'</option>';
						isCardEmpty = false;
						lastCard=i;
						cardHTML+=itemHTML;
					}
				}
			}
		}
		cardHTML += '<option value="new">신규 카드등록</option>';
		document.getElementById('card_option').innerHTML = cardHTML;
		if(!isCardEmpty) {
			document.getElementById('card_option').value = lastCard;
		}
		
		
		if(customer.bank) {
			if(customer.bank.name && customer.bank.number) {
				document.getElementById('bank_user').innerHTML = customer.bank.name;
				document.getElementById('bank_number').innerHTML = customer.bank.number;
			}
			else {
				document.getElementById('bank_user').innerHTML = "환불";
				document.getElementById('bank_number').innerHTML = "계좌가 등록되어 있지 않아 적립금으로 환불됩니다.";
			}
		}else {
			document.getElementById('bank_user').innerHTML = "환불";
			document.getElementById('bank_number').innerHTML = "계좌가 등록되어 있지 않아 적립금으로 환불됩니다.";
		}
		document.getElementById('payment-loader').style.display='none';
	}
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}