document.addEventListener("DOMContentLoaded", function (event) {
    var _selector = document.querySelector('input[name=check_all]');
    _selector.addEventListener('change', function (event) {
        if (_selector.checked) {
            document.getElementById('check_age').checked = true;
			document.getElementById('check_usage').checked = true;
			document.getElementById('check_privacy').checked = true;
        } else {
            document.getElementById('check_age').checked = false;
			document.getElementById('check_usage').checked = false;
			document.getElementById('check_privacy').checked = false;
        }
    });
});

$(document).mouseup(function (e)
{
    var container = $("#sidebar_main");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        document.getElementById('sidebar_cart').style.display='none';
    }
});


$('#cat').hover(function()
{
}, function()
{
});