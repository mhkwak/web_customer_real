// JavaScript Document
function signin() {
	firebase.auth().signInWithEmailAndPassword(document.getElementById('customer_id').value, document.getElementById('customer_pw').value).catch(function(error) {
	  var errorCode = error.code;
	  var errorMessage = error.message;
	});
	close_sidebar('sidebar_signin');
}
function signup() {
	open_dialog('dialog_agreement');
}
function findpw() {
	var findid = document.getElementById('customer_id').value;
	if(!findid) {alert("비밀번호를 찾으실 아이디를 입력해주세요.");}
	else {}
}
function close_dialog(target) {
	document.getElementById(target).style.display='none';
}
function open_dialog(target) {
	document.getElementById(target).style.display='block';
}
var sms={};
function getSMSCode() {return sms;}
function send_code() {
	var check_age = document.getElementById('check_age').checked;
	var check_usage = document.getElementById('check_usage').checked;
	var check_privacy = document.getElementById('check_privacy').checked;
	if(check_age&&check_usage&&check_privacy) {
		sendSMS();
	}else { alert("모든 약관에 동의해주세요."); }
	function sendSMS() {
		var smsHEAD = 'http://api.coolsms.co.kr/sendmsg?user=jeongyookgak&password=%200297a0d059a15401faaacd423e7a4491&enc=MD5&to=';
		sms.name=document.getElementById('user_name').value;
		sms.phone=document.getElementById('user_phone').value;
		sms.code = createCertification();
		var smsAction = smsHEAD + sms.phone + "&from=18000658&text=[(주)정육각] 본인인증번호는 "+sms.code+" 입니다. 정확히 입력해주세요.";
		var iframe = document.getElementById('smsFrame');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
		var sms_form=innerDoc.getElementById('sms_form');
		sms_form.action=smsAction; sms_form.submit();
		document.getElementById('user_code').style.display = 'block';
	}
	function createCertification() { return Math.floor(Math.random() * (900000)) + 100000;	}
}
function check_code() {
	if(document.getElementById('user_phone').value==sms.phone && document.getElementById('user_code').value==sms.code) {
		close_dialog('dialog_agreement');
		next_signup();
	}else { alert("인증번호가 맞지 않습니다. 다시 확인해주세요."); }
	function next_signup() {
		open_dialog('dialog_signup');
		document.getElementById('signup_confirm_btn').onclick = signup_confirm;
	}
}

function link_payment() {
}




function open_sidebar(target) {
	if(target=='sidebar_signin' && firebase.auth().currentUser) {
		firebase.auth().signOut().then(function() {}, function(error) {});
	}
	else {
		document.getElementById(target).style.display='block';
	}
}
function close_sidebar(target) {
	document.getElementById(target).style.display='none';
}




function show_options(target) {
	document.getElementById(target+'-wrap').style.display='block';
}


function date_prior() {
	changedate(false);
}
function date_next() {
	changedate(true);
}

function track_code() {
	window.open( 'http://distribution.ekape.or.kr/user/one/combine/combineSearch.do?searchKeywor'+document.getElementById('tracking_code').value,'_blank');
}

function select_less(target) {
	addselect(target,-1);
}
function select_more(target) {
	addselect(target,1);
}
function select_detail(target) {
	//window.open( 'customer.php?uid='+target.uid,'_blank');
}
function select_add(target) {
	addToCart(target);
	open_sidebar('sidebar_cart');
}
function select_option(target,option) {
	addoption(target,option);
}
function direct_less(target,option) {
	directToCart('less',target,option);
}
function direct_more(target,option) {
	directToCart('more',target,option);
}
function direct_remove(target,option) {
	directToCart('remove',target,option);
}