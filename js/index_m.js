var config = {
    apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
    authDomain: "jyg-custom.firebaseapp.com",
    databaseURL: "https://jyg-custom.firebaseio.com",
    projectId: "jyg-custom",
    storageBucket: "jyg-custom.appspot.com",
    messagingSenderId: "222150107123"
  };
firebase.initializeApp(config);
cacheBlocked();
function cacheBlocked() {
	try{ localStorage.setItem("cacheBlocked",false);
	}catch(e) { alert("현재 사용중이신 브라우저의 설정이 쿠키 사용안함으로 설정되어 있어 정상적인 사이트 이용이 불가능합니다.\n설정에서 쿠키 '사용함'으로 변경 후 다시 시도해주시기 바랍니다.");}
}
var firebase_init = true;
function getFirebase_init() {return firebase_init;}
var welcome_ready=false;
firebase.auth().onAuthStateChanged(function(custom) {
	firebase_init = false;
		var acustom = firebase.auth().currentUser;
		if(acustom) {
			if(welcome_ready) {alert("로그인되었습니다.");}
			init_cart_listener(acustom.uid,false);
			setCookie('OID', acustom.uid, 1);
		}else {welcome_ready=true;setCookie('OID', 'oid', -1);}
});
var fireflag={json:false,customer:false};
var json; function get_json() {return json;} function set_json(val) {json=val;}
var customer; function get_customer() {return customer;} function set_customer(val) {customer=val;}
var datelist; function get_datelist() {return datelist;} function set_datelist(val) {datelist=val;}
var selecteddate; function get_selecteddate() {return selecteddate;} function set_selecteddate(val) {selecteddate=val;}
var goodkey; function get_goodkey() {return goodkey;} function set_goodkey(val) {goodkey=val;}
var index_date;
init_json_listener();
function init_json_listener() {
	firebase.database().ref('json').on('value', function(jsonsnap) {
		console.log("json refresh");
		json = jsonsnap.val();
		fireflag.json=true;
		print_production_date();
		init_index_goods();
		print_index_goods();
		print_index_date();
		print_event_banner();
		asyncsupport();
	});
	function asyncsupport() {
		if(fireflag.customer) {
			if(firebase.auth().currentUser) {
				init_cart_listener(firebase.auth().currentUser.uid,true);
			}
		}
	}
	function print_production_date() {
		var date_info = json.date_info;
		for(var key in date_info) {
			if(date_info[key]) {
				var dateHTML='<img class="date-card front" src="img/date/'+date_info[key][0]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][1]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][2]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][3]+'.jpg">'+
					'<img class="date-card middle" src="img/date/'+date_info[key][4]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][5]+'.jpg">'+
					'<img class="date-card middle" src="img/date/'+date_info[key][6]+'.jpg">'+
					'<img class="date-card last" src="img/date/'+date_info[key][7]+'.jpg">';
				if(key.indexOf('pork')!=-1) {
					dateHTML = dateHTML + '<span class="date-card-after">부터</span>';
				}
				document.getElementById(key+'-date').innerHTML=dateHTML;
			}
		}
	}
	function print_index_date() {
		index_date = json.date_shippingdate;
		if(index_date && document.getElementById('date_standard')) {
			document.getElementById('date_standard').innerHTML = index_date.substring(5,7)+'월 '+index_date.substring(8,10)+'일 '+index_date.substring(12,13)+'요일';
		}
	}
	function print_event_banner() {
		if(document.getElementById('event_banner_top')) {
			if(firebase.auth().currentUser) {
				document.getElementById('event_banner_top').innerHTML = '<a href="eventdetail_m.php?code=recommend">[이벤트] 친구 추천 시 적립금이 2,000원!</a>';
			}else {
				document.getElementById('event_banner_top').innerHTML = '[이벤트] 신규가입 시 적립금 3,000원!';
			}
		}
	}
	function init_index_goods() {
		var goods_info = json.goods_info.goods_info;
		json.goods_list=[];
		json.goods_length = 0;
		for(var goodkey in goods_info) {
			if(goods_info[goodkey].standardindex) {
				json.goods_length++;
				json.goods_list[goods_info[goodkey].standardindex-1]=goods_info[goodkey];
				json.goods_list[goods_info[goodkey].standardindex-1].key = goodkey;
			}
		}
	}
	function print_index_goods() {
		var rowHTML=[];
		var rowCount=0;
		rowHTML[0]=''; rowHTML[1]=''; rowHTML[2]=''; rowHTML[3]=''; rowHTML[4]=''; rowHTML[5]=''; rowHTML[6]=''; rowHTML[7]='';
		for(var i=0;i<json.goods_length;i++) {
			var optionHTML='';
			for(var j in json.goods_list[i].options) {
				if(json.goods_list[i].options[j]) {
					if(json.goods_list[i].options[j].status.publish) {
						optionHTML+='<div id="'+json.goods_list[i].key+'-op'+j+'" onClick="select_option('+"'"+json.goods_list[i].key+"',"+j+')">'+json.goods_list[i].options[j].name+'</div>';
					}
				}
			}
			var itemHTML='';
			itemHTML='<div class="col-xs-6">'+
						'<img src="img/good_index/'+json.goods_list[i].key+'-index.jpg" class="simage" onClick="select_detail('+"'"+json.goods_list[i].key+"'"+')">'+
						'<div class="stext" onClick="select_detail('+"'"+json.goods_list[i].key+"'"+')">'+
							'<p id="'+json.goods_list[i].key+'-sname" class="sname">'+json.goods_list[i].standardname+'</p>'+
							'<p id="'+json.goods_list[i].key+'-sweight" class="sweight">'+json.goods_list[i].standardweightkor+'</p>'+
							'<p id="'+json.goods_list[i].key+'-sprice"class="sprice">'+json.goods_list[i].standardpricekor+'</p>'+
						'</div>'+
					'</div>';
			rowHTML[rowCount]+=itemHTML;
			json.goods_info.goods_info[json.goods_list[i].key].selectCount=1;
			json.goods_info.goods_info[json.goods_list[i].key].selectOption=0;
			if(i%2===1) {rowCount++;}
		}
		if((json.goods_length%2)===0) {
			rowHTML[rowCount] = '<div class="col-xs-6"></div>'+rowHTML[rowCount]+'<div class="col-xs-6"></div>';
			rowCount++;
		}else if((json.goods_length%2)===1) {
			rowHTML[rowCount] = rowHTML[rowCount]+'<div class="col-xs-6"></div>';
			rowCount++;
		}
		for(i=0;i<rowCount;i++) {
			if(document.getElementById('maingoods_row_'+i)) {
				document.getElementById('maingoods_row_'+i).innerHTML = rowHTML[i];
			}
		}
	}
}

function init_cart_listener(currentUID,asyncflag) {
	if(!asyncflag) {
		firebase.database().ref('customers/'+currentUID).on('value', function(customersnap) {
			console.log("customer refresh");
			customer = customersnap.val();
			fireflag.customer=true;
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();
			}
		});
	}else {
		init_cart_goods();
		print_cart_goods();
		print_cart_prices();
	}
	function init_cart_goods() {
		customer.calcprice={
			sum:0,
			point:0,
			shipping:2500,
			echo:0,
			total:0,
			net:0,
			count:0
		};
		
		init_available_dates();
		
		function init_available_dates() {
			datelist = {};
			for(var i in customer.carts) {
				if(i!=='current' && i!=='timestamp') {
					for(var j in json.goods_info.goods_info[i].dayoff) {
						if(json.goods_info.goods_info[i].dayoff[j]) {
							datelist[j] = true;
						}
					}
				}
			}
		}
		init_shipping_date();
		function init_shipping_date() {
			if(realtime_date(customer.carts)) {
				selecteddate = customer.carts.current;
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(customer.carts.current);
					firebase.database().ref('customers/'+currentUID+'/carts/current').set(selecteddate);
				}
			}
			else if(!selecteddate) {
				var startdate = new Date();
				startdate.setHours(startdate.getHours()+9);
				if(startdate.getUTCHours()>16) {startdate.setDate(startdate.getDate()+1);}
				selecteddate = getNextDate(startdate.toISOString());
			}else {
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(selecteddate);
				}
			}
			index_date=selecteddate;
			function realtime_date(targetCart) {
				var pastDate = new Date();
				pastDate.setDate(pastDate.getDate()+1);
				if(!firebase.auth().currentUser) {return false;}
				var targetUID = firebase.auth().currentUser.uid;
				if(!targetUID) { return false;}
				if(targetCart) {
					if(targetCart.current!=0) {
						if(targetCart.current.toString().indexOf('(')!=-1) {
							if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
								firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
								return false;
							}
							return true;
						}
					}
				}return false;
			}
			function getNextDate(target) {
				var returndate = new Date(target.substring(0,10));
				var findflag = false;
				for(var i=0;i<50;i++) {
					returndate.setDate(returndate.getDate()+1);
					if(!datelist[returndate.toISOString().substring(0,10)]) { findflag=true; break; }
				}
				if(!findflag) { return target; }
				return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
			}
			function getKorDay(target) {
				if(target===0) {return '일';}
				if(target===1) {return '월';}
				if(target===2) {return '화';}
				if(target===3) {return '수';}
				if(target===4) {return '목';}
				if(target===5) {return '금';}
				if(target===6) {return '토';}
				return '일';
			}
		}
	}
	function print_cart_goods() {
		var cartHTML = '';
		var itemCount = 0;
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					if(customer.carts[i][j]) {
						var option = Number(j.toString().substring(1))-1;
						var key = i;
						var count = customer.carts[i][j].count;
						var itemHTML = '<div class="cart_item">'+
										'<div class="citem-name"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						itemCount++;
						cartHTML+=itemHTML;
						customer.calcprice.sum+=json.goods_info.goods_info[key].standardprice*count;
						customer.calcprice.count+=count;
					}
				}
			}
		}
		document.getElementById('cart_list').innerHTML = cartHTML;
		function goodsName(target) {
			return json.goods_info.goods_info[target].standardname;
		}
		function goodsOption(target,option) {
			return json.goods_info.goods_info[target].options[option].name;
		}
		function goodsPrice(target,count) {
			return numberWithWon(json.goods_info.goods_info[target].standardprice*count);
		}
	}
	function print_cart_prices() {
		if(customer.calcprice.count<2) {customer.calcprice.echo = 0;}
		else if(customer.calcprice.count<7) {customer.calcprice.echo=(customer.calcprice.count-1)*500;}
		else {customer.calcprice.echo=2500;}
		customer.calcprice.total = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo;
		document.getElementById('cart_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('cart_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('cart_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('cart_total').innerHTML = numberWithWon(customer.calcprice.total);
		
		document.getElementById('cart_shippingdate').innerHTML = selecteddate;
		
		if(customer.calcprice.count==0) {
			document.getElementById('cart_empty').style.display="block";
		} else {
			document.getElementById('cart_empty').style.display="none";
		}
	}
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}
function setCookie(cName, cValue, cDay){
	var expire = new Date();
	expire.setDate(expire.getDate() + cDay);
	cookies = cName + '=' + escape(cValue) + '; path=/ ';
	if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
	document.cookie = cookies;
}