var config = {
    apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
    authDomain: "jyg-custom.firebaseapp.com",
    databaseURL: "https://jyg-custom.firebaseio.com",
    projectId: "jyg-custom",
    storageBucket: "jyg-custom.appspot.com",
    messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
cacheBlocked();
function cacheBlocked() {
	try{ localStorage.setItem("cacheBlocked",false);
	}catch(e) { alert("현재 사용중이신 브라우저의 설정이 쿠키 사용안함으로 설정되어 있어 정상적인 사이트 이용이 불가능합니다.\n설정에서 쿠키 '사용함'으로 변경 후 다시 시도해주시기 바랍니다.");}
}
var firebase_init = true;
function getFirebase_init() {return firebase_init;}
var welcome_ready=false;
firebase.auth().onAuthStateChanged(function(custom) {
	firebase_init = false;
		var acustom = firebase.auth().currentUser;
		if(acustom) {
			if(welcome_ready) {alert("로그인되었습니다.");}
			init_cart_listener(acustom.uid,false);
		}else {welcome_ready=true;}
});
var fireflag={json:false,customer:false};
var json; function get_json() {return json;} function set_json(val) {json=val;}
var customer; function get_customer() {return customer;} function set_customer(val) {customer=val;}
var datelist; function get_datelist() {return datelist;} function set_datelist(val) {datelist=val;}
var selecteddate; function get_selecteddate() {return selecteddate;} function set_selecteddate(val) {selecteddate=val;}
var goodkey; function get_goodkey() {return goodkey;} function set_goodkey(val) {goodkey=val;}
init_json_listener();
function init_json_listener() {
	firebase.database().ref('json').on('value', function(jsonsnap) {
		console.log("json refresh");
		json = jsonsnap.val();
		fireflag.json=true;
		init_index_goods();
		asyncsupport();
	});
	function asyncsupport() {
		if(fireflag.customer) {
			if(firebase.auth().currentUser) {
				init_cart_listener(firebase.auth().currentUser.uid,true);
			}
		}
	}
	function init_index_goods() {
		var goods_info = json.goods_info.goods_info;
		json.goods_list=[];
		json.goods_length = 0;
		for(var goodkey in goods_info) {
			if(goods_info[goodkey].standardindex) {
				json.goods_length++;
				json.goods_list[goods_info[goodkey].standardindex-1]=goods_info[goodkey];
				json.goods_list[goods_info[goodkey].standardindex-1].key = goodkey;
			}
		}
	}
}

function init_cart_listener(currentUID,asyncflag) {
	if(!asyncflag) {
		firebase.database().ref('customers/'+currentUID).on('value', function(customersnap) {
			console.log("customer refresh");
			customer = customersnap.val();
			fireflag.customer=true;
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();

			}
		});
	}else {
		init_cart_goods();
		print_cart_goods();
		print_cart_prices();
	}
	function init_cart_goods() {
		customer.calcprice={
			sum:0,
			point:0,
			shipping:2500,
			echo:0,
			total:0,
			net:0,
			count:0
		};
		
		init_available_dates();
		
		function init_available_dates() {
			datelist = {};
			for(var i in customer.carts) {
				if(i!=='current' && i!=='timestamp') {
					for(var j in json.goods_info.goods_info[i].dayoff) {
						if(json.goods_info.goods_info[i].dayoff[j]) {
							datelist[j] = true;
						}
					}
				}
			}
		}
		init_shipping_date();
		function init_shipping_date() {
			if(realtime_date(customer.carts)) {
				selecteddate = customer.carts.current;
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(customer.carts.current);
					firebase.database().ref('customers/'+currentUID+'/carts/current').set(selecteddate);
				}
			}
			else if(!selecteddate) {
				var startdate = new Date();
				startdate.setHours(startdate.getHours()+9);
				if(startdate.getUTCHours()>16) {startdate.setDate(startdate.getDate()+1);}
				selecteddate = getNextDate(startdate.toISOString());
			}else {
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(selecteddate);
				}
			}
			function realtime_date(targetCart) {
				var pastDate = new Date();
				pastDate.setDate(pastDate.getDate()+1);
				if(!firebase.auth().currentUser) {return false;}
				var targetUID = firebase.auth().currentUser.uid;
				if(!targetUID) { return false;}
				if(targetCart) {
					if(targetCart.current!=0) {
						if(targetCart.current.toString().indexOf('(')!=-1) {
							if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
								firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
								return false;
							}
							return true;
						}
					}
				}return false;
			}
			function getNextDate(target) {
				var returndate = new Date(target.substring(0,10));
				var findflag = false;
				for(var i=0;i<50;i++) {
					returndate.setDate(returndate.getDate()+1);
					if(!datelist[returndate.toISOString().substring(0,10)]) { findflag=true; break; }
				}
				if(!findflag) { return target; }
				return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
			}
			function getKorDay(target) {
				if(target===0) {return '일';}
				if(target===1) {return '월';}
				if(target===2) {return '화';}
				if(target===3) {return '수';}
				if(target===4) {return '목';}
				if(target===5) {return '금';}
				if(target===6) {return '토';}
				return '일';
			}
		}
	}
	function print_cart_goods() {
		var cartHTML = '';
		var itemCount = 0;
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					if(customer.carts[i][j]) {
						var option = Number(j.toString().substring(1))-1;
						var key = i;
						var count = customer.carts[i][j].count;
						var itemHTML = '<div class="cart_item">'+
										'<div class="citem-name"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						itemCount++;
						cartHTML+=itemHTML;
						customer.calcprice.sum+=json.goods_info.goods_info[key].standardprice*count;
						customer.calcprice.count+=count;
					}
				}
			}
		}
		document.getElementById('cart_list').innerHTML = cartHTML;
		function goodsName(target) {
			return json.goods_info.goods_info[target].standardname;
		}
		function goodsOption(target,option) {
			return json.goods_info.goods_info[target].options[option].name;
		}
		function goodsPrice(target,count) {
			return numberWithWon(json.goods_info.goods_info[target].standardprice*count);
		}
	}
	function print_cart_prices() {
		if(customer.calcprice.count<2) {customer.calcprice.echo = 0;}
		else if(customer.calcprice.count<7) {customer.calcprice.echo=(customer.calcprice.count-1)*500;}
		else {customer.calcprice.echo=2500;}
		customer.calcprice.total = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo;
		document.getElementById('cart_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('cart_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('cart_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('cart_total').innerHTML = numberWithWon(customer.calcprice.total);
		
		document.getElementById('cart_shippingdate').innerHTML = selecteddate;
		
		if(customer.calcprice.count==0) {
			document.getElementById('cart_empty').style.display="block";
		} else {
			document.getElementById('cart_empty').style.display="none";
		}
	}
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}
loadReview();


window.onscroll=function(){
  	var scrollHeight = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
   	var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
   	var clientHeight = document.documentElement.clientHeight;
   	if((scrollTop+clientHeight) >= scrollHeight-100){
   	   	setTimeout(function(){loadMoreReview();},500);
   	}
}
var reviewList = new Array();
var startIndex;
var topIndex = 0;
var botIndex = 12;
var endIndex;
var openedCategory = new Array();

function loadReview() {
  	var target = "https://jyg-custom.firebaseio.com/reviews.json";
  	var returnJson;
  	var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			var reviewBox = JSON.parse(this.responseText);
			loadReviewQ(reviewBox.reviews);
    	}
  	};
  	xhttp.open("GET", target, true);
  	xhttp.send();
}
var rCount = 0;
var isFirstLoaded = false;
function loadReviewQ(reviewBox) {

	document.getElementById('reviewlist').innerHTML = '';
	for(key in reviewBox) {
		if(reviewBox[key].published) {
			if(reviewBox[key].reference == "none"){
				reviewList.push(reviewBox[key]);
				rCount++;
			}

		}
	}
	reviewList.sort(function(a,b) {
		if(a.postDate === b.postDate){
			if(a.writerID === b.writerID){ return 0; }
			return  a.writerID < b.writerID ? 1 : -1;
		} return  a.postDate < b.postDate ? 1 : -1;
	});
	loadReviewPart();
}

function loadMoreReview() {
	if(!isFirstLoaded) {return;}
	if(topIndex == 0) {
		topIndex += 12;
		botIndex += 8;
	}
	else {
		topIndex += 8;
		botIndex += 8;
	}
	loadReviewPart();
}

function loadReviewPart() {
	isFirstLoaded = true;
	for(var i=topIndex;i<botIndex;++i) {
		print_review(reviewList[i], i, rCount);
	}
}

function print_review(reviewList, reviewCount, rCount) {
	if(!reviewList) {return;}
	var referenceText = '';
	var reviewGrid = '';
	var reviewct = '';
	var ctt = '';
	if(reviewList.pid == 'chicken-whole' || reviewList.pid == 'chicken-cut' || reviewList.pid == 'chicken-leg' || reviewList.pid == 'chicken-inner' || reviewList.pid == 'chicken-breast') {
		reviewct = '닭고기';
		ctt = 'ctchicken';
	}
	else if(reviewList.pid == 'porkbelly-fresh' || reviewList.pid == 'porkbelly-clean') {
		reviewct = '삼겹살';
		ctt = 'ctporkbelly';
	}
	else if(reviewList.pid == 'porkneck-fresh' || reviewList.pid == 'porkneck-clean') {
		reviewct = '목살';
		ctt = 'ctporkneck';
	}
	else if(reviewList.pid == 'egg-fresh') {
		reviewct = '달걀';
		ctt = 'ctegg';
	}else {
		reviewct = '미분류';
		ctt = 'ctporkbelly';
	}

	if(reviewList.reference=="qualified") { referenceText = '네이버'; }
	else if(reviewList.reference=="none") { referenceText = reviewList.post; }
	else if(reviewList.reference=="naver_blog") { referenceText = '네이버'; }
	else if(reviewList.reference=="facebook_post") { referenceText = '페이스북'; }
	else if(reviewList.reference=="instagram_post") { referenceText = '인스타'; }
	
	var hideReviewItem = '';
	for(var i=0; i<document.getElementsByName('tag').length; ++i) {
		if(document.getElementsByName('tag')[i].value==ctt) {
			if(document.getElementsByName('tag')[i].checked == false) {
				hideReviewItem = 'style="display:none"';
			}
		}
	}
	var reviewSRC = reviewList.mainImage;
	if(!reviewList.mainImage) {
		if(window.location.pathname == '/review.php') {
			reviewSRC = 'img/review/empty-'+reviewList.pid+'.jpg';
		}else {
			reviewSRC = 'img/review/m_empty-'+reviewList.pid+'.jpg';
		}
	}
	if(window.location.pathname == '/review.php') {
		reviewGrid = '<div class="col-xs-12 '+ctt+'" '+hideReviewItem+'>';
		var reviewItem = reviewGrid+'<button id="review_open_'+reviewCount+'" class="modal-trigger"><div class="card"><span class="card-number">'+(rCount-reviewCount)+'</span><span class="card-category">'+reviewct+'</span><img src="'+reviewSRC+'" width="100px" height="75px" /><span class="card-aa">'+reviewList.title+'</span><span class="card-title">'+reviewList.postDate+'</span><span class="card-writer">'+reviewList.writerName+'</span></button></div>';
		document.getElementById('reviewlist').insertAdjacentHTML('beforeend',reviewItem);
		
		document.getElementById('review_open_'+reviewCount).onclick = function() {
			open_dialog('dialog_review');
			document.getElementById('dialog_review-title').innerHTML = '['+reviewct+'] '+reviewList.title;
			document.getElementById('dialog_review-writer').innerHTML = '작성자 | '+reviewList.writerName+'&nbsp;&nbsp;'+'작성일 | '+reviewList.postDate;
			var contentHTML = '';
			if(reviewList.reference=="instagram_post" || reviewList.reference=="facebook_post") {
				contentHTML = '<iframe src="'+reviewList.href+'" scrolling="no"></iframe>';
			}else {
				if(!reviewList.mainImage) {
					contentHTML = '<p style="white-space: pre-line; padding: 10px; text-align:left">'+referenceText+'</p>';
				}
				else {
					contentHTML = '<img src="'+reviewSRC+'" width="100%">'+'<p style="white-space: pre-line; padding: 10px; text-align:left">'+referenceText+'</p>';
				}
			}
			document.getElementById('dialog_review-content').innerHTML = contentHTML;
		}
	}
	if(window.location.pathname == '/review_m.php') {
		reviewGrid = '<div class="col-xs-12 '+ctt+'" '+hideReviewItem+'>';
		var reviewItem = reviewGrid+'<button type="button" id="review_open_'+reviewCount+'" class="modal-trigger"><div class="card"><div class="img-card"><img src="'+reviewSRC+'" /></div><div class="card-content"><span class="card-title">'+reviewList.postDate+'</span><span class="card-writer">'+reviewList.writerName+'</span><br><span class="card-post">'+referenceText+'</span></div></button></div>';
		reviewItem = reviewGrid+'<button type="button" id="review_open_'+reviewCount+'" class="modal-trigger"><div class="card"><div class="img-card"><img class="reviewThumb" src="'+reviewSRC+'" /></div><div class="card-content"><span class="card-title">'+reviewList.title+'</span><span class="card-date">'+reviewList.postDate+'</span><span class="card-writer">'+reviewList.writerName+'</span><br><span class="card-post">'+referenceText+'</span></div></button></div>';
		document.getElementById('reviewlist').insertAdjacentHTML('beforeend',reviewItem);
		
		setTimeout(function(){for(var m=0; m<document.getElementsByClassName('reviewThumb').length; m++) {
			console.log(document.getElementsByClassName('reviewThumb')[m].width);
			document.getElementsByClassName('reviewThumb')[m].style.height = document.getElementsByClassName('reviewThumb')[m].width + 'px';
		}},1);
		
		setTimeout(function(){for(var n=0; n<document.getElementsByClassName('card-content').length; n++) {
			console.log(window.innerHeight);
			document.getElementsByClassName('card-content')[n].style.top = ((65 - Number(document.getElementsByClassName('reviewThumb')[0].height))/2) + 'px';
		}},50);
		
		document.getElementById('review_open_'+reviewCount).onclick = function() {
			open_dialog('dialog_review');
			
			
			document.getElementById('dialog_review-title').innerHTML = '['+reviewct+'] '+reviewList.title;
			document.getElementById('dialog_review-writer').innerHTML = '작성자 | '+reviewList.writerName+'&nbsp;&nbsp;'+'작성일 | '+reviewList.postDate;
			var contentHTML = '';
			if(reviewList.reference=="instagram_post" || reviewList.reference=="facebook_post") {
				contentHTML = '<iframe src="'+reviewList.href+'" scrolling="no"></iframe>';
			}else {
				if(!reviewList.mainImage) {
					contentHTML = '<p style="white-space: pre-line; padding: 10px; text-align:left">'+referenceText+'</p>';
				}
				else {
					contentHTML = '<img src="'+reviewSRC+'" width="100%">'+'<p style="white-space: pre-line; padding: 10px; text-align:left">'+referenceText+'</p>';
				}
			}
			document.getElementById('dialog_review-content').innerHTML = contentHTML;
		}
	}


    for(var i=0; i<document.getElementsByName('tag').length; ++i) {
        document.getElementsByName('tag')[i].onclick = function(e) {
            var count = 0;
            if(document.getElementsByName('tag')[e.target.id].checked == false) {
                for(var j=0; j<document.getElementsByClassName(this.value).length; ++j) {
                    document.getElementsByClassName(this.value)[j].style.display = "none";
                }
            }
             if(document.getElementsByName('tag')[e.target.id].checked == true) {
                for(var k=0; k<document.getElementsByClassName(this.value).length; ++k) {
                    document.getElementsByClassName(this.value)[k].style.display = "block";
                }
            }
        }
    }
}

function openTabs(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}