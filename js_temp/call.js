$(function(){
	$("#agreement_usage").load("agreement/jyg_usage.html"); 
	$("#agreement_privacy").load("agreement/jyg_privacy.html");
});

function close_dialog_today(target) {
	function setCookie( name, value, expirehours ) { 
		var todayDate = new Date(); 
		todayDate.setHours( todayDate.getHours() + expirehours ); 
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";";
	}
	setCookie("ncookie","done",24);
	document.getElementById(target).style.display='none';
}
if(document.getElementById('dialog_notice')) {
	cookiedata = document.cookie;
	//if (cookiedata.indexOf("ncookie=done") < 0){  document.getElementById('dialog_notice').style.display = "block"; } 
	//else { document.getElementById('dialog_notice').style.display = "none"; }
}

document.addEventListener("DOMContentLoaded", function (event) {
	var _selector = document.querySelector('input[name=check_all]');
    _selector.addEventListener('change', function (event) {
        if (_selector.checked) {
            document.getElementById('check_age').checked = true;
			document.getElementById('check_usage').checked = true;
			document.getElementById('check_privacy').checked = true;
        } else {
            document.getElementById('check_age').checked = false;
			document.getElementById('check_usage').checked = false;
			document.getElementById('check_privacy').checked = false;
        }
    });
	
	var _selector2 = document.querySelector('select[name=detail_option]');
	if(_selector2) {
		_selector2.addEventListener('change', function (event) {
			select_option_detail();
		});
	}
	
	var _input3 = document.querySelector('input[name=payment_pointUsed]');
	if(_input3) {
		_input3.addEventListener('change', function (event) {
			input_point(_input3.value);
		});
	}
	
	var _option4 = document.querySelector('input[id=radio_address_1]');
	var _option5 = document.querySelector('input[id=radio_address_2]');
	if(_option4 && _option5) {
		_option4.addEventListener('change', function (event) {
			select_address(_option4.value);
		});
		_option5.addEventListener('change', function (event) {
			select_address(_option5.value);
		});
	}
	
	var _option6 = document.querySelector('input[id=radio_method_1]');
	var _option7 = document.querySelector('input[id=radio_method_2]');
	if(_option6 && _option7) {
		_option6.addEventListener('change', function (event) {
			select_method(_option6.value);
		});
		_option7.addEventListener('change', function (event) {
			select_method(_option7.value);
		});
	}
	
	
	var _input8 = document.querySelector('select[name=card_option]');
	if(_input8) {
		_input8.addEventListener('change', function (event) {
			input_card(_input8.value);
		});
	}
	
	
	var _option9 = document.querySelector('input[id=radio_refund_1]');
	var _option10 = document.querySelector('input[id=radio_refund_2]');
	if(_option9 && _option10) {
		_option9.addEventListener('change', function (event) {
			select_refund(_option9.value);
		});
		_option10.addEventListener('change', function (event) {
			select_refund(_option10.value);
		});
	}
	
	
	var _option11 = document.querySelector('input[id=radio_tax_1]');
	var _option12 = document.querySelector('input[id=radio_tax_2]');
	if(_option11 && _option12) {
		_option11.addEventListener('change', function (event) {
			select_tax(_option11.value);
		});
		_option12.addEventListener('change', function (event) {
			select_tax(_option12.value);
		});
	}
	
	
	var _option13 = document.querySelector('input[id=radio_own_1]');
	var _option14 = document.querySelector('input[id=radio_own_2]');
	if(_option13 && _option14) {
		_option13.addEventListener('change', function (event) {
			select_own(_option13.value);
		});
		_option14.addEventListener('change', function (event) {
			select_own(_option14.value);
		});
	}
});
$(document).mouseup(function (e)
{
    var container = $("#sidebar_main");
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
//		document.getElementById('sidebar-shadow').style.display='none';
		removeClass(document.getElementById('sidebar_cart'),'sidebar_active');
    }
	var container2 = $("#sidebar_sign");

    if (!container2.is(e.target) && container2.has(e.target).length === 0 && !container.is(e.target) && container.has(e.target).length === 0)
    {
		document.getElementById('sidebar-shadow').style.display='none';
		removeClass(document.getElementById('sidebar_signin'),'sidebar_active');
    }
	
	var container3 = $("#detail-control-inner");

    if (!container3.is(e.target) && container3.has(e.target).length === 0)
    {
		if(document.getElementById('detail-control')) {
        	document.getElementById('detail-control').style.display='none';
		}
    }
});

var scrollCheck;
var lastScrollTop = 0;
window.addEventListener("scroll", function(){
   var st = window.pageYOffset || document.documentElement.scrollTop;
   if (st > lastScrollTop){
       scrollCheck = true;
   } else {
	   if(st < lastScrollTop) {
		   scrollCheck = true;
	   }
   }
   lastScrollTop = st;
}, false);
setInterval(function() {
	if(document.getElementById('following_nav') !== null) {
		if (scrollCheck) {
			document.getElementById('following_nav').style.position = 'fixed';
			document.getElementById('following_nav').style.top = '0px';
			scrollCheck = false; 
		}
		else {
			document.getElementById('following_nav').style.position = 'absolute';
			document.getElementById('following_nav').style.top = document.body.scrollTop+'px';
		}
	}
}, 250);

// JavaScript Document
function signin() {
	var signin_id = document.getElementById('customer_id').value;
	var signin_pw = document.getElementById('customer_pw').value;
	document.getElementById('customer_pw').value="";
	firebase.auth().signInWithEmailAndPassword(signin_id, signin_pw).catch(function(error) {
	  var errorCode = error.code;
	  var errorMessage = error.message;
		if(errorCode=="auth/invalid-email") {alert("아이디 혹은 비밀번호가 잘못되었습니다.");}
		else if(errorCode=="auth/user-disabled") {alert("가입되지 않은 사용자입니다.");}
		else if(errorCode=="auth/user-not-found") {alert("가입되지 않은 사용자입니다.");}
		else if(errorCode=="auth/wrong-password") {alert("아이디 혹은 비밀번호가 잘못되었습니다.");}
		else {alert(errorCode+errorMessage);}
	});
	close_sidebar('sidebar_signin');
}
function signup() {
	open_dialog('dialog_agreement');
}
function signout() {
	firebase.auth().signOut().then(function() {}, function(error) {});
}
function findpw() {
	console.log("findpw");
	var findid = document.getElementById('customer_id').value;
	if(!findid) {alert("비밀번호를 찾으실 아이디를 입력해주세요.");}
	else {
		firebase.auth().sendPasswordResetEmail(findid).then(function() {}, function(error) {});
		alert("입력하신 아이디로 비밀번호재설정 메일이 발송되었습니다.");
	}
}
function close_dialog(target) {
	document.body.style.overflow = 'auto';
	document.getElementById(target).style.display='none';
}
function open_dialog(target) {
	document.body.style.overflow = 'hidden';
	document.getElementById(target).style.display='block';
}
var sms={};
function getSMSCode() {return sms;}
function send_code() {
	var check_age = document.getElementById('check_age').checked;
	var check_usage = document.getElementById('check_usage').checked;
	var check_privacy = document.getElementById('check_privacy').checked;
	if(check_age&&check_usage&&check_privacy) {
		var userInput = document.getElementById('user_phone').value;
		firebase.database().ref('customersPhone/phone/'+userInput).once('value', function(phonesnap) {
			var phoneExist = phonesnap.val();
			if(!phoneExist) {
				sendSMS();
			}else {
				alert("이미 가입된 전화번호입니다.");
			}
		});
	}else { alert("모든 약관에 동의해주세요."); } // talespark
	function sendSMS() {
		var smsHEAD = 'http://api.coolsms.co.kr/sendmsg?user=jeongyookgak&password=%200297a0d059a15401faaacd423e7a4491&enc=MD5&to=';
		sms.name=document.getElementById('user_name').value;
		sms.phone=document.getElementById('user_phone').value;
		sms.code = createCertification();
		//console.log(sms.code);
		var smsAction = smsHEAD + sms.phone + "&from=18000658&text=[(주)정육각] 본인인증번호는 "+sms.code+" 입니다. 정확히 입력해주세요.";
		var iframe = document.getElementById('smsFrame');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
		var sms_form=innerDoc.getElementById('sms_form');
		sms_form.action=smsAction; sms_form.submit();
		document.getElementById('user_code').style.display = 'block';
	}
	function createCertification() { return Math.floor(Math.random() * (900000)) + 100000;	}
}
function check_code() {
	if(document.getElementById('user_phone').value==sms.phone && document.getElementById('user_code').value==sms.code) {
		close_dialog('dialog_agreement');
		next_signup();
	}else { alert("인증번호가 맞지 않습니다. 다시 확인해주세요."); }
	function next_signup() {
		open_dialog('dialog_signup');
		document.getElementById('signup_confirm_btn').onclick = signup_confirm;
	}
}

function link_payment() {
	location.href='payment.php';
}
function link_review() {
	location.href='review.php';
}
function link_support() {
	location.href='support.php';
}
function link_mainevent() {
	location.href='eventdetail.php?code=jygday';
}
function link_noticedetail(target) {
	location.href='notice.php?code='+target;
}




function open_sidebar(target) {
	if(getFirebase_init()) {return;}
	if(target=='sidebar_signin' && firebase.auth().currentUser) {
		location.href = 'mypage.php';
	}
	else if(target=='sidebar_cart' && !firebase.auth().currentUser) {
		alert("로그인해주세요.");
		document.getElementById('sidebar_signin').className += " sidebar_active";
		document.getElementById('sidebar-shadow').style.display="block";
	}
	else {
		console.log(target,"1");
		document.getElementById(target).className += " sidebar_active";
		document.getElementById('sidebar-shadow').style.display="block";
	}
}
function close_sidebar(target) {
	document.getElementById('sidebar-shadow').style.display='none';
	removeClass(document.getElementById(target),'sidebar_active');
}

function removeClass(element, className) { 
	var check = new RegExp("(\\s|^)" + className + "(\\s|$)");
	element.className = element.className.replace(check, " ").trim();
}




function show_options(target) {
	show_goodsimages(target);
	document.getElementById(target+'-wrap').style.display='block';
}
function hide_options(target) {
	hide_goodsimages(target);
	document.getElementById(target+'-wrap').style.display='none';
}
function show_goodsimages(target) {
	document.getElementById(target+'-src').src='img/good_index/'+target+'-hover.jpg';
}
function hide_goodsimages(target) {
	document.getElementById(target+'-src').src='img/good_index/'+target+'-index.jpg';
}


function date_prior() {
	changedate(false);
}
function date_next() {
	changedate(true);
}

function track_code() {
	window.open( 'http://distribution.ekape.or.kr/user/one/combine/combineSearch.do?searchKeywor'+document.getElementById('tracking_code').value,'_blank');
}
function link_index() {
	location.href = 'index.php';
}
function link_mypage() {
	location.href = 'mypage.php';
}
function link_eventdetail(qstring) {
	location.href = 'eventdetail.php'+'?code='+qstring;
}


function select_less_detail() {
	addselect_detail(-1);
}
function select_more_detail() {
	addselect_detail(1);
}
function select_add_detail() {
	var failed = addToCart_detail();
	if(!failed) {open_sidebar('sidebar_cart');}
	else {open_sidebar('sidebar_signin');}
}
function select_option_detail() {
	addoption_detail();
}


function select_less(target) {
	addselect(target,-1);
}
function select_more(target) {
	addselect(target,1);
}
function select_detail(target) {
	location.href = 'detail.php?goods='+target;
}
function select_add(target) {
	var failed = addToCart(target);
	if(!failed) {open_sidebar('sidebar_cart');}
	else {open_sidebar('sidebar_signin');}
}
function select_option(target,option) {
	addoption(target,option);
}
function direct_less(target,option) {
	directToCart('less',target,option);
}
function direct_more(target,option) {
	directToCart('more',target,option);
}
function direct_remove(target,option) {
	directToCart('remove',target,option);
}


function madeOrder() {
	if(document.getElementById('check_payment').checked) {
		requestOrder();
	}else {
		alert("결제진행에 동의해주세요.");
	}
}

function add_card() {
	request_batch_key();
}
function remove_card(target) {
	var c;
	c = confirm("정말로 카드를 삭제하시겠습니까?");
	if(c) {cardDelete(target);}
}

function open_mypage(target) {
	document.getElementById('mypage_order').style.display = 'none';
	document.getElementById('mypage_point').style.display = 'none';
	document.getElementById('mypage_coupon').style.display = 'none';
	document.getElementById('mypage_address').style.display = 'none';
	document.getElementById('mypage_card').style.display = 'none';
	document.getElementById('mypage_bank').style.display = 'none';
	if(document.getElementById('mypage_cms')) {
		document.getElementById('mypage_cms').style.display = 'none';
	}
	if(document.getElementById('mypage_code')) {
		document.getElementById('mypage_code').style.display = 'none';
	}
	if(document.getElementById('mypage_pointcode')) {
		document.getElementById('mypage_pointcode').style.display = 'none';
	}
	if(document.getElementById('mypage_detail')) {
		document.getElementById('mypage_detail').style.display = 'none';
	}
	document.getElementById(target).style.display = 'block';
}
function open_support(target) {
	clear_faqfilter();
	print_faq();
	document.getElementById('support_notice').style.display = 'none';
	document.getElementById('support_ask').style.display = 'none';
	document.getElementById('support_faq').style.display = 'none';
	document.getElementById(target).style.display = 'block';
}
function open_support(target,faq_category) {
	set_faqfilter(faq_category);
	print_faq();
	document.getElementById('support_notice').style.display = 'none';
	document.getElementById('support_ask').style.display = 'none';
	document.getElementById('support_faq').style.display = 'none';
	document.getElementById(target).style.display = 'block';
}
function submit_question() {
	save_question();
}
function open_faq_item(target) {
	if(document.getElementById('faq_item_'+target).style.display=='block') {
		document.getElementById('faq_item_'+target).style.display='none';
		document.getElementById('faq_plus_'+target).style.display='block';
		document.getElementById('faq_minus_'+target).style.display='none';
	}
	else {
		document.getElementById('faq_item_'+target).style.display='block';
		document.getElementById('faq_plus_'+target).style.display='none';
		document.getElementById('faq_minus_'+target).style.display='block';
	}
}



function detail_menu_open(target) {
	if(target.indexOf('pork')!=-1) {target = 'pork';}
	if(document.getElementById('detail_menu_'+target+'_group')) {
		document.getElementById('detail_menu_pork_group').style.display='none';
		document.getElementById('detail_menu_chicken_group').style.display='none';
		document.getElementById('detail_menu_egg_group').style.display='none';
		document.getElementById('detail_menu_'+target+'_group').style.display='block';
		
		if(document.getElementById('detail_menu_pork')) {
			document.getElementById('detail_menu_pork').style.color='#222222';
			document.getElementById('detail_menu_chicken').style.color='#222222';
			document.getElementById('detail_menu_egg').style.color='#222222';
			document.getElementById('detail_menu_'+target).style.color='#90312b';
		}
	}
}
var detail_menu_page=0;
var detail_menu_max=3;
function detail_menu_prev() {
	detail_menu_page--;
	
	if(detail_menu_page==0) {detail_menu_open('pork');}
	else if(detail_menu_page==1) {detail_menu_open('chicken');}
	else if(detail_menu_page==2) {detail_menu_open('egg');}
	else {detail_menu_page=detail_menu_max-1; detail_menu_open('egg');}
}
function detail_menu_next() {
	detail_menu_page++;
	
	if(detail_menu_page==0) {detail_menu_open('pork');}
	else if(detail_menu_page==1) {detail_menu_open('chicken');}
	else if(detail_menu_page==2) {detail_menu_open('egg');}
	else {detail_menu_page=0; detail_menu_open('pork');}
}

function generate_code() {
	gene_code();
}


function apply_coupon(target) {
	if(target=='blocked') {
		alert('쿠폰 사용조건을 확인해주세요.');
	}else {
		set_selectedcoupon(target);
		close_dialog('dialog_coupon');
	}
}
function release_coupon() {
	set_selectedcoupon(null);
	close_dialog('dialog_coupon');
}

