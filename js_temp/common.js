function realtime_date(targetCart) {
	var pastDate = new Date();
	pastDate.setDate(pastDate.getDate()+1);
	var targetUID = firebase.auth().currentUser.uid;
	if(!targetUID) { return false;}
	if(targetCart) {
		if(targetCart.current!=0) {
			if(targetCart.current.toString().indexOf('(')!=-1) {
				if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
					firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
					return false;
				}
				return true;
			}
		}
	}return false;
}
function changedate(isNext) {
	var gselecteddate = get_selecteddate();
	if(isNext) {
		gselecteddate = getOtherDate(gselecteddate,1);
	} else {
		gselecteddate = getOtherDate(gselecteddate,-1);
	}
	document.getElementById('cart_shippingdate').innerHTML = gselecteddate;
	if(document.getElementById('payment_shippingdate')) {
		document.getElementById('payment_shippingdate').innerHTML = gselecteddate;
	}
	realtimeWrite();
	function realtimeWrite() {
		var retUID = firebase.auth().currentUser.uid;
		if(retUID) {
			firebase.database().ref('customers/'+retUID+'/carts/current').set(gselecteddate);
		}
	}
	function getOtherDate(target,direction) {
		var returndate = new Date(target.substring(0,10));
		var findflag = false;
		for(var i=0;i<40;i++) {
			returndate.setDate(returndate.getDate()+direction);
			if(!datelist[returndate.toISOString().substring(0,10)]) { findflag=true; break; }
		}
		var compareDate = new Date();
		compareDate.setHours(compareDate.getHours()+9);
		if(compareDate.getUTCHours()>16) { compareDate.setDate(compareDate.getDate()+1); }
		if(returndate<compareDate) {findflag=false;}
		compareDate.setDate(compareDate.getDate()+40);
		if(returndate>compareDate) {findflag=false;}
		if(!findflag) { return target; }
		return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
	}
	function getKorDay(target) {
		if(target===0) {return '일';}
		if(target===1) {return '월';}
		if(target===2) {return '화';}
		if(target===3) {return '수';}
		if(target===4) {return '목';}
		if(target===5) {return '금';}
		if(target===6) {return '토';}
		return '일';
	}
	set_selecteddate(gselecteddate);
}


function addselect_detail(count) {
	var target = get_goodkey();
	var gjson = get_json();
	if(document.getElementById('detail_quantity') && target) {
		if(gjson.goods_info.goods_info[target].selectCount+count<1) {
			gjson.goods_info.goods_info[target].selectCount=1;
		}else if(gjson.goods_info.goods_info[target].selectCount+count>99) {
			gjson.goods_info.goods_info[target].selectCount=99;
		}else {
			gjson.goods_info.goods_info[target].selectCount+=count
		}
		document.getElementById('detail_quantity').innerHTML = gjson.goods_info.goods_info[target].selectCount;
		document.getElementById('detail_price').innerHTML = numberWithWon(gjson.goods_info.goods_info[target].selectCount * gjson.goods_info.goods_info[target].standardprice);
	}
	set_json(gjson);
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}
function addselect(target,count) {
	var gjson = get_json();
	if(document.getElementById(target+'-scount')) {
		if(gjson.goods_info.goods_info[target].selectCount+count<1) {
			gjson.goods_info.goods_info[target].selectCount=1;
		}else if(gjson.goods_info.goods_info[target].selectCount+count>99) {
			gjson.goods_info.goods_info[target].selectCount=99;
		}else {
			gjson.goods_info.goods_info[target].selectCount+=count
		}
		document.getElementById(target+'-scount').innerHTML = gjson.goods_info.goods_info[target].selectCount;
	}
	set_json(gjson);
}

function addoption_detail() {
	if(!document.getElementById('detail_option')) {return;}
	var target = get_goodkey();
	var option = document.getElementById('detail_option').value;
	if(option<0) { return; }
	var gjson = get_json();
	gjson.goods_info.goods_info[target].selectOption=option;
	set_json(gjson);
}

function addoption(target,option) {
	var gjson = get_json();
	if(document.getElementById(target+'-op'+option)) {
		gjson.goods_info.goods_info[target].selectOption=option;
		for(var i=0;i<5;i++) {
			if(document.getElementById(target+'-op'+i)){
				if(i==option) {
					document.getElementById(target+'-op'+i).style.backgroundColor="#FFF";
					document.getElementById(target+'-op'+i).style.color="#000";
				}else {
					document.getElementById(target+'-op'+i).style.backgroundColor=null;
					document.getElementById(target+'-op'+i).style.color="#FFF";
				}
			}
		}
	}
	set_json(gjson);
}

function addToCart_detail() {
	var target = get_goodkey();
	var gjson = get_json();
	var currentUID = firebase.auth().currentUser;
	if(!currentUID) { alert("로그인해주세요."); return true; }
	if(!target) {return;}
	currentUID = currentUID.uid;
	var toption = Number(gjson.goods_info.goods_info[target].selectOption)+1;
	if(validation()) {
		firebase_transaction();
	}
	function validation() {
		if(!gjson.goods_info.goods_info[target].options[gjson.goods_info.goods_info[target].selectOption]) { return false;}
		if(isNaN(gjson.goods_info.goods_info[target].selectCount)) {return false;}
		if(gjson.goods_info.goods_info[target].selectCount<1) {return false;}
		if(gjson.goods_info.goods_info[target].selectCount>99) {return false;}
		return true;
	}
	function firebase_transaction() {
		//console.log('customers/'+currentUID+'/carts/'+target+'/'+'a'+toption);
		firebase.database().ref('customers/'+currentUID+'/carts/'+target+'/'+'a'+toption).transaction(function(post) {
			if (post) {
				post.count+=gjson.goods_info.goods_info[target].selectCount;
				post.timestamp=new Date().toISOString();
			}else {
				post={
					count:gjson.goods_info.goods_info[target].selectCount,
					timestamp:new Date().toISOString()
				};
			}
			return post;
		});
	}
}
function addToCart(target) {
	var gjson = get_json();
	var currentUID = firebase.auth().currentUser;
	if(!currentUID) { alert("로그인해주세요."); return true; }
	currentUID = currentUID.uid;
	var toption = Number(gjson.goods_info.goods_info[target].selectOption)+1;
	if(validation()) {
		firebase_transaction();
	}
	function validation() {
		if(!gjson.goods_info.goods_info[target].options[gjson.goods_info.goods_info[target].selectOption]) { return false;}
		if(isNaN(gjson.goods_info.goods_info[target].selectCount)) {return false;}
		if(gjson.goods_info.goods_info[target].selectCount<1) {return false;}
		if(gjson.goods_info.goods_info[target].selectCount>99) {return false;}
		return true;
	}
	function firebase_transaction() {
		//console.log('customers/'+currentUID+'/carts/'+target+'/'+'a'+toption);
		firebase.database().ref('customers/'+currentUID+'/carts/'+target+'/'+'a'+toption).transaction(function(post) {
			if (post) {
				post.count+=gjson.goods_info.goods_info[target].selectCount;
				post.timestamp=new Date().toISOString();
			}else {
				post={
					count:gjson.goods_info.goods_info[target].selectCount,
					timestamp:new Date().toISOString()
				};
			}
			return post;
		});
	}
}


function directToCart(type,target,toption) {
	var gcustomer = get_customer();
	var currentUID = firebase.auth().currentUser;
	if(!currentUID) { alert("로그인해주세요."); return true; }
	currentUID = currentUID.uid;
	toption = Number(toption)+1;
	if(type=="less") { if(gcustomer.carts[target]['a'+toption].count<2) { clearitem(); } else {changeitem(-1);} }
	else if(type=="more") { changeitem(1); }
	else if(type=="remove") { clearitem(); }
	
	function changeitem(count) {
		console.log("change item",'customers/'+currentUID+'/carts/'+target+'/'+toption);
		firebase.database().ref('customers/'+currentUID+'/carts/'+target+'/'+'a'+toption).transaction(function(post) {
			if (post) {
				post.count+=count;
				post.timestamp=new Date().toISOString();
			}else {
				post={
					count:count,
					timestamp:new Date().toISOString()
				};
			}
			return post;
		});
	}
	function clearitem() {
		console.log("clear item",'customers/'+currentUID+'/carts/'+target+'/'+toption);
		firebase.database().ref('customers/'+currentUID+'/carts/'+target+'/'+'a'+toption).set(null);
	}
}


function signup_confirm() {
	var sms = getSMSCode();
	var signupinfo = {};
	signupinfo.name = sms.name;
	signupinfo.phone = sms.phone;
	signupinfo.email = document.getElementById('user_id').value;
	signupinfo.pw = document.getElementById('user_pw').value;
	signupinfo.pwpw = document.getElementById('user_pwpw').value;
	if(validation()) {
		firebase_createauth();
	}
	function validation() {
		var returnFlag = true;
		if(signupinfo.pw!==signupinfo.pwpw) { alert("비밀번호를 다르게 입력하셨습니다. 다시 입력해주세요."); returnFlag=false; }
		if(signupinfo.pw.length<8) { alert("최소 8자 이상의 비밀번호를 입력해주세요."); returnFlag=false; }
		document.getElementById('user_pw').value=null;
		document.getElementById('user_pwpw').value=null;
		if(signupinfo.email.indexOf('@')===-1 || signupinfo.email.indexOf('.')===-1) { alert("올바른 이메일주소를 입력해주세요."); returnFlag=false; }
		return returnFlag;
	}
	function firebase_createauth() {
		firebase.auth().createUserWithEmailAndPassword(signupinfo.email, signupinfo.pw).then(function(user) {
			var user = firebase.auth().currentUser;
			create_userinfo(user.uid);
		}, function(error) {
			var errorCode = error.code;
			var errorMessage = error.message;
			if(errorCode == "auth/email-already-in-use") { alert('이미 가입되어 있는 이메일주소입니다.'); }
			else if(errorCode == "auth/invalid-email") { alert('올바르지 않은 이메일주소입니다'); }
			else if(errorCode == "auth/weak-password") { alert('최소 6자 이상의 비밀번호를 입력해주십시오.'); }
			else { alert("error!! " + errorCode + " / " + errorMessage); }
		});
		function create_userinfo(getuid) {
			var start_point=3000;
			var userinfo = {
				username: signupinfo.name, userphone: signupinfo.phone, useremail: signupinfo.email,
				activate: true, agreeSMS: false, agreeEMAIL: false, submits:{ trypay:"ready", type:0, option:0 },
				payments: { batch_1: { batchcard: "", batchpublic: "", batchsecret: "", timeconfirm: "", timelastuse: "" } },
				rewards: { coupons: { current: "0", count: "0" },
					points: {current: start_point, count: "1" },
					points_history: { 0: { changed:((-1)*(start_point)), title:"신규가입 환영", timestamp: new Date().toISOString() } },
					levels: { status: "green",start: "", expire: "" }
				},
				profile: {birth:"",gender:"",children:"",familysize:"",cook:""},
				order_records: { current:{ count:"0",cost:"0",last:"0" } },
				order_summaries: { count: 0, activecount:0 },
				carts: { current:{ count: 0 } },
				cards: { current: { count: 0,last: 0 }},
				addresses: { current: { count: 0,last: 0 }}
			};
			if(getuid) {
				firebase.database().ref('customers/' + getuid).set(userinfo).then(function(newuser) {
					alert("반갑습니다. "+signupinfo.name+"님 회원가입이 완료되었습니다.");
					close_dialog('dialog_signup');
				});
			}
		}
	}
}
function select_address(flag) {
	if(flag==0) {
		document.getElementById('saved_address').style.display = 'block';
		document.getElementById('input_address').style.display = 'none';
	}else {
		document.getElementById('saved_address').style.display = 'none';
		document.getElementById('input_address').style.display = 'block';
	}
}
function select_method(flag) {
	if(flag==0) {
		document.getElementById('card_method').style.display = 'block';
		document.getElementById('direct_method').style.display = 'none';
	}else {
		document.getElementById('card_method').style.display = 'none';
		document.getElementById('direct_method').style.display = 'block';
	}
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	if(returnUID) {
		init_cart_listener(returnUID,true);
	}
}
function select_refund(flag) {
	if(flag==0) {
		document.getElementById('point_refund').style.display = 'block';
		document.getElementById('bank_refund').style.display = 'none';
	}else {
		document.getElementById('point_refund').style.display = 'none';
		document.getElementById('bank_refund').style.display = 'block';
	}
}
function select_tax(flag) {
	if(flag==0) {
		document.getElementById('get_tax').style.display = 'none';
	}else {
		document.getElementById('get_tax').style.display = 'block';
	}
}
function select_own(flag) {
	if(flag==0) {
		document.getElementById('person_own').style.display = 'block';
		document.getElementById('company_own').style.display = 'none';
	}else {
		document.getElementById('person_own').style.display = 'none';
		document.getElementById('company_own').style.display = 'block';
	}
}

function input_point(input_point) {
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	var returnValue = 0;
	var gcustomer = get_customer();
	input_point = Number(input_point);
	if(gcustomer && returnUID) {
		if(input_point<1) { returnValue = 0; }
		else if(Number(gcustomer.rewards.points.current)<input_point) {
			returnValue = Number(gcustomer.rewards.points.current);
		}else {
			returnValue = Math.floor(input_point);
		}
		document.getElementById('payment_pointUsed').value=returnValue;
		gcustomer.calcprice.point=returnValue;
		set_customer(gcustomer);
		init_cart_listener(returnUID,true);
	}
}

function input_card(card_type) {
	var gcustomer = get_customer();
	if(!gcustomer.cards[card_type]) { open_dialog('dialog_card'); }
	console.log("default try");
	if(gcustomer.selectedmethod) {
		gcustomer.selectedmethod.type=0;
		gcustomer.selectedmethod.card_option=card_type;
		gcustomer.selectedmethod.card_result=gcustomer.cards[card_type];
	}
	set_customer(gcustomer);
}

function save_question() {
	var user = firebase.auth().currentUser;
	if(!user) {return;}
	var returnUID = user.uid;
	if(!returnUID) {return;}
	var gcustomer = get_customer();
	var questionCount=0;
	var question;
	var tdate = new Date();
	parseQuestion();
	put_firebase();
	
	function parseQuestion() {
		tdate.setHours(tdate.getHours()+9);
		question={
			postDate:tdate.toISOString(),
			postTitle:document.getElementById('question_title').value,
			postDetail:document.getElementById('question_detail').value,
			replyDate:'none',
			replyDetail:""
		};
		for(var i in gcustomer.questions) {
			if(gcustomer.questions[i]) {
				questionCount++;
			}
		}
	}
	function put_firebase() {
		var clearflag=0;
		var questionIndex = {
			postDate:question.postDate,
			questionUID:returnUID
		};
		firebase.database().ref('customers/'+returnUID+'/questions/'+questionCount).set(question).then(function(newQ) {
			clearflag++;
			if(clearflag==2) { setClear(); }
		});
		firebase.database().ref('questions/list/'+tdate.getTime().toString()).set(questionIndex).then(function(newQ) {
			clearflag++;
			if(clearflag==2) { setClear(); }
		});
		function setClear() {
			alert("문의가 등록되었습니다.");
			close_dialog('dialog_question');
			document.getElementById('question_title').value="";
			document.getElementById('question_detail').value="";
		}
	}
}
function gene_code() {
	var user = firebase.auth().currentUser;
	if(!user) {alert("잠시후에 다시 시도해주세요");return;}
	var returnUID = user.uid;
	if(!returnUID) {alert("잠시후에 다시 시도해주세요");return;}
	var gcustomer = get_customer();
	if(gcustomer.usercode) {alert("이미 코드가 등록되어있습니다.");return;}
	var codelist;
	firebase.database().ref('customersCode').once('value', function(codesnap) {
		codelist = codesnap.val();
		if(!codelist) {codelist={};}
		var gcode = "Aa";
		var codecount=0;
		for(var i in codelist) {
			codecount++;
		}
		if(codecount<10000000) {gcode+='J';}
		if(codecount<1000000) {gcode+='M';}
		if(codecount<100000) {gcode+='p';}
		if(codecount<10000) {gcode+='K';}
		if(codecount<1000) {gcode+='s';}
		if(codecount<100) {gcode+='h';}
		if(codecount<10) {gcode+='j';}
		gcode+=codecount.toString();
		codelist[gcode] = returnUID;
		gcustomer.usercode = gcode;
		console.log(gcode);
		console.log(codelist);
		var clearflag=0;
		firebase.database().ref('customersCode/'+gcode).set(returnUID).then(function(newQ) {
			clearflag++;
			if(clearflag==2) { setClear(); }
		});
		firebase.database().ref('customers/'+returnUID+'/usercode').set(gcode).then(function(newQ) {
			clearflag++;
			if(clearflag==2) { setClear(); }
		});
		function setClear() {
			set_customer(gcustomer);
			alert("코드가 발급되었습니다.");
			//location.reload();
		}
	});
}
/////////////////////////////////////// 카드등록 ///////////////////////////////////////////
function request_batch_key() {
	var user = firebase.auth().currentUser;
	if(!user) {alert("잠시후에 다시 시도해주세요");return;}
	var returnUID = user.uid;
	if(!returnUID) {alert("잠시후에 다시 시도해주세요");return;}
	var cardinfo;
	var cardnick='카드';
	if(get_card()) {
		request_key();
	}
	function get_card() {
		if(document.getElementById('card_nick')) {
			if(!document.getElementById('card_nick').value || document.getElementById('card_nick').value=="") {
				alert("카드 별명을 입력해주세요.");
				return false;
			}
		}
		if(document.getElementById('card_number')) {
			cardinfo={
				CardNo:document.getElementById('card_number').value,
				ExpYear:document.getElementById('card_year').value,
				ExpMonth:document.getElementById('card_month').value,
				IDNo:document.getElementById('card_id').value,
				CardPw:document.getElementById('card_pw').value
			};
			cardnick = document.getElementById('card_nick').value;
			return true;
		}else { alert('잘못된 접근입니다.'); return false;}
	}
	function request_key() {
		$.post("/pay_nice/payment/keyrequest.php",cardinfo,
			   function(data, status){
					var result = JSON.parse(data);
					if(result.ResultCode=='F100') {
						var param = {
							ResultCode:result.ResultCode,
							ResultMsg:result.ResultMsg,
							BID:result.BID,
							CardName:result.CardName,
							CardCode:result.CardCode,
							LastCode:result.LastCode,
							AuthDate:result.AuthDate,
							CardCl: result.CardCl,
							res_msg:cardnick,
							cno:result.BID,	res_cd:"0000",order_no:"order_no",user_nm:"user_nm",amount:"amount",auth_no:"auth_no",tran_date:"tran_date",pnt_auth_no:"pnt_auth_no",pnt_tran_date:"pnt_tran_date",cpon_auth_no:"cpon_auth_no",cpon_tran_date:"cpon_tran_date",card_no:"card_no",issuer_cd:"issuer_cd",issuer_nm:"issuer_nm",acquirer_cd:"acquirer_cd",acquirer_nm:"acquirer_nm",install_period:"install_period",noint:"noint",bank_cd:"bank_cd",bank_nm:"bank_nm",account_no:"account_no",deposit_nm:"deposit_nm",expire_date:"expire_date",cash_res_cd:"cash_res_cd",cash_res_msg:"cash_res_msg",cash_auth_no:"cash_auth_no",cash_tran_date:"cash_tran_date",auth_id:"auth_id",billid:"billid",mobile_no:"mobile_no",ars_no:"ars_no",cp_cd:"cp_cd",used_pnt:"used_pnt",remain_pnt:"remain_pnt",pay_pnt:"pay_pnt",accrue_pnt:"accrue_pnt",remain_cpon:"remain_cpon",used_cpon:"used_cpon",mall_nm:"mall_nm",escrow_yn:"escrow_yn",complex_yn:"complex_yn",canc_acq_date:"canc_acq_date",canc_date:"canc_date",refund_date:"refund_date",pay_type:"pay_type"
						};
						try_firebase(param);
				  }
				  else {
			 		alert(result.ResultMsg);
				  }
			});
		function try_firebase(newcard) {
			var newkey = 0;
			for(var i in customer.cards) { if(i!='current'){newkey++;} }
			put_firebase(newkey);
			function put_firebase() {
				firebase.database().ref('customers/'+returnUID+'/cards/'+newkey).set(newcard).then(function(neworder) {
					alert("카드가 정상적으로 등록되었습니다.");
					close_dialog('dialog_card');
					clear_input();
				});
			}
		}
	}
	function clear_input() {
		document.getElementById('card_nick').value=null;
		document.getElementById('card_number').value=null;
		document.getElementById('card_year').value=null;
		document.getElementById('card_month').value=null;
		document.getElementById('card_id').value=null;
		document.getElementById('card_pw').value=null;
	}
}
/////////////////////////////////////// 주문하기 ///////////////////////////////////////////
function requestOrder() {
	var order={
		goods:{},
		histories:{},
		informations:{},
		payment:{},
		totalPrice:0
	};
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	var returnKey;
	var pjson = get_json();
	var pcustomer = get_customer();
	var pdate = get_selecteddate();
	//.log(pdate.substring(0,10),ssday);
	//pdate = pdate.substring(0,10)+' ('+ssday+')';
	
	//temp
	var validorder = false;
	if(!pjson || !pcustomer || !pdate) { alert("잠시 후 다시 시도해주세요."); return; }
	var is={
		direct:false,
		newaddress:false,
		directrefund:false,
		tax:false,
		personal:false
	};
	init_selection();
	parseorder();
	validation();
	put_firebase();
	function init_selection() {
		is.direct = document.getElementById('radio_method_2').checked;
		is.newaddress = document.getElementById('radio_address_2').checked;
		is.directrefund = document.getElementById('radio_refund_2').checked;
		is.tax = document.getElementById('radio_tax_2').checked;
		is.personal = document.getElementById('radio_own_1').checked;
	}
	function parseorder() {
		setOrderKey();
		add_analytics();
		getgoods();
		gethistories();
		getinformations();
		getpayment();
		getcoupon();
		gettotalPrice();
		getPoint();
		function setOrderKey() {
			var lastkey = -1;
			for(var key in pcustomer.order_records) {
				if(key!='current' && Number(key)>lastkey) {
					lastkey = Number(key);
				}
			}
			if(lastkey==-1) {returnKey=0;}
			else {returnKey = lastkey+1;}
		}
		function add_analytics() {
			ga('ecommerce:addTransaction', {
				'id': returnUID+'_'+returnKey,
				'affiliation': 'JYG_WEB'
			});
		}
		function getgoods() {
			var goods_totalCount = 0;
			for(var key in pcustomer.carts) {
				if(key!='current' && key!='timestamp') {
					if(pcustomer.carts[key]) {
						for(var toption in pcustomer.carts[key]) {
							if(pcustomer.carts[key][toption]) {
								var count = Number(pcustomer.carts[key][toption].count);
								var option = toption.substring(1);
								spreadgoods(key,option,count);
							}
						}
					}
				}
			}
			function spreadgoods(key,option,count) {
				/**for(var i=goods_totalCount;i<goods_totalCount+count;i++) {
					order.goods[i]={
						indexo:i,
						productID:key,
						productOption:option,
						productIndex:getStandardIndex(key),
						productPrice:getStandardPrice(key),
						productWeight:getStandardWeight(key)
					};
				}**/
				//goods_totalCount = goods_totalCount+count;
				for(var i=0;i<count;i++) {
					order.goods[goods_totalCount]={
						indexo:goods_totalCount,
						productID:key,
						productOption:option,
						productIndex:getStandardIndex(key),
						productPrice:getStandardPrice(key),
						productWeight:getStandardWeight(key)
					};
					goods_totalCount++;
				}
				addAnalyticsItem();
			}
			function getStandardIndex(key) {
				return pjson.goods_info.goods_info[key].standardprice/pjson.goods_info.goods_info[key].standardweight*100;
			}
			function getStandardPrice(key) {
				return pjson.goods_info.goods_info[key].standardprice.toString();
			}
			function getStandardWeight(key) {
				return pjson.goods_info.goods_info[key].standardweight.toString();
			}
			function addAnalyticsItem() {
				var gacategory = key;
				if(key.indexOf('-')!=-1) {
					gacategory = key.substring(0,key.indexOf('-'));
				}
				var gaprice = getStandardPrice(key);
				ga('ecommerce:addItem', { 
					'id': returnUID+'_'+returnKey,
					'name': key+'_'+option,
					'category': gacategory,
					'price': gaprice,
					'quantity': count,
					'currency': 'KRW'
				});
			}
		}
		function gethistories() {
			var status = "ordered";
			if(is.direct) { status = "waiting"; }
			order.histories[0]={
				error:"none",
				location:"undefined",
				status:status,
				timestamp:new Date().toISOString()
			};
		}
		function getinformations() {
			order.informations = {
				customer:"",
				customername:pcustomer.username,
				customeremail:pcustomer.useremail,
				customerphone:pcustomer.userphone,
				name:"",
				phone:"",
				postcode:"",
				address:"",
				room:"",
				tax:{
					name:"",
					email:"",
					code:"",
					type:0
				},
				direct:{
					directBank:"",
					directNo:""
				},
				request:document.getElementById('new_request').value,
				pointUsed:pcustomer.calcprice.point.toString(),
				shippingDate:pdate
			};
			if(is.newaddress) {
				order.informations.name = document.getElementById('new_name').value;
				order.informations.phone = document.getElementById('new_phone').value;
				order.informations.postcode = document.getElementById('new_postcode').value;
				order.informations.address = document.getElementById('new_address').value;
				order.informations.room = document.getElementById('new_room').value;
			}else {
				order.informations.name = pcustomer.address.name;
				order.informations.phone = pcustomer.address.phone;
				order.informations.postcode = pcustomer.address.postcode;
				order.informations.address = pcustomer.address.address;
				order.informations.room = pcustomer.address.room;
			}
			if(is.direct) {
				if(is.directrefund) {
					if(pcustomer.bank) {
						order.informations.direct={
							directBank:pcustomer.bank.name,
							directNo:pcustomer.bank.number
						};
					}
				}
				if(is.tax) {
					if(is.personal) {
						order.informations.tax={
							name:document.getElementById('tax_name').value,
							email:document.getElementById('tax_email').value,
							code:document.getElementById('tax_phone').value,
							type:0
						};
					}else {
						order.informations.tax={
							name:document.getElementById('tax_cname').value,
							email:document.getElementById('tax_cemail').value,
							code:document.getElementById('tax_cphone').value,
							type:1
						};
					}
				}
			}
		}
		function getpayment() {
			var prefix = "batch";
			var ptype = "batch_confirmed";
			var result = null;
			if(is.direct) { prefix = "direct"; ptype = "direct";}
			else {
				var cart_option = document.getElementById('card_option').value;
				if(pcustomer.cards[cart_option]) {
					result = pcustomer.cards[cart_option];
				}
			}
			order.payment = {
				address:"0",
				option:"0",
				prefix:prefix,
				price:0,
				pushTime:new Date().toISOString(),
				result:result,
				trypay:"success",
				type:ptype,
				calprice:pcustomer.calcprice
			};
		}
		function getcoupon() {
			console.log("COUPON",pcustomer.couponSelected);
			if(pcustomer.couponSelected) {
				order.coupon = {
					code:pcustomer.couponSelected
				}
			}
		}
		function gettotalPrice() {
			order.totalPrice = Number(pcustomer.calcprice.net);
		}
		function getPoint() {
			pcustomer.rewards.points.current = Number(pcustomer.rewards.points.current) - Number(pcustomer.calcprice.point);
			
			var historyCount=0;
			for(var i in pcustomer.rewards.points_history) {
				if(pcustomer.rewards.points_history[i]) { historyCount++; }
			}
			pcustomer.rewards.points_history[historyCount] = {
				changed:pcustomer.calcprice.point.toString(),
				timestamp:new Date().toISOString(),
				title:"상품구매"
			};
		}
	}
	function validation() {
		console.log(order.informations.phone.length,order);
		var validation_error = false;
		console.log(0);
		validation_error = goods_check();
		if(validation_error) { return; }
		
		console.log(1);
		validation_error = informations_check();
		if(validation_error) { return; }
		console.log(2);
		validation_error = payment_check();
		if(validation_error) { return; }
		console.log(3);
		validation_error = price_check();
		if(validation_error) { return; }
		console.log(4);
		if(!validation_error) { validorder = true; }
		
		function goods_check() {
			return false;
		}
		function informations_check() {
			if(!order.informations.shippingDate) {alert("배송일을 확인해주세요."); return true;}
			if(!order.informations.name) {alert("받으시는분 이름을 입력해주세요."); return true;}
			if(!order.informations.phone) {alert("받으시는분 연락처를 입력해주세요."); return true;}
			if(isNaN(order.informations.phone)) {alert("전화번호는 숫자만 입력해주세요."); return true;}
			if(order.informations.phone.indexOf(' ')!=-1) {alert("전화번호에 공백이 입력되어 있습니다."); return true;}
			if(order.informations.phone.length<9 || order.informations.phone.length>11) { alert("전화번호는 숫자만 입력해주세요.."); return true; }
			if(!order.informations.postcode) {alert("우편번호 찾기를 해주세요."); return true;}
			if(!order.informations.address) {alert("우편번호 찾기를 해주세요."); return true;}
			if(!order.informations.room) {alert("상세주소를 입력해주세요."); return true;}
			return false;
		}
		function payment_check() {
			if(order.payment.prefix==='direct'){}
			else {
				if(!order.payment.result) {alert("카드정보가 잘못되었습니다. 다른 카드를 선택하시거나 새로운 카드를 등록해주세요."); return true;}
				if(!order.payment.calprice) {alert("가격정보가 잘못되었습니다. 잠시후 다시 시도해주세요."); return true;}
			}
			return false;
		}
		function price_check() {
			if(order.payment.calprice.net!=order.totalPrice) {alert("가격정보가 잘못되었습니다. 잠시후 다시 시도해주세요."); return true;}
			return false;
		}
	}
	function put_firebase() {
		console.log("FB");
		if(!validorder) {return;}
		console.log("FB1",returnUID,returnKey,order);
		/**
		alert("테스트주문 통과 / 실제로 주문은 이루어지지 않았습니다. [승인필요]");
		location.href = 'complete.php?order='+returnKey;
		return;**/
		var completetext = '감사합니다! 주문이 완료되었으며, 상품생산이 완료되면 등록하신 카드를 통해 결제됩니다.';
		if(is.direct) { completetext="감사합니다! 입금이 확인된 후 생산이 시작되며, 오후4시를 넘어가면 도착희망일이 변경될 수 있습니다.";}
		if(returnUID&&order) {
			var ordersms = {
				phone:order.informations.customerphone,
				name:order.informations.customername,
				price:order.totalPrice
			};
			var emptycart = {
				current:0,
				timestamp:0
			};
			var paymenttype = order.payment.prefix;
			var asyncCount = 5;
			if(!pcustomer.address) {asyncCount++;}
			if(pcustomer.couponSelected) {asyncCount++;}
			var order_count=0;
			firebase.database().ref('realtime/'+paymenttype+'/'+returnUID+returnKey).set(ordersms).then(function(neworder) {
				order_count++;
				console.log("1");
				if(order_count==asyncCount) {completeOrder();}
			});
			firebase.database().ref('customers/'+returnUID+'/order_records/'+returnKey).set(order).then(function(neworder) {
				order_count++;
				console.log("2");
				if(order_count==asyncCount) {completeOrder();}
			});
			firebase.database().ref('customers/'+returnUID+'/carts/').set(emptycart).then(function(neworder) {
				order_count++;
				console.log("3");
				if(order_count==asyncCount) {completeOrder();}
			});
			firebase.database().ref('customers/'+returnUID+'/rewards').set(pcustomer.rewards).then(function(neworder) {
				order_count++;
				console.log("4");
				console.log(pcustomer.rewards);
				if(order_count==asyncCount) {completeOrder();}
			});
			firebase.database().ref('pegasus/orders/'+order.informations.shippingDate+'/'+returnUID+'/'+returnKey).set(order).then(function(neworder) {
				order_count++;
				console.log("5");
				console.log(pcustomer.rewards);
				if(order_count==asyncCount) {completeOrder();}
			});
			
			if(!pcustomer.address) {
				var inputAddress={
					name:order.informations.name,
					phone:order.informations.phone,
					postcode:order.informations.postcode,
					address:order.informations.address,
					room:order.informations.room
				};
				firebase.database().ref('customers/'+returnUID+'/address').set(inputAddress).then(function(newBank) {
					order_count++;
					console.log("6");
					if(order_count==asyncCount) {completeOrder();}
				});
			}
			if(pcustomer.couponSelected) {
				firebase.database().ref('customers/'+returnUID+'/coupons/get/'+pcustomer.couponSelected+'/used').set(new Date().toISOString()).then(function(newCoupon) {
					order_count++;
					console.log("7");
					if(order_count==asyncCount) {completeOrder();}
				});
			}
		}
		function completeOrder() {
			ga('ecommerce:send');
			alert(completetext);
			location.href = 'complete.php?order='+returnKey;
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////

function cardDelete(target) {
	var user = firebase.auth().currentUser;
	if(!user) {alert("잠시후에 다시 시도해주세요");return;}
	var returnUID = user.uid;
	if(!returnUID) {alert("잠시후에 다시 시도해주세요");return;}
	update_firebase();
	function update_firebase() {
		firebase.database().ref('customers/'+returnUID+'/cards').transaction(function(post) {
			if (post) {
				var calpost = post;
				var postcount = 0;
				for(var i in calpost) { if(i!='current' && calpost[i]) {postcount++;} }
				if(postcount<=target){
					return post;
				}else {
					post={};
					for(i=0;i<postcount;i++) {
						if(i<target) { post[i]=calpost[i]; }
						else if(i==target) { post[i]=calpost[i]; }
						else {
							post[i]=post[i-1];
							post[i-1]=calpost[i];
						}
					}
					if(post[postcount-1]) { post[postcount-1]=null; }
					else { return post; }
					post.current = calpost.current;
				}
			}else {
				post={
					current:{
						count:0,
						last:0
					}
				};
			}
			return post;
		});
	}
}
function save_cms() {
	var user = firebase.auth().currentUser;
	if(!user) {alert("잠시후에 다시 시도해주세요");return;}
	var returnUID = user.uid;
	if(!returnUID) {alert("잠시후에 다시 시도해주세요");return;}
	
	var cms_index = new Date();
	cms_index.setUTCHours(cms_index.getUTCHours()+9);
	var cms_date = (Number(cms_index.getFullYear())*10000+Number(Number(cms_index.getUTCMonth())+1)*100+Number(cms_index.getUTCDate()))%1000000;
	var cms_time = cms_date*100 + Number(cms_index.getUTCMinutes());
	
	var cms_agentID = cms_time+returnUID.substring(0,12);
	var cms_memberID = returnUID.substring(0,20);
	var cms_phone = document.getElementById('mypage_cms_phone').value;
	var cms_bank = document.getElementById('mypage_cms_bankname').value;
	var cms_paymentNumber = document.getElementById('mypage_cms_paymentNumber').value;
	var cms_payerName = document.getElementById('mypage_cms_payerName').value;
	var cms_payerNumber = document.getElementById('mypage_cms_payerNumber').value;
	if(validation()) {
		call_cms();
	}
	function validation() {
		if(cms_bank=='00' || !cms_bank) { alert("은행을 선택해주세요."); return false;}
		return true;
	}
	function call_cms() {
		console.log("cms");
		var jsonObj = {
			"agreementAgentId": cms_agentID,
			"memberId": cms_memberID,
			"phone": cms_phone,
			"callMode": "Outbound",
			"paymentKind": "CMS",
			"paymentCompany": {
			"code": null,
			"name": cms_bank },
			"paymentNumber": cms_paymentNumber,
			"payerName": cms_payerName,
			"payerNumber": cms_payerNumber
		};
		$.post("/cms_hs/arsrequest.php",jsonObj,
			function(data, status){
				var result = JSON.parse(data);
				console.log(status);
				if(result.error) {
					alert(result.error.message);
				}else {
					put_firebase(result);
				}
			}
		);
		function put_firebase(result) {
			if(validation()) {
				alert('정상적으로 요청되었습니다.');
			}
			function validation() {
				if(result.result.code=='0000') {return true;}
				alert('[에러코드:'+result.result.code+'] '+result.result.message);
				return false;
			}
		}
	}
}
function save_bank() {
	var inputbank={
		name:document.getElementById('mypage_bankname').value,
		number:document.getElementById('mypage_banknumber').value
	};
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	if(validation()) {
		put_firebase();
	}
	function validation() {
		if(!returnUID) { return false; }
		if(inputbank.name.length<2) { alert("은행이름을 확인해주세요."); return false;}
		if(inputbank.number.length<8) { alert("계좌번호를 확인해주세요."); return false;}
		return true;
	}
	function put_firebase() {
		firebase.database().ref('customers/'+returnUID+'/bank').set(inputbank).then(function(newBank) {
			alert("계좌번호가 등록되었습니다.");
		});
	}
}
function save_address() {
	var inputAddress={
		name:document.getElementById('mypage_addressname').value,
		phone:document.getElementById('mypage_addressphone').value,
		postcode:document.getElementById('mypage_addresspostcode').value,
		address:document.getElementById('mypage_addressaddress').value,
		room:document.getElementById('mypage_addressroom').value
	};
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	if(validation()) {
		put_firebase();
	}
	function validation() {
		if(!returnUID) { return false; }
		if(!inputAddress.name || inputAddress.name=="") { alert("이름을 입력해주세요."); return false; }
		if(!inputAddress.phone || inputAddress.phone=="") { alert("전화번호를 입력해주세요."); return false; }
		if(isNaN(inputAddress.phone)) {alert("전화번호는 숫자만 입력해주세요."); return true;}
		if(inputAddress.phone.indexOf(' ')!=-1) {alert("전화번호에 공백이 입력되어 있습니다."); return true;}
		if(inputAddress.phone.length<9 || inputAddress.phone.length>11) { alert("전화번호를 다시 확인해주세요."); return false; }
		if(!inputAddress.postcode || inputAddress.postcode=="") { alert("우편번호 찾기를 진행해주세요."); return false; }
		if(inputAddress.postcode.length!=5) { alert("우편번호를 다시 확인해주세요."); return false; }
		if(!inputAddress.address || inputAddress.address=="") { alert("우편번호 찾기를 진행해주세요."); return false; }
		if(!inputAddress.room || inputAddress.room=="") { alert("상세주소를 입력해주세요."); return false; }
		return true;
	}
	function put_firebase() {
		firebase.database().ref('customers/'+returnUID+'/address').set(inputAddress).then(function(newBank) {
			alert("기본배송지가 등록되었습니다.");
		});
	}
}
function save_code() {
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	var gcustomer = get_customer();
	if(!returnUID) {alert("잠시후에 다시 시도해주시기 바랍니다."); return;}
	var input_code = document.getElementById("mypage_codepoint").value;
	input_code = input_code.replace(/\s+/g, '');
	var codetype;
	console.log(gcustomer.promocode);
	if(validation()) {
		if(codetype=="customer") {
			checkCustomer();
		}else if(codetype=="customercode") {
			checkCustomerCode();
		}
	}else {
		alert("등록하실 수 없는 코드입니다.");
	}
	document.getElementById("mypage_codepoint").value="";
	function validation() {
		if(!input_code) {return false;}
		if(input_code.length==10 || input_code.length==11 || input_code.length==12 || input_code.length==13) {
			if(input_code.substring(0,2).indexOf('01')!=-1) {
				codetype="customer";
				return true;
			}else if(input_code.substring(0,2).indexOf('Aa')!=-1) {
				codetype="customercode";
				return true;
			}
		}
		return false;
	}
	function checkCustomerCode() {
		if(gcustomer.promocode) {
			if(gcustomer.promocode.recommender) {alert("이미 추천인을 등록하셨습니다."); return;}
		}
		var codeUID;
		firebase.database().ref('customersCode/'+input_code).once('value', function(codesnap) {
			codeUID = codesnap.val();
			if(!codeUID) { alert("등록하실 수 없는 코드입니다."); }
			else if(returnUID==codeUID) { alert("올바르지 않은 추천인코드입니다."); }
			else {
				firebase.database().ref('customers/'+codeUID).once('value', function(customersnap) {
					var recommendcustomer = customersnap.val();
					if(recommendcustomer) {
						put_firebase(recommendcustomer,codeUID);
					}
				});
			}
		});
	}
	function checkCustomer() {
		if(gcustomer.promocode) {
			if(gcustomer.promocode.recommender) {alert("이미 추천인을 등록하셨습니다."); return;}
		}
		var phoneUID;
		input_code = input_code.replace(/-/g, '');
		firebase.database().ref('customersPhone/phone/'+input_code).once('value', function(phonesnap) {
			phoneUID = phonesnap.val();
			if(!phoneUID) { alert("등록하실 수 없는 코드입니다."); }
			else if(returnUID==phoneUID) { alert("올바르지 않은 추천인번호입니다."); }
			else {
				firebase.database().ref('customers/'+phoneUID).once('value', function(customersnap) {
					var recommendcustomer = customersnap.val();
					if(recommendcustomer) {
						put_firebase(recommendcustomer,phoneUID);
					}
				});
			}
		});
	}
	function put_firebase(recommendcustomer,phoneUID) {
		firebase.database().ref('customers/'+returnUID+'/promocode').set(
			{
				recommender:{
					uid:phoneUID,
					input_code:input_code
				}
			}).then(function() {
			alert("추천인이 등록었습니다. 첫 주문상품이 발송되면 적립금 2,000원이 고객님과 추천인에게 지급됩니다.");
			location.reload();
		});
	}
}
///////////////////////////////////// Coupon ///////////////////////////////////////////////////

function save_couponcode() {
	var user = firebase.auth().currentUser;
	var returnUID = user.uid;
	var gcustomer = get_customer();
	if(!returnUID) {alert("잠시후에 다시 시도해주시기 바랍니다."); return;}
	var input_code = document.getElementById("mypage_pointcode_input").value;
	var codetype;
	validation();
	document.getElementById("mypage_pointcode_input").value="";
	function validation() {
		if(!input_code) {return false;}
		if(input_code.length>5) {
			firebase.database().ref('couponlist/pointcoupon/valid/'+input_code).once('value', function(couponsnap) {
				var couponcode = couponsnap.val();
				if(couponcode) {
					if(!couponcode.activate) {
						alert("만료된 코드입니다.");
						return;
					}
					if(!gcustomer.coupons) {
						put_firebase(input_code,couponcode.point,couponcode.title);
					}
					else if(!gcustomer.coupons.code) {
						put_firebase(input_code,couponcode.point,couponcode.title);
					}
					else if(!gcustomer.coupons.code[input_code]) {
						put_firebase(input_code,couponcode.point,couponcode.title);
					}else {
						alert("이미 등록하신 코드입니다.");
						return;
					}
				}else {
					alert("올바르지 않은 코드입니다.");
					return;
				}
			});
		}else {
			alert("올바르지 않은 코드입니다.");
			return;
		}
		
		function put_firebase(putCode,putPoint,putTitle) {
			gcustomer.rewards.points.current = Number(gcustomer.rewards.points.current)+Number(putPoint);
			
			var historyCount=0;
			for(var i in gcustomer.rewards.points_history) {
				if(gcustomer.rewards.points_history[i]) { historyCount++; }
			}
			gcustomer.rewards.points_history[historyCount] = {
				changed:(Number(putPoint)*-1).toString(),
				timestamp:new Date().toISOString(),
				title:putTitle
			};
			
			var maxend = 3;
			var currentend = 0;
			firebase.database().ref('customers/'+returnUID+'/rewards').set(
				gcustomer.rewards
				).then(function() {
				currentend++;
				put_complete();
			});
			firebase.database().ref('customers/'+returnUID+'/coupons/code/'+putCode).set(
				new Date().toISOString()).then(function() {
				currentend++;
				put_complete();
			});
			firebase.database().ref('couponlist/pointcoupon/userlist/'+putCode+'/'+returnUID).set(
				new Date().toISOString()).then(function() {
				currentend++;
				put_complete();
			});
			function put_complete() {
				if(currentend==maxend) {
					alert("쿠폰코드가 적용되었습니다.");
					location.reload();
				}
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////
var element_layer;
function closeDaumPostcode() {
	element_layer = document.getElementById('layerd');
	element_layer.style.display = 'none';
}
function execDaumPostcode() {
	element_layer = document.getElementById('layerd');
	new daum.Postcode({
		oncomplete: function(data) {
			var fullAddr = data.address;
			var extraAddr = '';

			if(data.addressType === 'R'){
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			}
			if(data.zonecode.toString().substring(0,2).indexOf('63')!=-1) {
				alert("제주특별자치도에는 정육각의 돼지고기 판매가 불가능합니다.");
			}else {
				if(document.getElementById('new_postcode')) {
					document.getElementById('new_postcode').value = data.zonecode;
					document.getElementById('new_address').value = fullAddr;
				}
				if(document.getElementById('mypage_addresspostcode')) {
					document.getElementById('mypage_addresspostcode').value = data.zonecode;
					document.getElementById('mypage_addressaddress').value = fullAddr;
				}
			}
			element_layer.style.display = 'none';
		},
		width : '100%',
		height : '100%'
	}).embed(element_layer);
	element_layer.style.display = 'block';
	initLayerPosition();
}
function initLayerPosition(){
	var width = 400;
	var height = 500;
	var borderWidth = 2;
	width = screen.width-60;
	height = screen.height-60;
	if(width>400) {width = 400;}
	if(height>400) {height = 400;}

	element_layer.style.width = width + 'px';
	element_layer.style.height = height + 'px';
	element_layer.style.border = borderWidth + 'px solid';
	element_layer.style.left = (((window.innerWidth || document.documentElement.clientWidth) - width)/2 - borderWidth) + 'px';
	element_layer.style.top = (((window.innerHeight || document.documentElement.clientHeight) - height)/2 - borderWidth) + 'px';
	window.scrollTo(0,0);
}
function removeBanner() {
	document.getElementById('event_banner').style.display = 'none';
	if(window.location.pathname == '/index.php') {
		document.getElementById('following_nav').style.height = '90px';
		if(document.getElementById('index_main_container_1')) {
			document.getElementById('index_main_container_1').style.top='90px';
			document.getElementById('index_main_container_2').style.top='90px';
			document.getElementById('index_goods').style.top='90px';
		}
	}
	if(window.location.pathname == '/index_m.php') {
		document.getElementsByClassName('custom_nav')[0].style.height = '44px';
		document.getElementsByClassName('custom_nav-logo')[0].style.top = '0px';
		if(document.getElementById('index_main_container_1')) {
			document.getElementById('index_main_container_1').style.top='44px';
			document.getElementById('index_main_container_2').style.top='44px';
			document.getElementById('index_goods').style.marginTop='44px';
		}
	}
	
}