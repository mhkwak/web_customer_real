var config = {
    apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
    authDomain: "jyg-custom.firebaseapp.com",
    databaseURL: "https://jyg-custom.firebaseio.com",
    projectId: "jyg-custom",
    storageBucket: "jyg-custom.appspot.com",
    messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
cacheBlocked();
function cacheBlocked() {
	try{ localStorage.setItem("cacheBlocked",false);
	}catch(e) { alert("현재 사용중이신 브라우저의 설정이 쿠키 사용안함으로 설정되어 있어 정상적인 사이트 이용이 불가능합니다.\n설정에서 쿠키 '사용함'으로 변경 후 다시 시도해주시기 바랍니다.");}
}
var firebase_init = true;
function getFirebase_init() {return firebase_init;}
var welcome_ready=false;
firebase.auth().onAuthStateChanged(function(custom) {
	firebase_init = false;
		var acustom = firebase.auth().currentUser;
		if(acustom) {
			if(welcome_ready) {alert("로그인되었습니다.");}
			init_cart_listener(acustom.uid,false);
		}else {welcome_ready=true;
			  alert("로그인이 필요한 화면입니다.");
			location.href = "index.php";
			  }
});
var fireflag={json:false,customer:false};
var json; function get_json() {return json;} function set_json(val) {json=val;}
var customer; function get_customer() {return customer;} function set_customer(val) {customer=val;}
var datelist; function get_datelist() {return datelist;} function set_datelist(val) {datelist=val;}
var selecteddate; function get_selecteddate() {return selecteddate;} function set_selecteddate(val) {selecteddate=val;}
var goodkey; function get_goodkey() {return goodkey;} function set_goodkey(val) {goodkey=val;}
var faqfilter = ''; function set_faqfilter(target) {faqfilter = target;} function clear_faqfilter() {faqfilter='';}

var phpjson = getsupport_json();
if(phpjson) {
	json = phpjson;
	fireflag.json=true;
}
var phpcustomer = getsupport_init();
var phpuid = getsupport_uid();
if(phpcustomer && phpuid) {
	customer = phpcustomer;
	fireflag.customer=true;
}

init_json_listener();
function init_json_listener() {
	if(fireflag.json && fireflag.customer) {
		console.log("json fast");
		init_index_goods();
		init_complete_order();
		print_notice();
		print_faq();
		asyncsupport();
	}
	firebase.database().ref('json').on('value', function(jsonsnap) {
		console.log("json refresh");
		json = jsonsnap.val();
		fireflag.json=true;
		init_index_goods();
		init_complete_order();
		print_notice();
		print_faq();
		asyncsupport();
	});
	function asyncsupport() {
		if(fireflag.customer) {
			if(firebase.auth().currentUser) {
				init_cart_listener(firebase.auth().currentUser.uid,true);
			}else {
				init_cart_listener(phpuid,true);
			}
		}
	}
	function print_notice() {
		var support_notice = getsupport_data();
		var headHTML;
		var bodyHTML = '<tbody>';
		if(support_notice) {
			if(window.location.pathname == '/support.php')  {
				headHTML = '<thead><tr><td style="width:70px">번호</td><td style="width:500px">제목</td><td style="width:130px">작성자</td><td>작성일</td></tr></thead>';
				make_notice_pc();
			}
			else if(window.location.pathname == '/support_m.php') {
				headHTML = '<thead><tr><td>제목</td><td style="width: 70px;">작성일</td></tr></thead>';
				make_notice_mobile();
			}

			bodyHTML+='</tbody>';
			document.getElementById('support_notice_table').innerHTML = headHTML+bodyHTML;
		}
		function make_notice_pc() {
			for(var i in support_notice.notice) {
				if(!support_notice.notice[i].top) {
					var noticeNo = '<td>'+support_notice.notice[i].index+'</td>';
					var noticetitle = '<td style="text-align: left; padding-left:30px;">'+support_notice.notice[i].title+'</td>';
					var noticeWriter = '<td>'+support_notice.notice[i].writer+'</td>';
					var noticeDate = '<td>'+support_notice.notice[i].date+'</td>';
					bodyHTML='<tr style="cursor:pointer" onClick="link_noticedetail('+support_notice.notice[i].index+')">'+noticeNo+noticetitle+noticeWriter+noticeDate+'</tr>'+bodyHTML;
				}
			}
			for(i in support_notice.notice) {
				if(support_notice.notice[i].top) {
					var noticeNo = '<td>'+'공지'+'</td>';
					var noticetitle = '<td style="text-align: left; padding-left:30px;">'+support_notice.notice[i].title+'</td>';
					var noticeWriter = '<td>'+support_notice.notice[i].writer+'</td>';
					var noticeDate = '<td>'+support_notice.notice[i].date+'</td>';
					bodyHTML='<tr style="cursor:pointer" onClick="link_noticedetail('+support_notice.notice[i].index+')">'+noticeNo+noticetitle+noticeWriter+noticeDate+'</tr>'+bodyHTML;
				}
			}
		}
		function make_notice_mobile() {
			for(var i in support_notice.notice) {
				if(!support_notice.notice[i].top) {
					var noticetitle = '<td style="text-align: left; padding-left: 20px;">'+support_notice.notice[i].title+'</td>';
					var noticeDate = '<td style="text-align:center">'+support_notice.notice[i].date.substring(5)+'</td>';
					bodyHTML='<tr style="cursor:pointer" onClick="link_noticedetail('+support_notice.notice[i].index+')">'+noticetitle+noticeDate+'</tr>'+bodyHTML;
				}
			}
			for(i in support_notice.notice) {
				if(support_notice.notice[i].top) {
					var noticetitle = '<td style="text-align: left; padding-left: 20px;">'+'[공지]'+support_notice.notice[i].title+'</td>';
					var noticeDate = '<td style="text-align:center">'+support_notice.notice[i].date.substring(5)+'</td>';
					bodyHTML='<tr style="cursor:pointer" onClick="link_noticedetail('+support_notice.notice[i].index+')">'+noticetitle+noticeDate+'</tr>'+bodyHTML;
				}
			}
		}
	}
	function init_index_goods() {
		var goods_info = json.goods_info.goods_info;
		json.goods_list=[];
		json.goods_length = 0;
		for(var goodkey in goods_info) {
			if(goods_info[goodkey].standardindex) {
				json.goods_length++;
				json.goods_list[goods_info[goodkey].standardindex-1]=goods_info[goodkey];
				json.goods_list[goods_info[goodkey].standardindex-1].key = goodkey;
			}
		}
	}
	function init_complete_order() {
		orderkey = document.getElementById('orderkey').value;
	}
}

function init_cart_listener(currentUID,asyncflag) {
	if(!asyncflag) {
		firebase.database().ref('customers/'+currentUID).on('value', function(customersnap) {
			console.log("customer refresh");
			customer = customersnap.val();
			fireflag.customer=true;
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();
				print_ask_table();
			}
		});
	}else {
		init_cart_goods();
		print_cart_goods();
		print_cart_prices();
		print_ask_table();
	}
	function init_cart_goods() {
		customer.calcprice={
			sum:0,
			point:0,
			shipping:2500,
			echo:0,
			total:0,
			net:0,
			count:0
		};
		
		init_available_dates();
		
		function init_available_dates() {
			datelist = {};
			for(var i in customer.carts) {
				if(i!=='current' && i!=='timestamp') {
					for(var j in json.goods_info.goods_info[i].dayoff) {
						if(json.goods_info.goods_info[i].dayoff[j]) {
							datelist[j] = true;
						}
					}
				}
			}
		}
		init_shipping_date();
		function init_shipping_date() {
			if(realtime_date(customer.carts)) {
				selecteddate = customer.carts.current;
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(customer.carts.current);
					firebase.database().ref('customers/'+currentUID+'/carts/current').set(selecteddate);
				}
			}
			else if(!selecteddate) {
				var startdate = new Date();
				startdate.setHours(startdate.getHours()+9);
				if(startdate.getUTCHours()>16) {startdate.setDate(startdate.getDate()+1);}
				selecteddate = getNextDate(startdate.toISOString());
			}else {
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(selecteddate);
				}
			}
			function realtime_date(targetCart) {
				var pastDate = new Date();
				pastDate.setDate(pastDate.getDate()+1);
				if(!firebase.auth().currentUser) {return false;}
				var targetUID = firebase.auth().currentUser.uid;
				if(!targetUID) { return false;}
				if(targetCart) {
					if(targetCart.current!=0) {
						if(targetCart.current.toString().indexOf('(')!=-1) {
							if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
								firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
								return false;
							}
							return true;
						}
					}
				}return false;
			}
			function getNextDate(target) {
				var returndate = new Date(target.substring(0,10));
				var findflag = false;
				for(var i=0;i<50;i++) {
					returndate.setDate(returndate.getDate()+1);
					if(!datelist[returndate.toISOString().substring(0,10)]) { findflag=true; break; }
				}
				if(!findflag) { return target; }
				return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
			}
			function getKorDay(target) {
				if(target===0) {return '일';}
				if(target===1) {return '월';}
				if(target===2) {return '화';}
				if(target===3) {return '수';}
				if(target===4) {return '목';}
				if(target===5) {return '금';}
				if(target===6) {return '토';}
				return '일';
			}
		}
	}
	function print_cart_goods() {
		var cartHTML = '';
		var itemCount = 0;
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					if(customer.carts[i][j]) {
						var option = Number(j.toString().substring(1))-1;
						var key = i;
						var count = customer.carts[i][j].count;
						var itemHTML = '<div class="cart_item">'+
										'<div class="citem-name"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						itemCount++;
						cartHTML+=itemHTML;
						customer.calcprice.sum+=json.goods_info.goods_info[key].standardprice*count;
						customer.calcprice.count+=count;
					}
				}
			}
		}
		document.getElementById('cart_list').innerHTML = cartHTML;
		function goodsName(target) {
			return json.goods_info.goods_info[target].standardname;
		}
		function goodsOption(target,option) {
			return json.goods_info.goods_info[target].options[option].name;
		}
		function goodsPrice(target,count) {
			return numberWithWon(json.goods_info.goods_info[target].standardprice*count);
		}
	}
	function print_cart_prices() {
		if(customer.calcprice.count<2) {customer.calcprice.echo = 0;}
		else if(customer.calcprice.count<7) {customer.calcprice.echo=(customer.calcprice.count-1)*500;}
		else {customer.calcprice.echo=2500;}
		customer.calcprice.total = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo;
		document.getElementById('cart_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('cart_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('cart_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('cart_total').innerHTML = numberWithWon(customer.calcprice.total);
		
		document.getElementById('cart_shippingdate').innerHTML = selecteddate;
		
		if(customer.calcprice.count==0) {
			document.getElementById('cart_empty').style.display="block";
		} else {
			document.getElementById('cart_empty').style.display="none";
		}
	}
	
	function print_ask_table() {
		document.getElementById('support_question_btn').style.display='block';
		
		var headHTML;
		
		var headHTML;
		if(window.location.pathname == '/support.php')  {
			headHTML = '<thead><tr><td>문의날짜</td><td>문의제목</td><td colspan="2">답변여부</td></tr></thead>';
		}
		else if(window.location.pathname == '/support_m.php') {
			headHTML = '<thead><tr><td>문의</td><td>답변</td></tr></thead>';
		}
		var bodyHTML = '<tbody>';
		var existQuestion = false;
		if(window.location.pathname == '/support.php')  {
			make_ask_pc();
		}
		else if(window.location.pathname == '/support_m.php') {
			make_ask_mobile();
		}
		function make_ask_mobile() {
			for(var i in customer.questions) {
				if(customer.questions[i]) {
					existQuestion = true;
					var questionDate = '<td>'+customer.questions[i].postDate.substring(0,10)+'<br>';
					var questionTitle = '<div>'+customer.questions[i].postTitle+'</div>'+'</td>';
					var questionReply;
					if(customer.questions[i].replyDate!='none' && customer.questions[i].replyDate) {
						questionReply = '<td class="table_select" onClick="open_reply('+i+')">'+'답변완료</td>';
					}else {
						questionReply = '<td class="table_select" onClick="open_reply('+i+')">'+'미확인</td>';
					}
					bodyHTML='<tr>'+questionDate+questionTitle+questionReply+'</tr>'+bodyHTML;
				}
			}
		}
		function make_ask_pc() {
			for(var i in customer.questions) {
				if(customer.questions[i]) {
					existQuestion = true;
					var replyText='미응답';
					if(customer.questions[i].replyDate!='none' && customer.questions[i].replyDate) {
						replyText='확인';
					}
					var questionDate = '<td>'+customer.questions[i].postDate.substring(0,10)+'</td>';
					var questionTitle = '<td>'+customer.questions[i].postTitle+'</td>';
					var questionReply = '<td>'+replyText+'</td>';
					var questionDetail = '<td class="table_select" onClick="open_reply('+i+')">'+'자세히'+'</td>';
					bodyHTML='<tr>'+questionDate+questionTitle+questionReply+questionDetail+'</tr>'+bodyHTML;
				}
			}
		}

		bodyHTML+='</tbody>';
		if(existQuestion) {
			document.getElementById('support_ask_table').innerHTML = headHTML+bodyHTML;
		}else {
		}
	}
	
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}
function open_reply(target) {
	if(customer.questions[target]) {
		document.getElementById('ask_date').innerHTML = customer.questions[target].postDate.substring(0,10);
		document.getElementById('ask_title').innerHTML = customer.questions[target].postTitle;
		document.getElementById('ask_detail').innerHTML = customer.questions[target].postDetail;
		document.getElementById('ask_replydate').innerHTML = customer.questions[target].replyDate;
		document.getElementById('ask_replydetail').innerHTML = customer.questions[target].replyDetail;
		open_dialog('dialog_reply');
		console.log(document.getElementById('ask_detail').offsetHeight);
		if(document.getElementById('reply_resize')) {
		document.getElementById('reply_resize').style.marginBottom = document.getElementById('ask_detail').offsetHeight+'px';
		}
	}
	else {}
}

function print_faq() {
	var support_notice = getsupport_data();
	var faqHTML = '';
	if(window.location.pathname == '/support.php')  {
		make_faq_pc();
	}
	else if(window.location.pathname == '/support_m.php') {
		make_faq_mobile();
	}
	function make_faq_pc() {
		if(support_notice && document.getElementById('faq_list')) {
			document.getElementById('support_faq_category').innerHTML = getfilterName(faqfilter);
			for(var i in support_notice.faq) {
				if(faqfilter == support_notice.faq[i].category || !faqfilter) {
					faqHTML = '<div class="support_faq-box"><div class="support_faq-question" onClick="open_faq_item('+i+')">'+
						'<img class="support_faq-icon" src="img/notice/faq_question.jpg" ><span>'+
						getfilterName(support_notice.faq[i].category)+' '+support_notice.faq[i].question+'</span><img class="support_faq-plus" src="img/notice/icon_plus.png" id="faq_plus_'+i+'">'+
						'<img class="support_faq-minus" src="img/notice/icon_minus.png" id="faq_minus_'+i+'"></div>'+
						'<div class="support_faq-answer" id="faq_item_'+i+'"><img class="support_faq-icon" src="img/notice/faq_answer.jpg" ><span>'+
						support_notice.faq[i].answer+'</span></div></div>' + faqHTML;
				}
			}
			document.getElementById('faq_list').innerHTML = faqHTML;
		}
	}
	function make_faq_mobile() {
		if(support_notice && document.getElementById('faq_list')) {
			document.getElementById('support_faq_category').innerHTML = getfilterName(faqfilter);
			for(var i in support_notice.faq) {
				if(faqfilter == support_notice.faq[i].category || !faqfilter) {
					faqHTML = '<div class="support_faq-box"><div class="support_faq-question" onClick="open_faq_item('+i+')">'+
						'<img class="support_faq-icon" src="img/notice/faq_question.jpg" ><span>'+
						getfilterName(support_notice.faq[i].category)+' '+support_notice.faq[i].question+'</span><img class="support_faq-plus" src="img/notice/icon_plus.png" id="faq_plus_'+i+'">'+
						'<img class="support_faq-minus" src="img/notice/icon_minus.png" id="faq_minus_'+i+'"></div>'+
						'<div class="support_faq-answer" id="faq_item_'+i+'"><img class="support_faq-icon" src="img/notice/faq_answer.jpg" ><span>'+
						support_notice.faq[i].answer+'</span></div></div>' + faqHTML;
				}
			}
			document.getElementById('faq_list').innerHTML = faqHTML;
		}
	}
	function getfilterName(target) {
		if(target=='product') { return '[상품]';}
		if(target=='order') { return '[주문/결제]';}
		if(target=='delivery') { return '[배송]';}
		if(target=='change') { return '[취소/교환/환불]';}
		return '';
	}
}