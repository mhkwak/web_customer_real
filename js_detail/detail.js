var config = {
    apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
    authDomain: "jyg-custom.firebaseapp.com",
    databaseURL: "https://jyg-custom.firebaseio.com",
    projectId: "jyg-custom",
    storageBucket: "jyg-custom.appspot.com",
    messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
cacheBlocked();
function cacheBlocked() {
	try{ localStorage.setItem("cacheBlocked",false);
	}catch(e) { alert("현재 사용중이신 브라우저의 설정이 쿠키 사용안함으로 설정되어 있어 정상적인 사이트 이용이 불가능합니다.\n설정에서 쿠키 '사용함'으로 변경 후 다시 시도해주시기 바랍니다.");}
}
var firebase_init = true;
function getFirebase_init() {return firebase_init;}
var welcome_ready=false;
firebase.auth().onAuthStateChanged(function(custom) {
	firebase_init = false;
		var acustom = firebase.auth().currentUser;
		if(acustom) {
			if(welcome_ready) {alert("로그인되었습니다.");}
			init_cart_listener(acustom.uid,false);
		}else {welcome_ready=true;}
});
var fireflag={json:false,customer:false};
var json; function get_json() {return json;} function set_json(val) {json=val;}
var customer; function get_customer() {return customer;} function set_customer(val) {customer=val;}
var datelist; function get_datelist() {return datelist;} function set_datelist(val) {datelist=val;}
var selecteddate; function get_selecteddate() {return selecteddate;} function set_selecteddate(val) {selecteddate=val;}
var goodkey; function get_goodkey() {return goodkey;} function set_goodkey(val) {goodkey=val;}
init_json_listener();
function init_json_listener() {
	firebase.database().ref('json').on('value', function(jsonsnap) {
		console.log("json refresh");
		json = jsonsnap.val();
		fireflag.json=true;
		init_index_goods();
		init_detail_goods();
		print_detail_goods();
		asyncsupport();
	});
	function asyncsupport() {
		if(fireflag.customer) {
			if(firebase.auth().currentUser) {
				init_cart_listener(firebase.auth().currentUser.uid,true);
			}
		}
	}
	function init_index_goods() {
		var goods_info = json.goods_info.goods_info;
		json.goods_list=[];
		json.goods_length = 0;
		for(var goodkeyi in goods_info) {
			if(goods_info[goodkeyi].standardindex) {
				json.goods_length++;
				json.goods_list[goods_info[goodkeyi].standardindex-1]=goods_info[goodkeyi];
				json.goods_list[goods_info[goodkeyi].standardindex-1].key = goodkeyi;
			}
		}
	}
	function init_detail_goods() {
		goodkey = document.getElementById('goodkey').value;
		console.log(goodkey);
		if(goodkey.toString().indexOf('-')!=-1) {
			detail_menu_open(goodkey.toString().substring(0,goodkey.toString().indexOf('-')));
		}
	}
	function print_detail_goods() {
		//document.getElementById('detail_tag_price').innerHTML="기준가("+json.goods_info.goods_info[goodkey].standardweightkor+")";
		if(goodkey.indexOf('chicken')!=-1) {document.getElementById('detail_tag_name').innerHTML="도계일";}
		else if(goodkey.indexOf('egg')!=-1) {document.getElementById('detail_tag_name').innerHTML="산란일";}
		if(json.goods_info.goods_info[goodkey]) {
			json.goods_info.goods_info[goodkey].selectCount=1;
			json.goods_info.goods_info[goodkey].selectOption=0;
			var currentgood = json.goods_info.goods_info[goodkey];
			var optionHTML = '';
			for(var i in currentgood.options) {
				if(currentgood.options[i]) {
					if(currentgood.options[i].status.publish) {
						optionHTML+='<option value="'+i+'">'+currentgood.options[i].name+'</option>';
					}
				}
			}
			document.getElementById('detail_option').innerHTML = optionHTML;
			document.getElementById('detail_option').value=0;
			
			document.getElementById('detail_name').innerHTML = currentgood.standardname;
			document.getElementById('detail_standardweight').innerHTML = currentgood.standardweightkor;
			document.getElementById('detail_standardprice').innerHTML = currentgood.standardpricekor;
			document.getElementById('detail_quantity').innerHTML = 1;
			document.getElementById('detail_date').innerHTML = currentgood.standarddate;
			if(document.getElementById('detail_indexprice')) {
				var detail_idx_price = Number(currentgood.standardprice)/Number(currentgood.standardweight)*100;
				detail_idx_price = Math.floor(detail_idx_price);
				document.getElementById('detail_indexprice').innerHTML = numberWithWon(detail_idx_price)+" / 100g";
				if(goodkey=="egg-fresh") {
					document.getElementById('detail_indexprice').innerHTML = "550원 / 1개";
				}
			}
			document.getElementById('detail_price').innerHTML = numberWithWon(currentgood.standardprice)+" : "+json.goods_info.goods_info[goodkey].standardweightkor+" 기준가";
			//document.getElementById('detail-control').style.display='block';
			
			document.getElementById('detail-main-01').src = "img/goods_detail/01-"+goodkey+".jpg";
			document.getElementById('detail-main-02').src = "img/goods_detail/02-"+goodkey+".jpg";
			var commonSRC = '';
			if(goodkey.indexOf('chicken')!=-1) {commonSRC='img/goods_detail/03-common-chicken.jpg';}
			else if(goodkey.indexOf('egg')!=-1) {commonSRC='img/goods_detail/03-common-egg.jpg';}
			else if(goodkey.indexOf('clean')!=-1) {commonSRC='img/goods_detail/03-common-porkclean.jpg';}
			else {commonSRC='img/goods_detail/03-common-porkfresh.jpg';}
			document.getElementById('detail-main-03').src = commonSRC;
			document.getElementById('detail_part').src='img/goods_icon/ico-'+goodkey+'.jpg'
		}else {
			alert('상품정보를 찾을 수 없습니다.');
			location.href ='index.php';
		}
		function numberWithWon(x) {
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
		}
	}
}

function init_cart_listener(currentUID,asyncflag) {
	if(!asyncflag) {
		firebase.database().ref('customers/'+currentUID).on('value', function(customersnap) {
			console.log("customer refresh");
			customer = customersnap.val();
			fireflag.customer=true;
			if(fireflag.json) {
				init_cart_goods();
				print_cart_goods();
				print_cart_prices();
			}
		});
	}else {
		init_cart_goods();
		print_cart_goods();
		print_cart_prices();
	}
	function init_cart_goods() {
		customer.calcprice={
			sum:0,
			point:0,
			shipping:2500,
			echo:0,
			total:0,
			net:0,
			count:0
		};
		
		init_available_dates();
		
		function init_available_dates() {
			datelist = {};
			for(var i in customer.carts) {
				if(i!=='current' && i!=='timestamp') {
					for(var j in json.goods_info.goods_info[i].dayoff) {
						if(json.goods_info.goods_info[i].dayoff[j]) {
							datelist[j] = true;
						}
					}
				}
			}
		}
		init_shipping_date();
		function init_shipping_date() {
			if(realtime_date(customer.carts)) {
				selecteddate = customer.carts.current;
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(customer.carts.current);
					firebase.database().ref('customers/'+currentUID+'/carts/current').set(selecteddate);
				}
			}
			else if(!selecteddate) {
				var startdate = new Date();
				startdate.setHours(startdate.getHours()+9);
				if(startdate.getUTCHours()>16) {startdate.setDate(startdate.getDate()+1);}
				selecteddate = getNextDate(startdate.toISOString());
			}else {
				if(datelist[selecteddate.substring(0,10)]) {
					selecteddate = getNextDate(selecteddate);
				}
			}
			function realtime_date(targetCart) {
				var pastDate = new Date();
				pastDate.setDate(pastDate.getDate()+1);
				if(!firebase.auth().currentUser) {return false;}
				var targetUID = firebase.auth().currentUser.uid;
				if(!targetUID) { return false;}
				if(targetCart) {
					if(targetCart.current!=0) {
						if(targetCart.current.toString().indexOf('(')!=-1) {
							if(pastDate.toISOString().substring(0,10)>targetCart.current.toString().substring(0,10)) {
								firebase.database().ref('customers/'+targetUID+'/carts/current').set(0);
								return false;
							}
							return true;
						}
					}
				}return false;
			}
			function getNextDate(target) {
				var returndate = new Date(target.substring(0,10));
				var findflag = false;
				for(var i=0;i<50;i++) {
					returndate.setDate(returndate.getDate()+1);
					if(!datelist[returndate.toISOString().substring(0,10)]) { findflag=true; break; }
				}
				if(!findflag) { return target; }
				return returndate.toISOString().substring(0,10)+' ('+getKorDay(returndate.getUTCDay())+')';
			}
			function getKorDay(target) {
				if(target===0) {return '일';}
				if(target===1) {return '월';}
				if(target===2) {return '화';}
				if(target===3) {return '수';}
				if(target===4) {return '목';}
				if(target===5) {return '금';}
				if(target===6) {return '토';}
				return '일';
			}
		}
	}
	function print_cart_goods() {
		var cartHTML = '';
		var itemCount = 0;
		for(var i in customer.carts) {
			if(i!=='current' && i!=='timestamp') {
				for(var j in customer.carts[i]) {
					if(customer.carts[i][j]) {
						var option = Number(j.toString().substring(1))-1;
						var key = i;
						var count = customer.carts[i][j].count;
						var itemHTML = '<div class="cart_item">'+
										'<div class="citem-name"><span>'+goodsName(key)+'</span><img src="img/common/close.jpg" class="citem-remove" onClick="direct_remove('+"'"+key+"',"+option+')"></div>'+
										'<div class="citem-option">'+goodsOption(key,option)+'</div>'+
										'<div class="citem-select">'+
										'<span class="citem_tag">수량</span>'+
										'<span class="citem-quantity">'+count+'</span>'+
										'<div class="cart_less" onClick="direct_less('+"'"+key+"',"+option+')">-</div><div class="cart_more" onClick="direct_more('+"'"+key+"',"+option+')">+</div>'+
										'<span class="citem-price">'+goodsPrice(key,count)+'</span>'+
										'</div>'+
										'</div>';
						itemCount++;
						cartHTML+=itemHTML;
						customer.calcprice.sum+=json.goods_info.goods_info[key].standardprice*count;
						customer.calcprice.count+=count;
					}
				}
			}
		}
		document.getElementById('cart_list').innerHTML = cartHTML;
		function goodsName(target) {
			return json.goods_info.goods_info[target].standardname;
		}
		function goodsOption(target,option) {
			return json.goods_info.goods_info[target].options[option].name;
		}
		function goodsPrice(target,count) {
			return numberWithWon(json.goods_info.goods_info[target].standardprice*count);
		}
	}
	function print_cart_prices() {
		if(customer.calcprice.count<2) {customer.calcprice.echo = 0;}
		else if(customer.calcprice.count<7) {customer.calcprice.echo=(customer.calcprice.count-1)*500;}
		else {customer.calcprice.echo=2500;}
		customer.calcprice.total = customer.calcprice.sum+customer.calcprice.shipping-customer.calcprice.echo;
		document.getElementById('cart_sum').innerHTML = numberWithWon(customer.calcprice.sum);
		document.getElementById('cart_shipping').innerHTML = numberWithWon(customer.calcprice.shipping);
		document.getElementById('cart_echo').innerHTML = numberWithWon(-1*customer.calcprice.echo);
		document.getElementById('cart_total').innerHTML = numberWithWon(customer.calcprice.total);
		
		document.getElementById('cart_shippingdate').innerHTML = selecteddate;
		
		if(customer.calcprice.count==0) {
			document.getElementById('cart_empty').style.display="block";
		} else {
			document.getElementById('cart_empty').style.display="none";
		}
	}
	function numberWithWon(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
	}
}