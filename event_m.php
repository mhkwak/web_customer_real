<?php
$useragent=$_SERVER['HTTP_USER_AGENT'];
$hostname=$_SERVER['HTTP_HOST'];
if(!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
header('Location: http://'.$hostname.'/event.php');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta http-equiv="cache-control" content="no-store">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
    
    <title>정육각</title>
    <link rel="shortcut icon" href="/img/jyg.ico" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"-->
	<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-84539077-2', 'auto');
		ga('send', 'pageview');
	</script>
  </head>
  <body class="noselect">

<div class="dialog_shadow" id="dialog_rule">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_rule')">
		</div>
		<div class="dialog_text-wrapper">
			<span id="agreement_usage"></span>
		</div>
	</div>
</div>
<div class="dialog_shadow" id="dialog_privacy">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_privacy')">
		</div>
		<div class="dialog_text-wrapper">
			<span id="agreement_privacy"></span>
		</div>
	</div>
</div>
<div class="dialog_shadow" id="dialog_agreement">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_agreement')">
		</div>
		<div class="dialog_content-wrapper">
			<p class="dialog-title">정육각 회원가입</p>
			<p class="dialog-subtitle">(전화번호 인증)</p>
			<div class="agree-wrapper"><label><input id="check_all" name="check_all" type="checkbox">전체동의</label></div>
			<div class="agree-wrapper"><label><input id="check_age" type="checkbox">(필수) 만 14세 이상입니다.</label></div>
			<div class="agree-wrapper"><label><input id="check_usage" type="checkbox">(필수) 이용약관 동의</label><a onClick="open_dialog('dialog_rule')">전체 내용보기 ></a></div>
			<div class="agree-wrapper"><label><input id="check_privacy" type="checkbox">(필수) 개인정보 수집 및 이용동의</label><a onClick="open_dialog('dialog_privacy')">전체 내용보기 ></a></div>
			<div class="agree-input-wrapper">
				<div class="agree-input">이름<input type="text" placeholder="" class="signin_sms" id="user_name"></div>
				<div class="agree-input">휴대폰<input type="text" placeholder="'-'없이 입력해주세요." class="signin_sms" id="user_phone"><a onClick="send_code()">인증번호 발송 ></a></div>
				<div class="agree-input">인증번호<input type="number" placeholder="인증번호 6자리를 입력해주세요." class="signin_sms" id="user_code"></div>
			</div>
			<div class="agree-btn" onClick="check_code()">SMS 인증</div>
		</div>
	</div>
</div>

<div class="dialog_shadow" id="dialog_signup">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_signup')">
		</div>
		<div class="dialog_content-wrapper">
			<p class="dialog-title">정육각 회원가입</p>
			<p class="dialog-subtitle">(로그인정보 입력)</p>
			<div class="agree-input-wrapper">
				<div class="signup-input">아이디<input type="email" placeholder="이메일주소를 입력해주세요." class="signin_info" id="user_id"></div>
				<div class="signup-input">비밀번호<input type="password" placeholder="최소 8자리 이상의 비밀번호를 입력해주세요." class="signin_info" id="user_pw"></div>
				<div class="signup-input">비밀번호확인<input type="password" placeholder="비밀번호를 한번 더 입력해주세요." class="signin_info" id="user_pwpw"></div>
			</div>
			<div class="agree-btn" id="signup_confirm_btn">가입하기</div>
		</div>
	</div>
</div>
<div id="sidebar-shadow"></div>
<div class="sidebar-signin" id="sidebar_signin">
	<div class="sidebar-content" id="sidebar_sign">
		<img class="close_sidebar_m" src="img/common/close.jpg" onClick="close_sidebar('sidebar_signin')">
		<div class="signin_head">
			<img src="img/common/icon_sign.png">
			<span>로그인</span>
		</div>
		<div class="signin_body">
			<input type="email" placeholder="아이디(이메일)" class="signin_input" id="customer_id">
			<input type="password" placeholder="비밀번호" class="signin_input" id="customer_pw" onKeydown="javascript:if(event.keyCode == 13){signin();}">
			<div class="sign_control">
				<button class="signup_btn" onClick="signup()">회원가입 ></button>
				<button class="findpw_btn" onClick="findpw()">비밀번호찾기 ></button>
			</div>
		</div>
		<div class="signin_tail">
			<img src="img/common/signin.jpg" class="signin_btn" onClick="signin()">
		</div>
	</div>
</div>
<div class="sidebar-cart" id="sidebar_cart">
	<div class="sidebar-content" id="sidebar_main">
		<div class="cart_head">장바구니<img class="close_sidebar_m" src="img/common/close.jpg" onClick="close_sidebar('sidebar_cart')"></div>
		<div class="cart_body">
			<div class="cart_list" id="cart_list"></div>
		</div>
		<div class="cart_tail">
			<div class="cart_date"><span>도착희망일</span><span id="cart_shippingdate"></span><div id="cart_prior" onClick="date_prior()">-</div><div id="cart_next" onClick="date_next()">+</div></div>
			<div class="cart_price-wrapper">
				<div class="cart_price">상품<span id="cart_sum"></span></div>
				<div class="cart_price">배송비<span id="cart_shipping"></span></div>
				<div class="cart_price">신선할인<span id="cart_echo"></span></div>
				<div class="cart_price net">합계(부가가치세 포함)<span id="cart_total"></span></div>
			</div>
			<div class="cart_purchase" onClick="link_payment()">주&nbsp;문&nbsp;하&nbsp;기</div>
		</div>
		<div class="cart_empty" id="cart_empty">
			<div class="cart_empty_icon"><img src="img/common/icon_empty.png" /></div>
			<p>장바구니가 비어있습니다.</p>
			<a class="cart_empty_button" href="index_m.php#index_goods">사러가기</a>
		</div>
	</div>
</div>
<div class="sidebar-menu" id="sidebar_menu">
	<div class="sidebar-content" id="sidebar_men">
		<div class="menu_head">메뉴 전체보기<img class="close_sidebar_m" src="img/common/close.jpg" onClick="close_sidebar('sidebar_menu')"></div>
		<div class="menu_body">
			<div class="menu_item"><a onClick="close_sidebar('sidebar_menu')" href="index_m.php#index_goods">사러가기</a></div>
			<div class="menu_item"><a href="aboutus_m.php">정육각 소개</a></div>
			<div class="menu_item nav_event"><a href="event_m.php">이벤트</a></div>
			<div class="menu_item nav_event"><a href="review_m.php">후기</a></div>
			<div class="menu_item"><a href="support_m.php">고객센터</a></div>
		</div>
	</div>
</div>

<div class="custom_nav">
	<div class="custom_ic ic_menu" onClick="open_sidebar('sidebar_menu')"><img src="img/common/icon_menu.png"></div>
	<img src="img/main/main_logo.jpg" class="custom_nav-logo" onClick="link_index()">
	<div class="custom_ic ic_cart" onClick="open_sidebar('sidebar_cart')"><img src="img/common/icon_cart.png"></div>
	<div class="custom_ic ic_sign" onClick="open_sidebar('sidebar_signin')"><img src="img/common/icon_sign.png"></div>
</div>

<div class="container event_image-box">
	<div class="row event_image-wrapper-top">
		<div class="event_banner_top"></div>
		<div class="event_banner_title">이벤트</div>
		<div class="event_banner_text">정육각에서 만날 수 있는 즐거운 이벤트들에 참여해보세요.</div>
	</div>
	<div class="row event_image-wrapper">
		<img src="img/event/jyg-event-banner-jygday.jpg" class="event_image event_image_jygday" onClick="link_eventdetail('jygday')" style="display: none">
		<img src="img/event/jyg-event-banner-signup.jpg" class="event_image event_image_signup">
		<img src="img/event/jyg-event-banner-recommend.jpg" class="event_image event_image_recommend" onClick="link_eventdetail('recommend')">
		<img src="img/event/jyg-event-banner-review.jpg" class="event_image event_image_review" onClick="link_eventdetail('review')">
	</div>
</div>

<footer class="footer cgrey">
	<div class="container">
	<div class="row">
		<div class="col-xs-12 footer-text">
			<p class="text-muted">개인정보관리책임자 김환민 | 사업자등록번호 224-87-00271 | 통신판매업신고번호 2016-대전유성-0475호</p>
			<p class="text-muted">주식회사 정육각 | 대표 김재연 | 주소 대전광역시 유성구 진잠로 106번길 5-23 (주) 정육각</p>
			<p class="text-muted">
				<a onClick="open_dialog('dialog_rule')">이용약관</a>
				<a onClick="open_dialog('dialog_privacy')">개인정보취급방침</a>
				전화 1800-0658 이메일 help@yookgak.com</p>
			<p class="text-muted">All Copyrights reserved @ JeongYookGak.com 2016 - design by CreativeCreator</p>
		</div>

	</div>
	</div>
</footer>
	<iframe id="smsFrame" src="sms.html" style="display:none"></iframe>
	<!--link rel="stylesheet" type="text/css" href="css/index.css"-->
    <link rel="stylesheet" type="text/css" href="css/component_m.css">
    <!-- 공통 적용 스크립트 , 모든 페이지에 노출되도록 설치. 단 전환페이지 설정값보다 항상 하단에 위치해야함 --> 
<script type="text/javascript" src="http://wcs.naver.net/wcslog.js"> </script> 
<script type="text/javascript"> 
if (!wcs_add) var wcs_add={};
wcs_add["wa"] = "s_313ececb53dd";
if (!_nasa) var _nasa={};
wcs.inflow();
wcs_do(_nasa);
</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script-->
	<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
	<script src="js_event/event.js"></script>
	<script src="js_share/call.js"></script>
	<script src="js_share/common.js"></script>
  </body>
</html>