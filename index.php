<?php
	$PATH_URL = 'https://jyg-custom.firebaseio.com/';
	$PATH_TARGET = 'json/date_info';
	$PATH_KEY = '?auth=BoRpAIVB2dzISgKkNBAKnc9s4IchG3O8YT2wiL21';
	$url = 'https://jyg-custom.firebaseio.com/'.$PATH_TARGET.'.json'.$PATH_KEY;
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$useragent=$_SERVER['HTTP_USER_AGENT'];
$hostname=$_SERVER['HTTP_HOST'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
header('Location: http://'.$hostname.'/index_m.php');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 
header("Pragma: no-cache");
$timeString = date( 'Y-m-d', strtotime ("+16 hours") );
?>
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--meta name="viewport" content="width=device-width, initial-scale=1"-->
    <meta http-equiv="cache-control" content="no-store">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
		<link rel="canonical" href="http://www.jeongyookgak.com/index.php" hreflang="ko-kr" />
		<meta name="description" content="초신선을 먹는 습관, 정육각 - 돼지고기, 닭고기, 달걀 판매 서비스">
		<meta property="fb:app_id" content="177874689343722" />
		<meta property="fb:pages" content="520405091347902" />
		<meta content="website" property="og:type"/>
		<meta property="og:title" content="정육각" />
		<meta property='og:description' content='초신선 식재료 (돼지고기, 닭고기, 달걀) 생산·판매 서비스' />
		<meta property="og:image" content="http://www.jeongyookgak.com/img/main/jyg-banner2.jpg" />
		<meta property="og:url" content="http://www.jeongyookgak.com" />
		<meta name="naver-site-verification" content="22389b40380ec9085e1538e21a5feaab21139fb8"/>
    
    <title>정육각</title>
    <link rel="shortcut icon" href="/img/jyg.ico" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/component.css">
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"-->
	<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-84539077-2', 'auto');
		ga('send', 'pageview');
	</script>
  </head>
  <body class="noselect">
<script>
  window.fbAsyncInit = function() {FB.init({appId: '177874689343722',xfbml: true,version: 'v2.9'});FB.AppEvents.logPageView();};
  (function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) {return;}js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
</script>
<div class="dialog_shadow" id="dialog_notice" style="display: none">
	<div class="dialog_popup rule" style="background-color: #ECE9E4;">
		<div class="dialog_text-wrapper" style="overflow: hidden; height: 620px">
			<img src="img/notice/notice_dialog.jpg?v=1" width="520px" style="margin-bottom: 40px; margin-top: 70px;">
			<div style="border-top: 1px solid #999999">
			<button style="border: 0; background: none; float: right" onClick="close_dialog('dialog_notice')">닫기</button>
			<button style="border: 0; background: none; float: right; margin-right: 20px;" onClick="close_dialog_today('dialog_notice')">오늘하루 그만보기</button>
			</div>
		</div>
	</div>
</div>
<div class="dialog_shadow" id="dialog_rule">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_rule')">
		</div>
		<div class="dialog_text-wrapper">
			<span id="agreement_usage"></span>
		</div>
	</div>
</div>
<div class="dialog_shadow" id="dialog_privacy">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_privacy')">
		</div>
		<div class="dialog_text-wrapper">
			<span id="agreement_privacy"></span>
		</div>
	</div>
</div>
<div class="dialog_shadow" id="dialog_agreement">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_agreement')">
		</div>
		<div class="dialog_content-wrapper">
			<p class="dialog-title">정육각 회원가입</p>
			<p class="dialog-subtitle">(전화번호 인증)</p>
			<div class="agree-wrapper"><label><input id="check_all" name="check_all" type="checkbox">전체동의</label></div>
			<div class="agree-wrapper"><label><input id="check_age" type="checkbox">(필수) 만 14세 이상입니다.</label></div>
			<div class="agree-wrapper"><label><input id="check_usage" type="checkbox">(필수) 이용약관 동의</label><a onClick="open_dialog('dialog_rule')">전체 내용보기 ></a></div>
			<div class="agree-wrapper"><label><input id="check_privacy" type="checkbox">(필수) 개인정보 수집 및 이용동의</label><a onClick="open_dialog('dialog_privacy')">전체 내용보기 ></a></div>
			<div class="agree-input-wrapper">
				<div class="agree-input">이름<input type="text" placeholder="" class="signin_sms" id="user_name"></div>
				<div class="agree-input">휴대폰<input type="text" placeholder="'-'없이 입력해주세요." class="signin_sms" id="user_phone"><a onClick="send_code()">인증번호 발송 ></a></div>
				<div class="agree-input">인증번호<input type="number" placeholder="인증번호 6자리를 입력해주세요." class="signin_sms" id="user_code"></div>
			</div>
			<div class="agree-btn" onClick="check_code()">SMS 인증</div>
		</div>
	</div>
</div>

<div class="dialog_shadow" id="dialog_signup">
	<div class="dialog_popup rule">
		<div class="dialog_close-wrapper">
			<img src="img/common/close.jpg" onClick="close_dialog('dialog_signup')">
		</div>
		<div class="dialog_content-wrapper">
			<p class="dialog-title">정육각 회원가입</p>
			<p class="dialog-subtitle">(로그인정보 입력)</p>
			<div class="agree-input-wrapper">
				<div class="signup-input">아이디<input type="email" placeholder="이메일주소를 입력해주세요." class="signin_info" id="user_id"></div>
				<div class="signup-input">비밀번호<input type="password" placeholder="최소 8자리 이상의 비밀번호를 입력해주세요." class="signin_info" id="user_pw"></div>
				<div class="signup-input">비밀번호확인<input type="password" placeholder="비밀번호를 한번 더 입력해주세요." class="signin_info" id="user_pwpw"></div>
			</div>
			<div class="agree-btn" id="signup_confirm_btn">가입하기</div>
		</div>
	</div>
</div>
<div id="sidebar-shadow"></div>
<div class="sidebar-signin" id="sidebar_signin">
	<div class="sidebar-content" id="sidebar_sign">
		<div class="signin_head">
			<img src="img/common/icon_sign.png">
			<span>로그인</span>
		</div>
		<div class="signin_body">
			<input type="email" placeholder="아이디(이메일)" class="signin_input" id="customer_id">
			<input type="password" placeholder="비밀번호" class="signin_input" id="customer_pw" onKeydown="javascript:if(event.keyCode == 13){signin();}">
			<div class="sign_control">
				<button class="signup_btn" onClick="signup()">회원가입 ></button>
				<button class="findpw_btn" onClick="findpw()">비밀번호찾기 ></button>
			</div>
		</div>
		<div class="signin_tail">
			<img src="img/common/signin.jpg" class="signin_btn" onClick="signin()">
		</div>
	</div>
</div>
<div class="sidebar-cart" id="sidebar_cart">
	<div class="sidebar-content" id="sidebar_main">
		<div class="cart_head">장바구니</div>
		<div class="cart_body">
			<div class="cart_list" id="cart_list">
				<!--div class="cart_item">
					<div class="citem-name"><span>정육각 돼지고기 삼겹살</span><img src="img/common/close.jpg" class="citem-remove"></div>
					<div class="citem-option">구이용 두껍게</div>
					<div class="citem-select">
						<span class="citem_tag">수량</span>
						<span class="citem-quantity">3근</span>
						<div class="cart_less" onClick="care_less()">-</div><div class="cart_more" onClick="care_less()">+</div>
						<span class="citem-price">13,400원</span>
					</div>
				</div-->
			</div>
		</div>
		<div class="cart_tail">
			<div class="cart_date"><span>도착희망일</span><span id="cart_shippingdate"></span><div id="cart_prior" onClick="date_prior()">-</div><div id="cart_next" onClick="date_next()">+</div></div>
			<div class="cart_price-wrapper">
				<div class="cart_price">상품<span id="cart_sum"></span></div>
				<div class="cart_price">배송비<span id="cart_shipping"></span></div>
				<div class="cart_price">신선할인<span id="cart_echo"></span></div>
				<div class="cart_price net">합계(부가가치세 포함)<span id="cart_total"></span></div>
			</div>
			<div class="cart_purchase" onClick="link_payment()">주&nbsp;문&nbsp;하&nbsp;기</div>
		</div>
		<div class="cart_empty" id="cart_empty">
			<div class="cart_empty_icon"><img src="img/common/icon_empty.png" /></div>
			<p>장바구니가 비어있습니다.</p>
		</div>
	</div>
</div>

<div id="following_nav" class="navbar navbar-static-top" role="navigation" style="height: 140px; position: absolute; top:0px;">
     <div id="event_banner">
 	     <div class="container">
 	     	<span class="text_banner" id="event_banner_top">이벤트 정보를 불러오는 중입니다!</span>
 	     	<span id="close_banner" onclick="removeBanner()"></span>
 	     	
		  </div>
	  </div>
      <div class="container">
        <div class="navbar-header">
          <img src="img/main/main_logo.jpg" class="navbar-logo" onClick="link_index()">
        </div>
        <div>
         <ul class="nav navbar-nav navbar-left">
         	<li><a id="menu_top" href="index.php#index_scrolltarget">사러가기</a></li>
         	<li><a href="aboutus.php">정육각</a></li>
         	<li class="nav_event"><a href="event.php">이벤트</a></li>
         	<li class="nav_event"><a href="review.php">후기</a></li>
         	<li><a href="support.php">고객센터</a></li>
		 </ul>
          <ul class="nav navbar-nav navbar-right">
            <li onClick="open_sidebar('sidebar_signin')" ><img src="img/common/icon_sign.png" class="nav_icon"></li>
            <li onClick="open_sidebar('sidebar_cart')" ><img src="img/common/icon_cart.png" class="nav_icon"></li>
          </ul>
        </div>
      </div>
    </div>
    
<div class="container-fluid" id="index_main_container_1" style="top:140px">
	<!--img src="img/main/jyg-banner1.jpg" style="height:545px; width:1920px"/-->
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <!--li data-target="#carousel-example-generic" data-slide-to="2"></li-->
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="img/main/jyg-banner1.jpg" alt="jyg-banner-1">
      <div class="carousel-caption">
      </div>
    </div>
    <div class="item">
      <img src="img/main/jyg-banner2.jpg" alt="jyg-banner-2">
      <div class="carousel-caption">
      </div>
    </div>
    <!--div class="item">
      <img src="img/main/jyg-banner1.jpg" alt="jyg-banner-3">
      <div class="carousel-caption">
      </div>
    </div-->
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>



<div class="container-fluid cgrey" id="index_main_container_2" style="top:140px">
	<div class="container">
		<div class="row">
			<div class="col-xs-3">
				<div class="box_hover" onClick="select_detail('porkbelly-clean')">
					<img src="img/date/date_porkclean.jpg" class="box_image">
					<p>무항생제 돼지고기(1+등급) 도축일</p>
					<div id="pork-clean-date">
						<img class="date-card front" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card last" src="img/date/none.jpg">
					</div>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="box_hover" onClick="select_detail('porkbelly-fresh')">
					<img src="img/date/date_porkfresh.jpg" class="box_image">
					<p>돼지고기(1+등급) 도축일</p>
					<div id="pork-fresh-date">
						<img class="date-card front" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card last" src="img/date/none.jpg">
					</div>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="box_hover" onClick="select_detail('chicken-whole')">
					<img src="img/date/date_chicken.jpg" class="box_image">
					<p>무항생제 닭고기(1등급) 도계일</p>
					<div id="chicken-fresh-date">
						<img class="date-card front" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card last" src="img/date/none.jpg">
					</div>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="box_hover" onClick="select_detail('egg-fresh')">
					<img src="img/date/date_egg.jpg" class="box_image">
					<p>무항생제 달걀(특) 산란일</p>
					<div id="egg-fresh-date">
						<img class="date-card front" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card" src="img/date/none.jpg">
						<img class="date-card middle" src="img/date/none.jpg">
						<img class="date-card last" src="img/date/none.jpg">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3"></div>
			<div class="col-xs-6">
				<h3 class="phrase-catch"><b>초신선</b>을 먹는 습관</h3>
			  	<p class="subphrase-catch"><span id="date_standard">예상배송일</span>에 수령하실 정육각의 도축일/도계일/산란일을 확인하세요.</p>
			</div>
			<div class="col-xs-3"></div>
		</div>
	</div>
</div>
<div class="container" id="index_goods" style="top:140px">
  <div class="row">
    <div class="col-xs-3"></div>
    <div class="col-xs-6 phrase-good-wrapper">
     <img src="img/main/main_sublogo.jpg" class="phrase-logo" id="index_scrolltarget">
		<h3 class="phrase-good"><b>정육각</b>의 <b>초신선</b></h3>
      <p class="subphrase-good">이제는 자연의 초신선을 손쉽게 집에서 만나보실 수 있습니다.</p>
    </div>
    <div class="col-xs-3">
    </div>
  </div>
  <div class="row" id="maingoods_row_0">
    <div class="col-xs-4">
    	<img src="img/good_index/porkbelly-clean-index.jpg" class="simage">
		<div class="swrapper">
			<div class="soption-wrapper">
				<span>옵션</span>
				<div>
					<div id="porkbelly-clean-op0" onClick="select_option('porkbelly-clean-op0')">구이용-보통(16mm)</div>
					<div id="porkbelly-clean-op1" onClick="select_option('porkbelly-clean-op1')">구이용-얇게(11mm)</div>
					<div id="porkbelly-clean-op2" onClick="select_option('porkbelly-clean-op2')">구이용-두껍게(24mm)</div>
					<div id="porkbelly-clean-op3" onClick="select_option('porkbelly-clean-op3')">수육용</div>
				</div>
			</div>
			<div class="squantity-wrapper">수량
				<div class="squantity-more">+</div>
				<div class="squantity-less">-</div>
				<div class="squantity-count" id="porkbelly-clean-scount">3</div>
				<span>개</span>
			</div>
			<div class="sbutton-detail" onClick="select_detail('porkbelly-clean')">상세보기</div>
			<div class="sbutton-cart" onClick="select_add('porkbelly-clean')">장바구니</div>
		</div>
    	<div class="stext">
			<p id="porkbelly-clean-sname" class="sname">무항생제 돼지고기 삼겹살</p>
			<p id="porkbelly-clean-sweight" class="sweight">600g</p>
			<p id="porkbelly-clean-sprice"class="sprice"></p>
		</div>
    </div>
    <div class="col-xs-4">
    	<img src="img/good_index/porkneck-clean-index.jpg" class="simage">
		<div class="swrapper">
			<div class="soption-wrapper">
				<span>옵션</span>
				<div>
					<div id="porkneck-clean-op0" onClick="select_option('porkneck-clean-op0')">구이용-보통(16mm)</div>
					<div id="porkneck-clean-op1" onClick="select_option('porkneck-clean-op1')">구이용-얇게(11mm)</div>
					<div id="porkneck-clean-op2" onClick="select_option('porkneck-clean-op2')">구이용-두껍게(24mm)</div>
					<div id="porkneck-clean-op3" onClick="select_option('porkneck-clean-op3')">수육용</div>
				</div>
			</div>
			<div class="squantity-wrapper">수량
				<div class="squantity-more">+</div>
				<div class="squantity-less">-</div>
				<div class="squantity-count" id="porkneck-clean-scount">3</div>
				<span>개</span>
			</div>
			<div class="sbutton-detail" onClick="select_detail('porkneck-clean')">상세보기</div>
			<div class="sbutton-cart" onClick="select_add('porkneck-clean')">장바구니</div>
		</div>
    	<div class="stext">
			<p id="porkneck-clean-sname" class="sname">무항생제 돼지고기 목살</p>
			<p id="porkneck-clean-sweight" class="sweight">600g</p>
			<p id="porkneck-clean-sprice"class="sprice"></p>
		</div>
    </div>
    <div class="col-xs-4">
    	<img src="img/good_index/porkbelly-fresh-index.jpg" class="simage">
		<div class="swrapper">
			<div class="soption-wrapper">
				<span>옵션</span>
				<div>
					<div id="porkbelly-fresh-op0" onClick="select_option('porkbelly-fresh-op0')">구이용-보통(16mm)</div>
					<div id="porkbelly-fresh-op1" onClick="select_option('porkbelly-fresh-op1')">구이용-얇게(11mm)</div>
					<div id="porkbelly-fresh-op2" onClick="select_option('porkbelly-fresh-op2')">구이용-두껍게(24mm)</div>
					<div id="porkbelly-fresh-op3" onClick="select_option('porkbelly-fresh-op3')">수육용</div>
				</div>
			</div>
			<div class="squantity-wrapper">수량
				<div class="squantity-more">+</div>
				<div class="squantity-less">-</div>
				<div class="squantity-count" id="porkbelly-fresh-scount">3</div>
				<span>개</span>
			</div>
			<div class="sbutton-detail" onClick="select_detail('porkbelly-fresh')">상세보기</div>
			<div class="sbutton-cart" onClick="select_add('porkbelly-fresh')">장바구니</div>
		</div>
    	<div class="stext">
			<p id="porkbelly-fresh-sname" class="sname">정육각 돼지고기 삼겹살</p>
			<p id="porkbelly-fresh-sweight" class="sweight">600g</p>
			<p id="porkbelly-fresh-sprice"class="sprice"></p>
		</div>
    </div>
  </div>
	<div class="row" id="maingoods_row_1">
		<div class="col-xs-4">
			<img src="img/good_index/porkneck-fresh-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="porkneck-fresh-op0" onClick="select_option('porkneck-fresh-op0')">구이용-보통(16mm)</div>
						<div id="porkneck-fresh-op1" onClick="select_option('porkneck-fresh-op1')">구이용-얇게(11mm)</div>
						<div id="porkneck-fresh-op2" onClick="select_option('porkneck-fresh-op2')">구이용-두껍게(24mm)</div>
						<div id="porkneck-fresh-op3" onClick="select_option('porkneck-fresh-op3')">수육용</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="porkneck-fresh-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('porkneck-fresh')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('porkneck-fresh')">장바구니</div>
			</div>
			<div class="stext">
				<p id="porkneck-fresh-sname" class="sname">정육각 돼지고기 목살</p>
				<p id="porkneck-fresh-sweight" class="sweight">600g</p>
				<p id="porkneck-fresh-sprice"class="sprice"></p>
			</div>
    	</div>
    	<div class="col-xs-4">
			<img src="img/good_index/chicken-whole-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="chicken-whole-op0" onClick="select_option('chicken-whole-op0')">보통(1.1kg)</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="chicken-whole-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('chicken-whole')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('chicken-whole')">장바구니</div>
			</div>
			<div class="stext">
				<p id="chicken-whole-sname" class="sname">정육각 통닭</p>
				<p id="chicken-whole-sweight" class="sweight">1.1kg</p>
				<p id="chicken-whole-sprice"class="sprice"></p>
			</div>
    	</div>
    	<div class="col-xs-4">
			<img src="img/good_index/chicken-cut-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="chicken-cut-op0" onClick="select_option('chicken-cut-op0')">보통(900g)</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="chicken-cut-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('chicken-cut')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('chicken-cut')">장바구니</div>
			</div>
			<div class="stext">
				<p id="chicken-cut-sname" class="sname">정육각 닭볶음탕</p>
				<p id="chicken-cut-sweight" class="sweight">900g</p>
				<p id="chicken-cut-sprice"class="sprice"></p>
			</div>
    	</div>
	</div>
	<div class="row" id="maingoods_row_2">
		<div class="col-xs-4">
			<img src="img/good_index/chicken-breast-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="chicken-breast-op0" onClick="select_option('chicken-breast-op0')">보통(550g)</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="chicken-breast-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('chicken-breast')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('chicken-breast')">장바구니</div>
			</div>
			<div class="stext">
				<p id="chicken-breast-sname" class="sname">정육각 닭가슴살</p>
				<p id="chicken-breast-sweight" class="sweight">550g</p>
				<p id="chicken-breast-sprice"class="sprice"></p>
			</div>
    	</div>
    	<div class="col-xs-4">
			<img src="img/good_index/chicken-inner-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="chicken-inner-op0" onClick="select_option('chicken-inner-op0')">보통(550g)</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="chicken-inner-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('chicken-inner')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('chicken-inner')">장바구니</div>
			</div>
			<div class="stext">
				<p id="chicken-inner-sname" class="sname">정육각 닭안심</p>
				<p id="chicken-inner-sweight" class="sweight">550g</p>
				<p id="chicken-inner-sprice"class="sprice"></p>
			</div>
    	</div>
    	<div class="col-xs-4">
			<img src="img/good_index/chicken-leg-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="chicken-leg-op0" onClick="select_option('chicken-leg-op0')">보통(550g)</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="chicken-leg-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('chicken-leg')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('chicken-leg')">장바구니</div>
			</div>
			<div class="stext">
				<p id="chicken-leg-sname" class="sname">정육각 닭다리살</p>
				<p id="chicken-leg-sweight" class="sweight">550g</p>
				<p id="chicken-leg-sprice"class="sprice"></p>
			</div>
    	</div>
	</div>
	<div class="row" id="maingoods_row_3">
		<div class="col-xs-4 col-xs-offset-4">
			<img src="img/good_index/egg-fresh-index.jpg" class="simage">
			<div class="swrapper">
				<div class="soption-wrapper">
					<span>옵션</span>
					<div>
						<div id="egg-fresh-op0" onClick="select_option('egg-fresh-op0')">팩(12개)</div>
					</div>
				</div>
				<div class="squantity-wrapper">수량
					<div class="squantity-more">+</div>
					<div class="squantity-less">-</div>
					<div class="squantity-count" id="egg-fresh-scount">3</div>
					<span>개</span>
				</div>
				<div class="sbutton-detail" onClick="select_detail('egg-fresh')">상세보기</div>
				<div class="sbutton-cart" onClick="select_add('egg-fresh')">장바구니</div>
			</div>
			<div class="stext">
				<p id="egg-fresh-sname" class="sname">정육각초신선유정란</p>
				<p id="egg-fresh-sweight" class="sweight">12개</p>
				<p id="egg-fresh-sprice"class="sprice"></p>
			</div>
    	</div>
	</div>
	<div class="row" id="maingoods_row_4"></div>
</div>

<div class="event_banner" onClick="link_mainevent()" style="top:140px; display: none">
  <img src="img/event/jyg-event-main-banner.jpg">
</div>

<div class="container" id="tracking-wrapper" style="margin-top: 50px">
  <div class="row">
    <div class="col-xs-12 tracking-box">
		<div class="tracking-title">축산물 이력 확인</div>
   		<input type="text" placeholder="축산물 이력 번호 입력" class="tracking-input" id="tracking_code">
		<div class="tracking-btn" onClick="track_code()">조회하기</div>
    </div>
  </div>
</div>
</div>

<footer class="footer cgrey">
	<div class="container">
	<div class="row">
    	<div class="col-xs-2">
    		<img src="img/main/footer_logo.jpg" class="footer-img">
		</div>
		<div class="col-xs-10 footer-text">
			<p class="text-muted">개인정보관리책임자 김환민 | 사업자등록번호 224-87-00271 | 통신판매업신고번호 2016-대전유성-0475호</p>
			<p class="text-muted">주식회사 정육각 | 대표 김재연 | 주소 대전광역시 유성구 진잠로 106번길 5-23 (주) 정육각</p>
			<p class="text-muted">
				<a onClick="open_dialog('dialog_rule')">이용약관</a>
				<a onClick="open_dialog('dialog_privacy')">개인정보취급방침</a>
				전화 1800-0658 이메일 help@yookgak.com</p>
			<p class="text-muted">All Copyrights reserved @ JeongYookGak.com 2016 - design by CreativeCreator</p>
		</div>

	</div>
	</div>
</footer>
	<iframe id="smsFrame" src="sms.html" style="display:none"></iframe>

    <!-- 공통 적용 스크립트 , 모든 페이지에 노출되도록 설치. 단 전환페이지 설정값보다 항상 하단에 위치해야함 --> 
<script type="text/javascript" src="http://wcs.naver.net/wcslog.js"> </script> 
<script type="text/javascript"> 
if (!wcs_add) var wcs_add={};
wcs_add["wa"] = "s_313ececb53dd";
if (!_nasa) var _nasa={};
wcs.inflow();
wcs_do(_nasa);
</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script-->
	<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
	<script src="js/index.js?version=<?php echo date("hisa"); ?>"></script>
	<script src="js_share/call.js?version=<?php echo date("hisa"); ?>"></script>
	<script src="js_share/common.js?version=<?php echo date("hisa"); ?>"></script>
	<script type="text/javascript">
		var serverdate = <?php echo $timeString; ?>; function getServerdate() {return serverdate;}
		var date_info = <?php $result = curl_exec($ch); ?>;
		for(var key in date_info) {
			if(date_info[key]) {
				var dateHTML='<img class="date-card front" src="img/date/'+date_info[key][0]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][1]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][2]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][3]+'.jpg">'+
					'<img class="date-card middle" src="img/date/'+date_info[key][4]+'.jpg">'+
					'<img class="date-card" src="img/date/'+date_info[key][5]+'.jpg">'+
					'<img class="date-card middle" src="img/date/'+date_info[key][6]+'.jpg">'+
					'<img class="date-card last" src="img/date/'+date_info[key][7]+'.jpg">';
				if(key.indexOf('pork')!=-1) {
					dateHTML = dateHTML + '<span class="date-card-after">부터</span>';
				}
				document.getElementById(key+'-date').innerHTML=dateHTML;
			}
		}
	</script>
  </body>
</html>