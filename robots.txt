User-agent: *
Disallow: /pay_nice/
Disallow: /pay_easy/
Disallow: /cms_hs/
Disallow: /js_temp/
Disallow: /js/
Disallow: /js_aboutus/
Disallow: /js_complete/
Disallow: /js_detail/
Disallow: /js_event/
Disallow: /js_eventdetail/
Disallow: /js_mypage/
Disallow: /js_notice/
Disallow: /js_payment/
Disallow: /js_review/
Disallow: /js_share/
Disallow: /js_support/
Disallow: /fonts/
Disallow: /agreement/
Disallow: /product_detail/