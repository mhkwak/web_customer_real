<?php
	$urlcode = $_GET['code'];
	$urlby = $_GET['by'];

$useragent=$_SERVER['HTTP_USER_AGENT'];
$hostname=$_SERVER['HTTP_HOST'];
$query_string=$_SERVER['QUERY_STRING']; 

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 
header("Pragma: no-cache");
?>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--meta name="viewport" content="width=device-width, initial-scale=1"-->
    <meta http-equiv="cache-control" content="no-store">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
		<link rel="canonical" href="http://www.jeongyookgak.com/index.php" hreflang="ko-kr" />
		<meta name="description" content="초신선을 먹는 습관, 정육각 - 돼지고기, 닭고기, 달걀 판매 서비스">
		<meta property="fb:app_id" content="177874689343722" />
		<meta property="fb:pages" content="520405091347902" />
		<meta content="website" property="og:type"/>
		<meta property="og:title" content="정육각" />
		<meta property='og:description' content='초신선 식재료 (돼지고기, 닭고기, 달걀) 생산·판매 서비스' />
		<meta property="og:image" content="http://www.jeongyookgak.com/img/main/jyg-banner2.jpg" />
		<meta property="og:url" content="http://www.jeongyookgak.com" />
		<meta name="naver-site-verification" content="22389b40380ec9085e1538e21a5feaab21139fb8"/>
    
    <title>정육각</title>
    <link rel="shortcut icon" href="/img/jyg.ico" />
    <!-- 공통 적용 스크립트 , 모든 페이지에 노출되도록 설치. 단 전환페이지 설정값보다 항상 하단에 위치해야함 --> 
<script type="text/javascript" src="http://wcs.naver.net/wcslog.js"> </script> 
<script type="text/javascript"> 
if (!wcs_add) var wcs_add={};
wcs_add["wa"] = "s_313ececb53dd";
if (!_nasa) var _nasa={};
wcs.inflow();
wcs_do(_nasa);
</script>

    <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-84539077-2', 'auto');
		ga('send', 'pageview');
		location.href="index.php";
	</script>
</head>

<body>
</body>
</html>