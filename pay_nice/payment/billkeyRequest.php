
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>NICEPAY :: 빌키 발급 요청</title>
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script language="javascript">

function requestIt() {
 	var formNm = document.tranMgr;
	formNm.action = 'https://web.nicepay.co.kr/billing/step1.jsp';
	//formNm.action = 'http://172.31.61.90:9090/billing/step1.jsp';
    var left = (screen.Width - 545)/2;
    var top = (screen.Height - 573)/2;
    var winopts= "left="+left+",top="+top+",width=545,height=573,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=no";
    var win =  window.open("", "billWindow", winopts);

    formNm.target = "billWindow";
    formNm.submit();
}

</script>
</head>
<body>
<br>
<br>
<form name="tranMgr" method="post" action="billkeyResult.php">
<table width="632" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  	<td >
  	  <table width="632" border="0" cellspacing="0" cellpadding="0" class="title">
        <tr>
          <td width="35">&nbsp;</td>
          <td>빌키 발급</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" background="images/bodyMiddle.gif">
    <table width="632" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="35" height="10">&nbsp;</td> <!--상단여백 높이 10px -->
        <td width="562">&nbsp;</td>
        <td width="35">&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td>빌키 발급 샘플입니다. </td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td> <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td> 
        <td class="bold"><img src="images/bullet.gif" /> 정보를 기입하신 후 확인버튼을 눌러주십시오.
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td ><table width="562" border="0" cellspacing="0" cellpadding="0" class="talbeBorder" >
          
           <tr>
            <!-- 테이블 일반의 높이는 30px // 짝수행셀의 경우 class="thead02" 사용 -->
            <td width="100" height="30" id="borderBottom" class="thead02">* 빌링 방식</td> 
            <td id="borderBottom" ><select name="PayMethod">
							<option selected="selected" value="BILL">신용카드</option>
							<option value="MOBILE_BILLING">휴대폰</option>
						</select>
			</td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 상품명</td> 
            <!-- 테이블 일반의 높이는 30px // 홀수행셀의 경우 class="thead01" 사용 -->
            <td id="borderBottom" ><input name="GoodsName" type="text" value="월회비"/></td>
          </tr>
          <tr>
            <!-- 테이블 일반의 높이는 30px // 짝수행셀의 경우 class="thead02" 사용 -->
            <td width="100" height="30" id="borderBottom" class="thead02">* 구매자명</td> 
            <td id="borderBottom" ><input name="BuyerName" type="text" value="홍길동"/></td>
          </tr>
          
           <tr>
            <td width="100" height="30" id="borderBottom" class="thead02">* 상점아이디(MID)</td> 
            <td id="borderBottom" ><input name="MID" type="text" value="nictest04m"/></td>
          </tr>
           <tr>
            <td width="100" height="30" id="borderBottom" class="thead02">* 주문번호</td> 
            <td id="borderBottom" ><input name="Moid" type="text" value="test_012345"/></td>
          </tr>
           <tr>
            <!-- 테이블 일반의 높이는 30px // 짝수행셀의 경우 class="thead02" 사용 -->
            <td width="100" height="30" id="borderBottom" class="thead01">* 금 액</td> 
            <td id="borderBottom" ><input name="Amt" type="text" value="1004"/></td>
          </tr>
          
           <tr>
            <!-- 테이블 일반의 높이는 30px // 짝수행셀의 경우 class="thead02" 사용 -->
            <td width="100" height="30" id="borderBottom" class="thead02">* 휴대폰 상품구분</td> 
            <td id="borderBottom" ><select name="GoodsCl">
							<option selected="selected" value="1">[실물]</option>
							<option value="0">[컨텐츠]</option>
						</select>
			</td>
          </tr>
        </table></td>
        <td height="15">&nbsp;</td>
      </tr>
      <tr>
      	<td height="60"></td>
        <td class="btnCenter"><input type="button" value="요청하기" onClick="requestIt();">
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="15"></td>  <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td class="comment">* 표 항목은 반드시 기입해주시기 바랍니다.<br><br/>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="10"></td>  <!--하단여백 높이 10px -->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>  
    </table></td>
  </tr>
  <tr>
    <td><img src="images/bodyBottom.gif" /></td>
  </tr>
</table>
<!--  결과를 전달받을 URL을 기재하여 주십시요. -->
<input type="hidden" name="ReturnUrl" value="http://172.31.61.90:9090/demo/billkeyResult.php"/>
<!--  구매자 연락처 -->
<input type="hidden" name="BuyerTel" value="010-1234-1234" />
<!--  구매자 이메일 -->
<input type="hidden" name="BuyerEmail" value="test@abc.com"/>
<!--  상점에서 예비로 받을 추가 파라미터를 기재합니다. -->
<input type="hidden" name="MallReserved" value="NICE_IT|TEST1=1|TEST2=2" />

</form>
</body>
</html>