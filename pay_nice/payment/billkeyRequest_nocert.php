<?php 

 $MID="nictest04m";
 $Moid="Moid_1112";

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
<title>NICEPAY :: 빌키 발급 요청</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<script src="js/materialize.min.js"></script>
<script language="javascript">
<!--
function requestIt() {
	var formNm = document.tranMgr;
	
    if (formNm.CardNo.value == "") {
		alert('카드번호를 입력하세요.');
	} else if (formNm.ExpYear.value == "") {
		alert('유효기간을 입력하세요.');
	} else if (formNm.IDNo.value == "") {
		alert('주민번호를 입력하세요.');
	} else if (formNm.CardPw.value == "") {
	    alert('카드 비밀번호를 입력하세요.');
	} else {
	    formNm.submit();
	}
}
-->
</script>
</head>
<body>
<br>
<br>
<div class="container">
<div class="col s12 m10 l8">
<form name="tranMgr" method="post" action="billkeyResult_nocert.php">
	<div class="row">
		<div class="col s12 m6">카드번호</div>
		<div class="col s12 m6"><input name="CardNo" type="text" maxlength="16" size="20" value=""/></div>
	</div>
	<div class="row">
		<div class="col s12 m6">유효기간 (월/년)</div>
		<div class="col s6 m3"><input name="ExpMonth" type="text" value="" size=2 maxlength=2/></div>
		<div class="col s6 m3"><input name="ExpYear" type="text" value="" size=2 maxlength=2/></div>
	</div>
	<div class="row">
		<div class="col s12 m6">생년월일(YYMMDD)</div>
		<div class="col s6 m3"><input name="IDNo" type="password" value="" size="9" maxlength="6"/></div>
		<div class="col s6 m3"><input disabled name="IDNob" type="password" value="*******" size="9" maxlength="6"/></div>
	</div>
	<div class="row">
		<div class="col s12 m6">비밀번호 앞 두자리</div>
		<div class="col s6 m3"><input name="CardPw" type="password" value="" size=2 maxlength=2/></div>
		<div class="col s6 m3"><input disabled name="CardPwb" type="password" value="**" size=2 maxlength=2/></div>
	</div>
<input type="button" value="요청하기" onClick="requestIt();">
<input type="hidden" name="MID" value="<?php echo($MID); ?>">
<input type="hidden" name="PayMethod" value="BILLKEY">
<input type="hidden" name="Moid" value="<?php echo($Moid); ?>">
<input type="hidden" name="UID" value="<?php echo($_GET["uid"]); ?>">
<input type="hidden" name="card_id" value="<?php echo($_GET["cid"]); ?>">
</form>
</div>
</div>
</body>
</html>