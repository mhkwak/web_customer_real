<?php

	/**************************
	 * 1. 라이브러리 인클루드 *
	 **************************/
	require("../lib/NicepayLite.php");
	
	/***************************************
	 * 2. NicepayLite 클래스의 인스턴스 생성 *
	 ***************************************/
	$nicepay = new NicepayLite;
	// 로그 경로를 설정하여 주십시요.
	$nicepay->m_NicepayHome = "c:\log";	
	
	$nicepay->m_Moid = $Moid;
	$nicepay->m_MID = $MID;
	$nicepay->m_MallIP = $MallIP;
	$nicepay->m_PayMethod = $PayMethod;
	$nicepay->m_BillKey = $BillKey;
	$nicepay->m_ssl = "true";
	$nicepay->m_ActionType = "PYO";
	// 상점키를 설정한다.
  $nicepay->m_LicenseKey = "b+zhZ4yOZ7FsH8pm5lhDfHZEb79tIwnjsdA0FBXh86yLc6BJeFVrZFXhAoJ3gEWgrWwN+lJMV0W4hvDdbe4Sjw==";
  
	$nicepay->m_debug = "DEBUG";

	// PG에 접속하여 승인 처리를 진행.
	$nicepay->startAction();
	

 // DB 처리하세요
 
	
?>	


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>NICEPAY :: 빌키 사용 중지 결과</title>
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<br>
<table width="632" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  	<td >
  	  <table width="632" border="0" cellspacing="0" cellpadding="0" class="title">
        <tr>
          <td width="35">&nbsp;</td>
          <td>빌키 중지 결과</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" background="images/bodyMiddle.gif"><table width="632" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="35" height="10">&nbsp;</td> <!--상단여백 높이 10px -->
        <td width="562">&nbsp;</td>
        <td width="35">&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td>요청이 완료되었습니다.
        </td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td> <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td> 
        <td class="bold"><img src="images/bullet.gif" /> 상세 내역입니다.
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td ><table width="562" border="0" cellspacing="0" cellpadding="0" class="talbeBorder" >
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">결과 내용</td> 
            <td id="borderBottom" >&nbsp;[<?php echo($nicepay->m_ResultData["ResultCode"]); ?>] <?php echo($nicepay->m_ResultData["ResultMsg"]); ?></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">빌 키</td> 
            <td id="borderBottom" >&nbsp;<?php echo($nicepay->m_ResultData["BID"]); ?></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead02">시 간</td> 
            <td id="borderBottom" >&nbsp;<?php echo($nicepay->m_ResultData["AuthDate"]); ?></td>
          </tr>
        </table></td>
        <td height="15">&nbsp;</td>
      </tr>
      <tr>
        <td height="15"></td>  <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td class="comment">    
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="10"></td>  <!--하단여백 높이 10px -->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>  
    </table></td>
  </tr>
  <tr>
    <td><img src="images/bodyBottom.gif" /></td>
  </tr>
</table>
</body>
</html>