<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>NICEPAY :: 빌링 승인 요청</title>
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script language="javascript">
<!--
function requestIt() {
	var formNm = document.tranMgr;
	
	if (formNm.BillKey.value == "") {
		alert('빌키를 입력하세요.');
	} else if (formNm.Amt.value == "") {
		alert('상품가격을 입력하세요.');
	} else if (formNm.BuyerName.value == "") {
		alert('금액을 입력하세요.');
	} else {
	    formNm.submit();
	}
}
-->
</script>
</head>
<body onLoad="requestIt()">
<br>
<br>
<form name="tranMgr" method="post" action="billResult_customer.php">
<table width="632" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  	<td >
  	  <table width="632" border="0" cellspacing="0" cellpadding="0" class="title">
        <tr>
          <td width="35">&nbsp;</td>
          <td>빌링 승인 요청</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" background="images/bodyMiddle.gif">
    <table width="632" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="35" height="10">&nbsp;</td> <!--상단여백 높이 10px -->
        <td width="562">&nbsp;</td>
        <td width="35">&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td>빌링 승인 요청 샘플입니다. </td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td> <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td> 
        <td class="bold"><img src="images/bullet.gif" /> 정보를 기입하신 후 확인버튼을 눌러주십시오.
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td ><table width="562" border="0" cellspacing="0" cellpadding="0" class="talbeBorder" >
          
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 빌링 수단</td> 
            <!-- 테이블 일반의 높이는 30px // 홀수행셀의 경우 class="thead01" 사용 -->
            <td id="borderBottom" ><select name="PayMethod">
							<option selected="selected" value="BILL">신용카드</option>
							<option value="MOBILE_BILLING">휴대폰</option>
						</select>
					</td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 빌키</td> 
            <td id="borderBottom" ><input name="BillKey" type="text" maxlength="30" size="30" value="<?php echo($_POST["BillKey"]); ?>"/></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead02">* 구매자명</td> 
            <td id="borderBottom" ><input name="BuyerName" type="text" value="<?php echo($_POST["BuyerName"]); ?>"/></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 금액</td> 
            <td id="borderBottom" ><input name="Amt" type="text" value="<?php echo($_POST["Amt"]); ?>"/></td>
          </tr>
		   		<tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 상품명</td> 
            <td id="borderBottom" ><input name="GoodsName" type="text" value="<?php echo($_POST["GoodsName"]); ?>"/></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 상점아이디</td> 
            <td id="borderBottom" ><input name="MID" type="text" value="jeongyookm"/></td>
          </tr>
					 <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 할부개월</td> 
            
            <td id="borderBottom" ><input name="CardQuota" type="text" value="00"/></td>
          </tr>
           <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 주문번호</td> 
            <td id="borderBottom" ><input name="Moid" type="text" value="<?php echo($_POST["niceCode"]); ?>"/></td>
          </tr>
           <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">* 실물 컨텐츠 여부</td> 
            <td id="borderBottom" ><input name="GoodsCl" type="text" value="1"/>0: 컨텐츠, 1: 실물</td>
          </tr>
        </table></td>
        <td height="15">&nbsp;</td>
      </tr>
      <tr>
      	<td height="60"></td>
        <td class="btnCenter"><input type="button" value="요청하기" onClick="requestIt();"></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="15"></td>  
		<td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td class="comment">* 표 항목은 반드시 기입해주시기 바랍니다.<br><br/>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="10"></td>  <!--하단여백 높이 10px -->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>  
    </table></td>
  </tr>
  <tr>
    <td><img src="images/bodyBottom.gif" /></td>
  </tr>
</table>
</form>
</body>
</html>