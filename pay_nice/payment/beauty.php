<?php 

 //$MID="nictest04m";
 //$Moid="Moid_1112";
 $MID="jeongyookm";
 $Moid="JYGP_1702";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
<link rel="shortcut icon" href="http://www.jeongyookgak.com/favicon.ico" />
<title>정육각 신선페이 :: 카드등록</title>
<link rel="stylesheet" href="mdl/material.min.css">
<script src="mdl/material.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script language="javascript">
<!--
function requestIt() {
	var formNm = document.tranMgr;
	formNm.submit();
}
-->
</script>
</head>
<body>
<br>
<br>
<div class="container">
<div style="max-width: 300px; left: 50%; margin-left: -150px; position: absolute; width: 300px">
<form name="tranMgr" method="post" action="billkeyResult_new.php">
	<div style="width:100%; height: 65px;">
		<img src="http://www.jeongyookgak.com/favicon.ico" width="60px" height="60px" style="left: 50%; margin-left: -25px; position: absolute">
	</div>
	<div style="width:100%">
		<span>카드등록</span>
	</div>
	<div style="width:100%">
		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="CardNo" name="CardNo" maxlength="16" value="<?php echo($_POST["card_no"]); ?>">
			<label class="mdl-textfield__label" for="sample4">카드번호 16자리</label>
			<span class="mdl-textfield__error">숫자만 입력해주세요</span>
		  </div>
	</div>
	<div style="width:100%">
		<span>유효기간</span>
	</div>
	<div style="width:49%; display: inline-block">
		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="ExpMonth" name="ExpMonth" maxlength="2" value="<?php echo($_POST["card_month"]); ?>">
			<label class="mdl-textfield__label" for="sample4">월 2자리</label>
			<span class="mdl-textfield__error">숫자만 입력해주세요</span>
		  </div>
	</div>
	<div style="width:49%; display: inline-block">
		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="ExpYear" name="ExpYear" maxlength="2" value="<?php echo($_POST["card_year"]); ?>">
			<label class="mdl-textfield__label" for="sample4">연도 2자리</label>
			<span class="mdl-textfield__error">숫자만 입력해주세요</span>
		  </div>
	</div>
	<div style="width:100%">
		<div style="width:50%; display: inline-block">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="IDNo" name="IDNo" maxlength="6" value="<?php echo($_POST["card_birth"]); ?>">
			<label class="mdl-textfield__label" for="sample4">생년월일 6자리</label>
			<span class="mdl-textfield__error">숫자만 입력해주세요</span>
		  </div>
		</div>
		<span style="width: 50%">-*******</span>
	</div>
	<div style="width:100%">
		<div style="width:50%; display: inline-block">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			<input class="mdl-textfield__input" type="password" id="CardPw" name="CardPw" maxlength="2" value="<?php echo($_POST["card_pw"]); ?>">
			<label class="mdl-textfield__label" for="sample4">비밀번호 2자리</label>
			<span class="mdl-textfield__error">숫자만 입력해주세요</span>
		  </div>
		</div>
		<span style="width: 50%">**</span>
	</div>
	<div style="width:100%">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onClick="javascript:requestIt()" style="width: 100%">등록요청</button>
	</div>
<input type="hidden" name="default" value="">
<input type="hidden" name="MID" value="<?php echo($MID); ?>">
<input type="hidden" name="PayMethod" value="BILLKEY">
<input type="hidden" name="Moid" value="<?php echo($Moid); ?>">
<input type="hidden" id="nick" name="nick" value="<?php echo $_POST["tokenSecret"]; ?>">
<input type="hidden" id="UID" name="UID" value="<?php echo $_POST["tokenAddress"]; ?>">

</form>
</div>
</div>
</body>
</html>