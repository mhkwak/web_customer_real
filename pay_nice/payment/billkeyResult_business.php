<?php

	/**************************
	 * 1. 라이브러리 인클루드 *
	 **************************/
	require("../lib/NicepayLite.php");
	
	/***************************************
	 * 2. NicepayLite 클래스의 인스턴스 생성 *
	 ***************************************/
	$nicepay = new NicepayLite;
	// 로그 경로를 설정하여 주십시요.
	$nicepay->m_NicepayHome = "../log";	
	
	
	$nicepay->m_CardNo = $CardNo;
	$nicepay->m_ExpYear = $ExpYear;
	$nicepay->m_ExpMonth = $ExpMonth;
	$nicepay->m_IDNo = $IDNo;
	$nicepay->m_CardPw = $CardPw;

	$nicepay->m_MID = $MID;
	$nicepay->m_MallIP = $MallIP;
	$nicepay->m_PayMethod = $PayMethod;
	$nicepay->m_ssl = "true";
	$nicepay->m_ActionType = "PYO";
    $nicepay->m_LicenseKey = "b+zhZ4yOZ7FsH8pm5lhDfHZEb79tIwnjsdA0FBXh86yLc6BJeFVrZFXhAoJ3gEWgrWwN+lJMV0W4hvDdbe4Sjw==";

    
	// PG에 접속하여 승인 처리를 진행.
	$nicepay->startAction();
	

 // DB 처리하세요
 
	
?>	
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>정육각 기업고객 신선페이등록결과</title>
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body onLoad="init()">
	<p>결제정보를 받아오고 있습니다.</p>
	<p>잠시만 기다려주세요.</p>
	
	<input type="hidden" id="ResultCode" name="ResultCode" value="<?php echo($nicepay->m_ResultData["ResultCode"]); ?>" />
	<input type="hidden" id="ResultMsg" name="ResultMsg" value="<?php echo($nicepay->m_ResultData["ResultMsg"]); ?>" />
	<input type="hidden" id="BID" name="BID"       value="<?php echo($nicepay->m_ResultData["BID"]); ?>" />
	<input type="hidden" id="CardName" name="CardName"  value="<?php echo($nicepay->m_ResultData["CardName"]); ?>" />
	<input type="hidden" id="CardCode" name="CardCode"  value="<?php echo($nicepay->m_ResultData["CardCode"]); ?>" />
	<input type="hidden" id="LastCode" name="LastCode"  value="<?php echo $_POST["LastCode"]; ?>" />
	<input type="hidden" id="AuthDate" name="AuthDate"  value="<?php echo($nicepay->m_ResultData["AuthDate"]); ?>" />
	<input type="hidden" id="CardCl" name="CardCl"    value="<?php echo($nicepay->m_ResultData["CardCl"]); ?>" />
	<input type="hidden" id="UID" name="UID"    value="<?php echo $_POST["UID"]; ?>" />
</body>
	<script src="https://www.gstatic.com/firebasejs/3.6.9/firebase.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript">
		var config = {
			apiKey: "AIzaSyDFxzLGrjCGnF6Z1ULQd_f8a6fow37_nQQ",
			authDomain: "jyg-business.firebaseapp.com",
			databaseURL: "https://jyg-business.firebaseio.com",
			storageBucket: "jyg-business.appspot.com",
			messagingSenderId: "898317256888"
		};
		firebase.initializeApp(config);
		function init() {
			writeResult();
		}
		function writeResult() {
			var ResultCode = document.getElementById('ResultCode').value;
			var ResultMsg = document.getElementById('ResultMsg').value;
			var BID = document.getElementById('BID').value;
			var CardName = document.getElementById('CardName').value;
			var CardCode = document.getElementById('CardCode').value;
			var LastCode = document.getElementById('LastCode').value;
			var AuthDate = document.getElementById('AuthDate').value;
			var CardCl = document.getElementById('CardCl').value;
			var UID = document.getElementById('UID').value;
			var param = {
				ResultCode:ResultCode,
				ResultMsg:ResultMsg,
				BID:BID,
				CardName:CardName,
				CardCode:CardCode,
				LastCode:LastCode,
				AuthDate:AuthDate,
				CardCl: CardCl
			};
			if(UID==null) {
				alert("결제정보가 잘못되었습니다. 다시 시도해주시기 바랍니다.");
				location.href = 'http://www.jeongyookgak.com/b2b/business.html';
			}
			var userURL = 'https://jyg-business.firebaseio.com/user/'+UID+'/realtime.json?auth=MYnzKOG5lJjbOlFlLstFvay39eRvOBnUPcue2T3B';
			$.ajax({
				url: userURL,
				type: 'PUT',
				data: JSON.stringify(param),
				success: function() { location.href = 'http://www.jeongyookgak.com/b2b/business.html';
				}
			});
		}
	</script>
</html>