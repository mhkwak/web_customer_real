<?php

	/**************************
	 * 1. 라이브러리 인클루드 *
	 **************************/
	require("../lib/NicepayLite.php");
	
	/***************************************
	 * 2. NicepayLite 클래스의 인스턴스 생성 *
	 ***************************************/
	$nicepay = new NicepayLite;
	// 로그 경로를 설정하여 주십시요.
	$nicepay->m_NicepayHome = "../log";	
	
	
	$nicepay->m_CardNo = $CardNo;
	$nicepay->m_ExpYear = $ExpYear;
	$nicepay->m_ExpMonth = $ExpMonth;
	$nicepay->m_IDNo = $IDNo;
	$nicepay->m_CardPw = $CardPw;

	$nicepay->m_MID = $MID;
	$nicepay->m_MallIP = $MallIP;
	$nicepay->m_PayMethod = $PayMethod;
	$nicepay->m_ssl = "true";
	$nicepay->m_ActionType = "PYO";
    //$nicepay->m_LicenseKey = "b+zhZ4yOZ7FsH8pm5lhDfHZEb79tIwnjsdA0FBXh86yLc6BJeFVrZFXhAoJ3gEWgrWwN+lJMV0W4hvDdbe4Sjw==";
	$nicepay->m_LicenseKey = "zkPjGh17rMMHMtKeUjlLB98BJ75gp9WDMJCjyuK0ArR1ZvuNGCz87gvwBIc+Y7f5hAC/3qX9g2fr3rDRwuN4Zg==";

    
	// PG에 접속하여 승인 처리를 진행.
	$nicepay->startAction();
	

 // DB 처리하세요
 
	
?>	
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>정육각 신선페이 등록</title>
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body onLoad="init()">
	<p>결제정보를 받아오고 있습니다.</p>
	<p>잠시만 기다려주세요.</p>
	
	<input type="hidden" id="ResultCode" name="ResultCode" value="<?php echo($nicepay->m_ResultData["ResultCode"]); ?>" />
	<input type="hidden" id="ResultMsg" name="ResultMsg" value="<?php echo($nicepay->m_ResultData["ResultMsg"]); ?>" />
	<input type="hidden" id="BID" name="BID"       value="<?php echo($nicepay->m_ResultData["BID"]); ?>" />
	<input type="hidden" id="CardName" name="CardName"  value="<?php echo($nicepay->m_ResultData["CardName"]); ?>" />
	<input type="hidden" id="CardCode" name="CardCode"  value="<?php echo($nicepay->m_ResultData["CardCode"]); ?>" />
	<input type="hidden" id="LastCode" name="LastCode"  value="<?php echo $_POST["LastCode"]; ?>" />
	<input type="hidden" id="AuthDate" name="AuthDate"  value="<?php echo($nicepay->m_ResultData["AuthDate"]); ?>" />
	<input type="hidden" id="CardCl" name="CardCl"    value="<?php echo($nicepay->m_ResultData["CardCl"]); ?>" />
	<input type="hidden" id="nick" name="nick"    value="<?php echo $_POST["nick"]; ?>" />
	<input type="hidden" id="UID" name="UID"    value="<?php echo $_POST["UID"]; ?>" />
</body>
	<script src="https://www.gstatic.com/firebasejs/3.6.9/firebase.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript">
		var config = {
			apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
		authDomain: "jyg-custom.firebaseapp.com",
		databaseURL: "https://jyg-custom.firebaseio.com",
		storageBucket: "jyg-custom.appspot.com",
		messagingSenderId: "222150107123"
		};
		firebase.initializeApp(config);
		function init() {
			writeResult();
		}
		function writeResult() {
			var ResultCode = document.getElementById('ResultCode').value;
			var ResultMsg = document.getElementById('ResultMsg').value;
			var BID = document.getElementById('BID').value;
			var CardName = document.getElementById('CardName').value;
			var CardCode = document.getElementById('CardCode').value;
			var LastCode = document.getElementById('LastCode').value;
			var AuthDate = document.getElementById('AuthDate').value;
			var CardCl = document.getElementById('CardCl').value;
			var UID = document.getElementById('UID').value;
			var nick = document.getElementById('nick').value;
			var param = {
				ResultCode:ResultCode,
				ResultMsg:ResultMsg,
				BID:BID,
				CardName:CardName,
				CardCode:CardCode,
				LastCode:LastCode,
				AuthDate:AuthDate,
				CardCl: CardCl,
					res_cd:"0000",
					res_msg:nick,
					order_no:"order_no",
					user_nm:"user_nm",
					cno:BID,
					amount:"amount",
					auth_no:"auth_no",
					tran_date:"tran_date",
					pnt_auth_no:"pnt_auth_no",
					pnt_tran_date:"pnt_tran_date",
					cpon_auth_no:"cpon_auth_no",
					cpon_tran_date:"cpon_tran_date",
					card_no:"card_no",
					issuer_cd:"issuer_cd",
					issuer_nm:"issuer_nm",
					acquirer_cd:"acquirer_cd",
					acquirer_nm:"acquirer_nm",
					install_period:"install_period",
					noint:"noint",
					bank_cd:"bank_cd",
					bank_nm:"bank_nm",
					account_no:"account_no",
					deposit_nm:"deposit_nm",
					expire_date:"expire_date",
					cash_res_cd:"cash_res_cd",
					cash_res_msg:"cash_res_msg",
					cash_auth_no:"cash_auth_no",
					cash_tran_date:"cash_tran_date",
					auth_id:"auth_id",
					billid:"billid",
					mobile_no:"mobile_no",
					ars_no:"ars_no",
					cp_cd:"cp_cd",
					used_pnt:"used_pnt",
					remain_pnt:"remain_pnt",
					pay_pnt:"pay_pnt",
					accrue_pnt:"accrue_pnt",
					remain_cpon:"remain_cpon",
					used_cpon:"used_cpon",
					mall_nm:"mall_nm",
					escrow_yn:"escrow_yn",
					complex_yn:"complex_yn",
					canc_acq_date:"canc_acq_date",
					canc_date:"canc_date",
					refund_date:"refund_date",
					pay_type:"pay_type"
			};
			if(UID==null) {
				alert("결제정보가 잘못되었습니다. 다시 시도해주시기 바랍니다.");
				//location.href = 'http://www.jeongyookgak.com/cart_new.html';
				window.close();
			}
			
			var submit_get = read_json_customer('customers/'+UID+'/submits');
		
			
			var userURL = 'https://jyg-custom.firebaseio.com/customers/'+UID+'/submits.json?auth=xNeEGR6BPzFr39RfKD8EeVW9F0owfRgOXUMQOvPz&print=silent';
			var postData = {
				trypay: "success",
				type: "batch",
				option: "new",
				prefix: submit_get.prefix,
				result: param
			};
			if(ResultCode=="F100") {
				$.ajax({
					url: userURL,
					type: 'PUT',
					data: JSON.stringify(postData),
					success: function() {
						window.close();
						//location.href = 'http://www.jeongyookgak.com/cart_new.html';
					}
				});
			}else {
				alert(ResultMsg+'\n'+'다시시도해주세요.');
				window.close();
			}
		}
function read_json_customer(target_json) {
	var target = "https://jyg-custom.firebaseio.com/"+target_json+".json?auth=xNeEGR6BPzFr39RfKD8EeVW9F0owfRgOXUMQOvPz";
	var returnJson;
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", target, false);
	xhttp.send();
	if (xhttp.readyState == 4 && xhttp.status == 200) {
		return JSON.parse(xhttp.responseText);
	}
	return null;
}
		
	</script>
</html>