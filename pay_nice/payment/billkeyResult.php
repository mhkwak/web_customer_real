<?php
extract($_POST);
extract($_GET);

 // $billkey : 빌키
 // $resultCode : 결과 코드
 // $resultMsg : 결과 메시지
 // $cardCode : 카드 코드
 // $cardName : 카드사 이름
 // $GoodsName : 상품명
 // $Moid : 상품 주문번호
 // $MallReserved : 상점 예약 필드
 // $BuyerEmail : 구매자 이메일
 // $TID				: 휴대폰 결제시 거래 아이디
 // $Amt				: 금액
 // $PayMethod	: 빌링 인증 수단 (MOBILE_BILLING : 휴대폰 빌링)
 // DB 처리하세요
 
	
?>	
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>NICEPAY :: 빌키 발급 결과</title>
<link rel="stylesheet" href="css/basic.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script language='javascript'>
    function resizeSize() {
        window.resizeTo("700","530");      
    }
</script>
</head>
<body onload="resizeSize()">
<br>
<table width="632" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  	<td >
  	  <table width="632" border="0" cellspacing="0" cellpadding="0" class="title">
        <tr>
          <td width="35">&nbsp;</td>
          <td>빌키 발급 결과</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" background="images/bodyMiddle.gif"><table width="632" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="35" height="10">&nbsp;</td> <!--상단여백 높이 10px -->
        <td width="562">&nbsp;</td>
        <td width="35">&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td>빌키 발급 요청이 완료되었습니다.
        </td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td> <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td> 
        <td class="bold"><img src="images/bullet.gif" /> 빌키 발급내역입니다.
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td >&nbsp;</td>
        <td ><table width="562" border="0" cellspacing="0" cellpadding="0" class="talbeBorder" >
          <tr>
            <!-- 테이블 일반의 높이는 30px // 홀수행셀의 경우 class="thead01" 사용 -->
            <td width="100" height="30" id="borderBottom" class="thead01">결과 내용</td> 
            <td id="borderBottom" >&nbsp;[<? echo($resultCode);?>] <? echo($resultMsg);?></td>
          </tr>
          <tr>
            <!-- 테이블 일반의 높이는 30px // 홀수행셀의 경우 class="thead02" 사용 -->
            <td width="100" height="30" id="borderBottom" class="thead02">상품명</td> 
            <td id="borderBottom" >&nbsp;<? echo($GoodsName);?></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">빌키</td> 
            <td id="borderBottom" >&nbsp;<? echo($billkey);?></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead02">카드종류</td> 
            <td id="borderBottom" >&nbsp;<? echo($cardName);?> 카드 (<? echo($cardCode);?>)</td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">MallReserved</td> 
            <td id="borderBottom" >&nbsp;<? echo($MallReserved);?></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">거래 아이디(휴대폰결제시)</td> 
            <td id="borderBottom" >&nbsp;<? echo($TID);?></td>
          </tr>
          <tr>
            <td width="100" height="30" id="borderBottom" class="thead01">빌링 인증 방식</td> 
            <td id="borderBottom" >&nbsp;<? echo($PayMethod);?></td>
          </tr>
        </table></td>
        <td height="15">&nbsp;</td>
      </tr>
      <tr>
        <td height="15"></td>  <!--컨텐츠와 컨텐츠 사이 간격 15px-->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td class="comment">해당 빌키로의 승인은 빌키 승인당시 요청한 상품명만 가능합니다.        
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="10"></td>  <!--하단여백 높이 10px -->
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>  
    </table></td>
  </tr>
  <tr>
    <td><img src="images/bodyBottom.gif" /></td>
  </tr>
</table>
</body>
</html>