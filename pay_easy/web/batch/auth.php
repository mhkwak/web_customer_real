<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KICC EASYPAY 8.0 SAMPLE</title>
<meta name="robots" content="noindex, nofollow"> 
<meta http-equiv="content-type" content="text/html; charset=euc-kr">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<script language="javascript" src="http://104.198.5.183/payment/easypay_web/web/js/default.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.4.1/firebase.js"></script>
<script type="text/javascript">

	var config = {
		apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
		authDomain: "jyg-custom.firebaseapp.com",
		databaseURL: "https://jyg-custom.firebaseio.com",
		storageBucket: "jyg-custom.appspot.com",
		messagingSenderId: "222150107123"
	};
  	firebase.initializeApp(config);
  	var user = firebase.auth().currentUser;
  	var database = firebase.database();
	var firebase_flag = false;
	var firebase_flag = false;
	
	var keySize = 128;
	var iterations = iterationCount = 10000;
 
	var iv = "DD7270B13C99F37FD1B726BCED52E751";
	var salt = "71585FE01900EBAD3FF13A59225DE2882F8A52EC8CC627B94584CA01B6995A7B";
	var passPhrase = "passPhrase passPhrase aes encoding algorithm";
	 	
	function init() {
		var v_userId;
		var v_userPw;
		v_userId = document.getElementById('v_userId').value;
		v_userPw = document.getElementById('v_userPw').value;
				
		firebase.auth().signInWithEmailAndPassword(v_userId, v_userPw).then(function(user) {
			user.getToken().then(function(token) {
				document.getElementById('v_signIn').style.display = 'none';
		});
		}).catch(function(error) {
			var errorCode = error.code; var errorMessage = error.message;
			alert(errorCode + errorMessage);
		});
		current_Time();
	}
	function current_Time() {
		var tt = new Date();
		document.getElementById('current_time_real').innerHTML = tt.getDate();
	}
	
    firebase.auth().onAuthStateChanged(function(user) {
		if (user) {
			if(firebase_flag==false) {
				var isAnonymous = user.isAnonymous;
				var user_id = user.uid;
				firebase_flag = true;
				if(user_id=='q4gHxuEuFNRBAcrexywejBBRHsI2') {
					firebase_init();
				}else {
					signOut();
				}
			}
		} else {
			if(firebase_flag==true) {
				firebase_flag = false;
			}
		}
	});
	function signOut() {
		firebase.auth().signOut().then(function() {}, function(error) {});
		localStorage.clear();
		document.getElementById('status_table').innerHTML = "";
		document.getElementById('v_signIn').style.display = 'block';
	}
    /* 입력 자동 Setting */
	var customers;
	var todaysOrder = new Array();
	var ordersCount;
	function firebase_init() {
		firebase.database().ref().child('customers').once('value').then(function(snapshot) {
			var exist = (snapshot.val() !== null);
			if(exist==true) {
				customers = snapshot.val();
				ordersCount = 0;
				
				calcOrders();
			}
		});
	}
	function calcOrders() {
		var count=0;
		for(var key in customers) {
			var order_records = customers[key].order_records;
			for(var kei in order_records) {
				if(kei!='current') {
					var goods = order_records[kei].goods;
					var goodsCount = 0;
					var goodsPrice = 0;
					var goodsFlag = true;
					var g_name='';
					var g_option='';
					var g_price='';
					var g_index='';
					var g_weight='';
					for(var index in goods) {
						goodsCount++;
						g_name = g_name + ';' + goods[index].productID;
						g_option = g_option + ';' + goods[index].productOption;
						g_price = g_price + ';' + goods[index].productPrice;
						g_index = g_index + ';' + goods[index].productIndex;
						g_weight = g_weight + ';' + goods[index].productWeight;
						if(goods[index].productWeight==-1 || goods[index].productWeight=='undefined' || goods[index].productWeight==null) {
							//goodsPrice = goodsPrice + Number(goods[index].productPrice);
							goodsFlag = false;
						}else {
							goodsPrice = goodsPrice + Number(goods[index].productPrice);
						}
					}
					if(order_records[kei].payment.price=='0' || true) {
						todaysOrder[count] = {
								indexo:'',
								order_date:'',
								customer_name:'',
								customer_uid:'',
								customer_email:'',
								customer_phone:'',
								customer_address:'',
								shipping_date:'',
								productNumbers:'',
								production:'',
								productPrice:'',
								Point:'',
								Shipping:'',
								Pay_Price:'',
								Pay_Type:'',
								PAID:'',
								card_no:'',
								g_name:'',
								g_option:'',
								g_price:'',
								g_index:'',
								g_weight:''
							};
						todaysOrder[count].g_name = g_name;
						todaysOrder[count].g_option = g_option;
						todaysOrder[count].g_price = g_price;
						todaysOrder[count].g_index = g_index;
						todaysOrder[count].g_weight = g_weight;
						
						todaysOrder[count].indexo = count;
						todaysOrder[count].order_date = order_records[kei].histories[0].timestamp;
						todaysOrder[count].customer_name = order_records[kei].informations.customername;
						todaysOrder[count].customer_uid = key;
						todaysOrder[count].customer_email = order_records[kei].informations.customeremail;
						todaysOrder[count].customer_phone = order_records[kei].informations.customerphone;
						todaysOrder[count].customer_address = order_records[kei].informations.address;
						todaysOrder[count].shipping_date = order_records[kei].informations.shippingDate;
						todaysOrder[count].productNumbers = goodsCount;
						todaysOrder[count].production = goodsFlag;
						todaysOrder[count].productPrice = goodsPrice;
						todaysOrder[count].Point = order_records[kei].informations.pointUsed;
						todaysOrder[count].Shipping = "";
						todaysOrder[count].Pay_Price = order_records[kei].totalPrice;
						todaysOrder[count].Pay_Type = order_records[kei].payment.type;
						todaysOrder[count].PAID = order_records[kei].payment.price;
						todaysOrder[count].card_no = 'X';
						if(order_records[kei].payment.type=='batch' || order_records[kei].payment.type=='batch_confirmed') {
							todaysOrder[count].card_no = customers[key].cards[order_records[kei].payment.option].card_no;
						}
						count++;
					}
				}
			}
		}
		todaysOrder.sort(orderSort);
		ordersCount = count;
		loadDoc();
	}
	function orderSort(a, b) {
		if(a.shipping_date === b.shipping_date){
			if(a.customer_name === b.customer_name){ return 0; }
			return  a.customer_name < b.customer_name ? 1 : -1;
		} return  a.shipping_date < b.shipping_date ? 1 : -1;
	}
	var goodsBox;
	function loadDoc() {
		firebase.database().ref("json/goods_info").once("value").then(function(snapshot) {
			goodsBox = snapshot.val();
			printTable();
		});
	}
	
	function printTable() {
		var table_head = '<tr><td>시간</td><td>이름</td><td>연락처</td><td>도착희망일</td><td>주문내역</td><td>가격확정</td><td>상품가격</td><td>적립금</td><td>배송비</td><td>최종금액</td><td>결제방법</td><td>지불금액</td><td>card_no</td><td>결제</td></tr>';
		var table_body='';
		for(var i=ordersCount-1;i>=0;i--) {
			var table_button = '<a onClick="callPay(' + i + ')">결제</a>';
			if(todaysOrder[i].productPrice<=todaysOrder[i].PAID && todaysOrder[i].production) {
			   table_button = '완료';
			   }
			else{
				if(todaysOrder[i].production && todaysOrder[i].card_no!='X'){table_button = '<button onClick="callPay(' + i + ')">결제</button>';}
				else {table_button = '결제불가';}
			}
			table_body = table_body + '<tr>';
			table_body = table_body + '<td>' + todaysOrder[i].order_date + '<br>';
			table_body = table_body + '' + todaysOrder[i].customer_uid + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].customer_name + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].customer_email + '<br>';
			table_body = table_body + '' + todaysOrder[i].customer_phone + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].shipping_date + '</td>';
			table_body = table_body + '<td>' + '<button onClick="openDetail('+i+')" style="color:red">'+todaysOrder[i].productNumbers+'건 상세</button>' + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].production + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].productPrice + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].Point + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].Shipping + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].Pay_Price + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].Pay_Type + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].PAID + '</td>';
			table_body = table_body + '<td>' + todaysOrder[i].card_no + '</td>';
			table_body = table_body + '<td>' + table_button + '</td>';
			table_body = table_body + '</tr>';
		}
		document.getElementById('status_table').innerHTML = table_head+table_body;
	}
	function openDetail(index) {
		document.getElementById('detailed_popup').style.display = 'block';
		var detail_basic = '';
		var detail_order = '';
		var detail_table = '';
		detail_basic = detail_basic + '주문시간: ' + todaysOrder[index].order_date +'<br>';
		detail_basic = detail_basic + '고객키: ' + todaysOrder[index].customer_uid + '<br>';
		detail_basic = detail_basic + '고객명: ' + todaysOrder[index].customer_name + '<br>';
		detail_basic = detail_basic + '고객메일: ' + todaysOrder[index].customer_email +'<br>';
		detail_basic = detail_basic + '고객전화: ' + todaysOrder[index].customer_phone + '<br>';
		
		var total_Price = Number(todaysOrder[index].Point) + Number(todaysOrder[index].Pay_Price);
		detail_order = detail_order + '도착희망일: ' + todaysOrder[index].shipping_date + '<br>';
		detail_order = detail_order + '결제예상금액: ' + todaysOrder[index].Pay_Price + '  = ' + total_Price +'(상품총액) - ' + todaysOrder[index].Point + '(적립금)<br>';
		detail_order = detail_order + '현재까지 결제금액: ' + todaysOrder[index].Pay_Price+'<br>';
		detail_order = detail_order + '결제방법: '+ todaysOrder[index].Pay_Type+' (' + todaysOrder[index].card_no + ')<br>';
		if(todaysOrder[index].productPrice<=todaysOrder[index].PAID && todaysOrder[index].production) {
	   		detail_order = detail_order + '결제완료<br>';
	   	}else {
		   detail_order = detail_order + '결제미완<br>';
		}
		detail_order = detail_order + '주문수량: ' + todaysOrder[index].productNumbers+'<br>';
		
		var goodArray = todaysOrder[index].g_name.split(";");
		var optionArray = todaysOrder[index].g_option.split(";");
		var priceArray = todaysOrder[index].g_price.split(";");
		var indexArray = todaysOrder[index].g_index.split(";");
		var weightArray = todaysOrder[index].g_weight.split(";");

		detail_table = detail_table + '<tr><td>상품명</td><td>옵션</td><td>기준가격<br>(100g)</td><td>무게</td><td>라벨가격</td></tr>';
		for(var i=0;i<goodArray.length;i++) {
			detail_table = detail_table + '<tr>';
			detail_table = detail_table + '<td>' + getGoodsName(goodArray[i]) + '</td>';
			detail_table = detail_table + '<td>' + getGoodsOption(goodArray[i],optionArray[i]) + '</td>';
			detail_table = detail_table + '<td>' + indexArray[i] + '</td>';
			if(weightArray[i]=='-1') {
				detail_table = detail_table + '<td><input id="weight_'+i+'" value="" type="text" /></td>';
				detail_table = detail_table + '<td><input id="price_'+i+'" value="" type="text" /></td>';
				detail_table = detail_table + '<td><a onClick="applyWeight('+index+','+i+')">적용</a></td>';
			}else {
				detail_table = detail_table + '<td>' + weightArray[i] + '</td>';
				detail_table = detail_table + '<td>' + priceArray[i] + '</td>';
			}
			detail_table = detail_table + '</tr>';
		}
		/**
		detail_basic = detail_basic + '' + todaysOrder[i].productNumbers+'<br>';
		detail_basic = detail_basic + '' + todaysOrder[i].production + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].productPrice + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].Point + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].Shipping + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].Pay_Price + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].Pay_Type + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].PAID + '</td>';
		detail_basic = detail_basic + '' + todaysOrder[i].card_no + '</td>';**/
		document.getElementById('detail_basic').innerHTML = detail_basic;
		document.getElementById('detail_order').innerHTML = detail_order;
		document.getElementById('detail_table').innerHTML = detail_table;
	}
	function getGoodsName(goods) {
		var g_name = '';
		$.each(goodsBox.goods_info, function(key, value){
			if(key==goods) {
				existGoods = true;
				g_name = value.name;
				return false;
			}
		});
		return g_name;
	}
	function getGoodsOption(goods,opt) {
		opt = Number(opt) -1;
		var g_option = '';
		$.each(goodsBox.goods_info, function(key, value){
			if(key==goods) {
				existGoods = true;
				g_option = value.options[opt].name;
				return false;
			}
		});
		return g_option;
	}
	function CloseDetail() {
		document.getElementById('detailed_popup').style.display = 'none';
	}
	function CloseSum() {
		document.getElementById('detailed_sum').style.display = 'none';
	}
	function applyWeight(index,i) {
		
	}
	
	function callPay(i) {
		alert(todaysOrder[i].customer_name + '\n' + todaysOrder[i].customer_email + '\n' + todaysOrder[i].customer_phone + '\n' + todaysOrder[i].customer_address + '\n' + todaysOrder[i].card_no + '\n' + todaysOrder[i].Pay_Price + '\n');
		f_try_pay(todaysOrder[i].customer_name,todaysOrder[i].customer_email,todaysOrder[i].customer_phone,todaysOrder[i].customer_address,todaysOrder[i].card_no,'정육각',todaysOrder[i].Pay_Price);
	}
	function viewSum() {
		var detail_sum='';
		var sum_title = new Array();
		var sumArray = new Array();
		detail_sum = detail_sum + '<tr><td>구분</td>';
		var sCount = 0;
		$.each(goodsBox.goods_info, function(key, value){
			if(key=='porkbelly-fresh' || key=='porkneck-fresh') {
				for(var i=0;i<value.optioncount;i++) {
					detail_sum = detail_sum + '<td>:::' + value.name+value.options[i].name + ':::</td>';
					sum_title[sCount] = key+i;
					sCount++;
				}
			}
		});
		detail_sum = detail_sum + '</tr>';
		for(var i=ordersCount-1;i>=0;i--) {
			if(i==ordersCount-1) {
				for(var t=0;t<sCount;t++) {sumArray[t]=0;}
				detail_sum = detail_sum + '<tr>';
				detail_sum = detail_sum + '<td>'+todaysOrder[i].shipping_date+'</td>';
			}else {
				if(todaysOrder[i+1].shipping_date!=todaysOrder[i].shipping_date) {
					for(var t=0;t<sCount;t++) {
						detail_sum = detail_sum + '<td>' + sumArray[t] + '</td>';
						sumArray[t]=0;
					}
					detail_sum = detail_sum + '</tr>';
					detail_sum = detail_sum + '<tr>';
					detail_sum = detail_sum + '<td>'+todaysOrder[i].shipping_date+'</td>';
				}
			}
			
			var goodArray = todaysOrder[i].g_name.split(";");
			var optionArray = todaysOrder[i].g_option.split(";");
			var priceArray = todaysOrder[i].g_price.split(";");
			var indexArray = todaysOrder[i].g_index.split(";");
			var weightArray = todaysOrder[i].g_weight.split(";");
			for(var j=0;j<goodArray.length;j++) {
				for(var k=0;k<sCount;k++) {
					var ee = Number(optionArray[j])-1;
					var compareT = goodArray[j]+ee;
					if(compareT==sum_title[k]) {
						sumArray[k]++;
						break;
					}
				}
			}
		}
		for(var t=0;t<sCount;t++) {
			detail_sum = detail_sum + '<td>' + sumArray[t] + '</td>';
			sumArray[t]=0;
		}
		detail_sum = detail_sum + '</tr>';
		document.getElementById('detailed_sum').style.display = 'block';
		document.getElementById('detail_sum').innerHTML = detail_sum;
	}
	
	
    /* 입력 자동 Setting */
    function f_try_pay(u_name,u_email,u_phone,u_address,u_code,u_product,u_price){
        var frm_pay = document.frm_pay;
        
        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth() + 1;
        var date  = today.getDate();
        var time  = today.getTime();
        
        if(parseInt(month) < 10) {
            month = "0" + month;
        }

        if(parseInt(date) < 10) {
            date = "0" + date;
        }
        
        frm_pay.EP_mall_id.value = "05530874";
        frm_pay.EP_mall_nm.value = "(주) 정육각";
        frm_pay.EP_order_no.value = "ORDER_" + year + month + date + time;   //가맹점주문번호
        frm_pay.EP_user_id.value = "USER_" + time;                           //고객ID
        frm_pay.EP_user_nm.value = String(u_name);
		frm_pay.EP_card_no.value = String(u_code);
        frm_pay.EP_user_mail.value = String(u_email);
        frm_pay.EP_user_phone1.value = String(u_phone);
        frm_pay.EP_user_phone2.value = "";
        frm_pay.EP_user_addr.value = String(u_address);;
        frm_pay.EP_product_nm.value = String(u_product);
        frm_pay.EP_product_amt.value = String(u_price);
		f_submit();
    }
    function f_submit() {
        var frm_pay = document.frm_pay;
        var bRetVal = false;
        frm_pay.EP_tot_amt.value = frm_pay.EP_card_amt.value = frm_pay.EP_product_amt.value;
        
        if( frm_pay.EP_card_amt.value < 50000 ) 
        {
            frm_pay.EP_install_period.value = "00";
            frm_pay.EP_noint.value = "00";
        }
        
        bRetVal = true;
        if ( bRetVal ) frm_pay.submit();
    }
</script>
</head>
<body>
<form name="ordered">
        <input type="hidden" name="batchPrice" id="batchPrice" value="<?php echo $_POST['batchPrice']; ?>"/>
        <input type="hidden" name="batchKey" id="batchKey" value="<?php echo $_POST['batchKey']; ?>"/>
        <input type="hidden" name="batchNumber" id="batchNumber" value="<?php echo $_POST['batchNumber']; ?>"/>
    </form>
<form name="frm_pay" method="post" action="../easypay_request.php" target="_blank"> <!--/easypay_request_payment.php -->
<!-- http://104.198.5.183/payment/easypay_web/web/easypay_request.php -->
	


<!--------------------------->
<!-- ::: 공통 인증 요청 값 -->
<!--------------------------->

<input type="hidden" id="EP_mall_nm"        name="EP_mall_nm"           value="">         <!-- 가맹점명-->
<input type="hidden" id="EP_currency"       name="EP_currency"          value="00">       <!-- 통화코드 // 00 : 원화-->
<input type="hidden" id="EP_return_url"     name="EP_return_url"        value="">         <!-- 가맹점 CALLBACK URL // -->
<input type="hidden" id="EP_ci_url"         name="EP_ci_url"            value="">         <!-- CI LOGO URL // -->
<input type="hidden" id="EP_lang_flag"      name="EP_lang_flag"         value="">         <!-- 언어 // -->
<input type="hidden" id="EP_charset"        name="EP_charset"           value="EUC-KR">   <!-- 가맹점 CharSet // -->
<input type="hidden" id="EP_user_type"      name="EP_user_type"         value="">         <!-- 사용자구분 // -->
<input type="hidden" id="EP_user_id"        name="EP_user_id"           value="">         <!-- 가맹점 고객ID // -->
<input type="hidden" id="EP_memb_user_no"   name="EP_memb_user_no"      value="">         <!-- 가맹점 고객일련번호 // -->
<input type="hidden" id="EP_user_nm"        name="EP_user_nm"           value="">         <!-- 가맹점 고객명 // -->
<input type="hidden" id="EP_user_mail"      name="EP_user_mail"         value="">         <!-- 가맹점 고객 E-mail // -->
<input type="hidden" id="EP_user_phone1"    name="EP_user_phone1"       value="">         <!-- 가맹점 고객 연락처1 // -->
<input type="hidden" id="EP_user_phone2"    name="EP_user_phone2"       value="">         <!-- 가맹점 고객 연락처2 // -->
<input type="hidden" id="EP_user_addr"      name="EP_user_addr"         value="">         <!-- 가맹점 고객 주소 // -->
<input type="hidden" id="EP_user_define1"   name="EP_user_define1"      value="">         <!-- 가맹점 필드1 // -->
<input type="hidden" id="EP_user_define2"   name="EP_user_define2"      value="">         <!-- 가맹점 필드2 // -->
<input type="hidden" id="EP_user_define3"   name="EP_user_define3"      value="">         <!-- 가맹점 필드3 // -->
<input type="hidden" id="EP_user_define4"   name="EP_user_define4"      value="">         <!-- 가맹점 필드4 // -->
<input type="hidden" id="EP_user_define5"   name="EP_user_define5"      value="">         <!-- 가맹점 필드5 // -->
<input type="hidden" id="EP_user_define6"   name="EP_user_define6"      value="">         <!-- 가맹점 필드6 // -->
<input type="hidden" id="EP_product_type"   name="EP_product_type"      value="">         <!-- 상품정보구분 // -->
<input type="hidden" id="EP_product_expr"   name="EP_product_expr"      value="">         <!-- 서비스 기간 // (YYYYMMDD) -->

<!--------------------------->
<!-- ::: 결제 인증 요청 값 -->
<!--------------------------->

<input type="hidden" id="EP_tr_cd"          name="EP_tr_cd"             value="00101000"> <!-- 거래구분(수정불가) -->
<input type="hidden" id="EP_pay_type"       name="EP_pay_type"          value="batch">    <!-- 결제수단(수정불가) -->
<input type="hidden" id="EP_tot_amt"        name="EP_tot_amt"           value="<?php echo $_POST['batchPrice']; ?>">         <!-- 결제총금액 -->
<input type="hidden" id="EP_currency"       name="EP_currency"          value="00">       <!-- 통화코드 : 00(원), 01(달러)-->
<input type="hidden" id="EP_card_txtype"    name="EP_card_txtype"       value="41">       <!-- 신용카드 처리구분(수정불가) -->
<input type="hidden" id="EP_req_type"       name="EP_req_type"          value="0">        <!-- 신용카드 결제종류(수정불가) -->
<input type="hidden" id="EP_card_amt"       name="EP_card_amt"          value="">         <!-- 신용카드 결제금액 -->
<input type="hidden" id="EP_wcc"            name="EP_wcc"               value="@">        <!-- 신용카드 WCC(수정불가) -->
<input type="hidden" id="EP_noint"          name="EP_noint"             value="00">       <!-- 무이자 -->

<input type="hidden" id="EP_mall_id" name="EP_mall_id" value="T5102001" size="50" maxlength="8" class="input_F">

<div style="display: none">
<select name="EP_install_period" class="input_F">
	<option value="00" selected>일시불</option>
	<option value="02">2개월</option>
	<option value="03">3개월</option>
	<option value="04">4개월</option>
	<option value="05">5개월</option>
	<option value="06">6개월</option>
	<option value="07">7개월</option>
	<option value="08">8개월</option>
	<option value="09">9개월</option>
	<option value="10">10개월</option>
	<option value="11">11개월</option>
	<option value="12">12개월</option>            
</select>
</div>

<input type="hidden" name="EP_card_no" value="<?php echo $_POST['batchKey']; ?>" size="30" maxlength="20" class="input_F">
<input type="hidden" name="EP_order_no" size="50" class="input_F" value="<?php echo $_POST['batchNumber']; ?>">
<input type="hidden" name="EP_product_nm" size="50" class="input_A">
<input type="hidden" name="EP_product_amt" size="50" class="input_A">

</form>

<div id="v_signIn">
<input id="v_userId" value="" type="text" />
<input id="v_userPw" value="" type="password" />
	<button onClick="init()">관리자접속</button>
	</div>
	<button onClick="signOut()">로그아웃</button>
	<p id="current_time_real"></p>
	<button onClick="viewSum()">주문 요약</button>
	<table id="status_table" style="font-size: 9px">
	</table>

<div style="position: fixed;width: 80%;height: 80%;left:10%;top:10%;background-color: darkgray;display: none;" id="detailed_popup">
	<div style="position: relative; text-align: center; background-color: darkcyan" onClick="CloseDetail()">닫기</div>
	<p style="padding-left: 15px">고객 정보</p>
	<p id="detail_basic" style="padding-left: 15px"></p>
	<p style="padding-left: 15px">주문상세정보</p>
	<p id="detail_order" style="padding-left: 15px"></p>
	
	<table id="detail_table">
	</table>
</div>
<div style="position: fixed;width: 80%;height: 80%;left:10%;top:10%;background-color: darkgray;display: none;" id="detailed_sum">
<div style="position: relative; text-align: center; background-color: darkcyan" onClick="CloseSum()">닫기</div>
	<p style="padding-left: 15px">날짜별 주문내역</p>
	<table id="detail_sum">
	</table>
</div>

</body>
</html>