<?php

   class EasyPay_Client

{

 

 	var $deli_fs;

	var $deli_gs;

	var $deli_rs;

	var $deli_us;

	

	var $_req_data_cnt;

	

	var $gw_url;

	var $gw_port;

	var $home_dir;

	var $log_dir;

	var $log_level;

	var $cert_file;



	var $enc_data;

	var $snd_key;

	var $trace_no;

	

	var $_easypay_txreq;

	

	var $_easypay_reqdata;

	var $_easypay_resdata;

	

	function EasyPay_Client() {

        

            $this->deli_fs = chr(28);

            $this->deli_gs = chr(29);

            $this->deli_rs = chr(30);

            $this->deli_us = chr(31);



            $this->gw_url    = "";

            $this->gw_port   = "";

            $this->home_dir  = "";

            $this->log_dir   = "";

            $this->log_level = "";

            $this->cert_file = "";

            $this->enc_data  = "";

            $this->snd_key   = "";

        

            $this->_req_data_cnt  = 0;

            $this->_easypay_txreq = "";

            $this->_easypay_reqdata = Array();

            $this->_easypay_resdata = Array();



	}



        function  clearup_msg()

        {

            $this->enc_data = "";

            $this->snd_key  = "";

            $this->trace_no = "";



            $this->_easypay_txreq = "";



            $this->_easypay_reqdata = "";

            $this->_easypay_resdata = "";

        }

	

	function set_gw_url($gw_url) {

	    $this->gw_url = $gw_url;

	}



        function set_gw_port($gw_port) {

	    $this->gw_port = $gw_port;

	}



	function set_home_dir($home_dir) {

	    $this->home_dir = $home_dir;

	}

	

	function set_log_dir($log_dir) {

	    $this->log_dir = $log_dir;

	}



        function set_log_level($log_level) {

	    $this->log_level = $log_level;

	}

	

	function set_cert_file($cert_file) {

	    $this->cert_file = $cert_file;

	}



        function set_enc_data($enc_data) {

	    $this->enc_data = $enc_data;

	}

	

	function set_snd_key($snd_key) {

	    $this->snd_key = $snd_key;

	}

	

	function set_trace_no($trace_no) {

	    $this->trace_no = $trace_no;

	}

	

	function set_easypay_deli_fs($value) {

        if ($value != "" && strlen($value)) {

    		$this->_easypay_txreq .= $value . $this->deli_fs;

    	}

	}

	

	function set_easypay_deli_us($no, $key, $value) {		

        if ($value != "" && strlen($value) != 0) {

    		$this->_easypay_reqdata[$no][1] .= $key ."=" . $value . 

    		                                   $this->deli_us;

    	}

	}

	

	function set_easypay_deli_rs($idx1, $idx2) {

		$this->_easypay_reqdata[$idx1][1] .= $this->_easypay_reqdata[$idx2][0] . 

		                                     "=" . 

		                                     $this->_easypay_reqdata[$idx2][1] . 

		                                     $this->deli_rs;

	}



	function set_easypay_deli_gs($no, $key, $value) {

		if ($value != "" && strlen($value) != 0) {

			$this->_easypay_reqdata[$no][1] .= $key ."=" . $value . $this->deli_gs;

		}

	}

	

	function set_easypay_item($data_name) {

		$i;

		for ($i = 0; $i < $this->_req_data_cnt; $i++)

			if ($this->_easypay_reqdata[$i][0] == $data_name)

				break;



		if ($i == $this->_req_data_cnt) {

			$this->_easypay_reqdata[$i][0] = $data_name;

			$this->_req_data_cnt++;

		}

		

		return $i;

	}



	function easypay_exec($mall_id, $tr_cd, $order_no, $cust_ip, $opt) {

		

	    $this->set_tx_req_data($opt);



        $res_data = $this->mf_exec( $this->home_dir . "/bin/linux_64/ep_cli",

                                    "-h",

                                    "order_no="  . $order_no          . "," .

                                    "cert_file=" . $this->cert_file   . "," .

                                    "mall_id="   . $mall_id           . "," .

                                    "tr_cd="     . $tr_cd             . "," .

                                    "gw_url="    . $this->gw_url      . "," .

                                    "gw_port="   . $this->gw_port     . "," .

                                    "plan_data=" . $this->_easypay_txreq . "," .

                                    "enc_data="  . $this->enc_data    . "," .

                                    "snd_key="   . $this->snd_key     . "," .

                                    "trace_no="  . $this->trace_no    . "," .

                                    "cust_ip="   . $cust_ip           . "," .

                                    "log_dir="   . $this->log_dir     . "," .

                                    "log_level=" . $this->log_level   . "," .

                                    "opt="       . $opt               . "" );




        if ( $res_data == "" )

        {

            $res_data = "res_cd=M114" . $this->deli_us . "res_msg=연동 모듈 실행 오류";
			/**
			$res_data = $this->home_dir . "/bin/linux_64/ep_cli".

                                    "-h".

                                    "order_no="  . $order_no          . "," .

                                    "cert_file=" . $this->cert_file   . "," .

                                    "mall_id="   . $mall_id           . "," .

                                    "tr_cd="     . $tr_cd             . "," .

                                    "gw_url="    . $this->gw_url      . "," .

                                    "gw_port="   . $this->gw_port     . "," .

                                    "plan_data=" . $this->_easypay_txreq . "," .

                                    "enc_data="  . $this->enc_data    . "," .

                                    "snd_key="   . $this->snd_key     . "," .

                                    "trace_no="  . $this->trace_no    . "," .

                                    "cust_ip="   . $cust_ip           . "," .

                                    "log_dir="   . $this->log_dir     . "," .

                                    "log_level=" . $this->log_level   . "," .

                                    "opt="       . $opt               . "";**/

        }

        

        parse_str(str_replace($this->deli_us, "&", $res_data), $this->_easypay_resdata);

	}

	

	function get_easypay_item($data_name) {

	    

		$i;

		$result;

		

		for ($i = 0; $i < $this->_req_data_cnt; $i++) {

			if ($this->_easypay_reqdata[$i][0] == $data_name) {

			    $result = $data_name . "=" . $this->_easypay_reqdata[$i][1];

			    break;

			}

		}

		

		return $result;

	}

	

	function set_tx_req_data($opt) {

		

		if($opt == "utf-8") {

			$pay_data = iconv( "utf-8", "euc-kr", $this->get_easypay_item("pay_data"));

	        $order_data = iconv( "utf-8", "euc-kr", $this->get_easypay_item("order_data"));

	        $escrow_data = iconv( "utf-8", "euc-kr", $this->get_easypay_item("escrow_data"));

	        $mgr_data = iconv( "utf-8", "euc-kr", $this->get_easypay_item("mgr_data"));

	        $cash_data = iconv( "utf-8", "euc-kr", $this->get_easypay_item("cash_data"));

	    }

	    else {

	        $pay_data = $this->get_easypay_item("pay_data");

	        $order_data = $this->get_easypay_item("order_data");

	        $escrow_data = $this->get_easypay_item("escrow_data");

	        $mgr_data = $this->get_easypay_item("mgr_data");

	        $cash_data = $this->get_easypay_item("cash_data");

	    }

	    

	    if($pay_data != "") {

	        $this->set_easypay_deli_fs($pay_data) ;

	    }

	    if($order_data != "") {

	        $this->set_easypay_deli_fs($order_data) ;

	    }

	    if($escrow_data != "") {

	        $this->set_easypay_deli_fs($escrow_data) ;

	    }

	    if($mgr_data != "") {

	        $this->set_easypay_deli_fs($mgr_data) ;

	    }

	    if($cash_data != "") {

	        $this->set_easypay_deli_fs($cash_data) ;

	    }

	    

	}

	

	function get_easypay_txreq() {

	    return $this->_easypay_txreq;

	}



    function mf_exec() {
        $arg = func_get_args();
        if(is_array($arg[0])) {

            $arg = $arg[0];

        }

		while(list(,$i) = each($arg))
        {
            $exec_cmd .= " " . escapeshellarg( $i );
        }
        $res_data = exec( $exec_cmd );
        return  $res_data;
    }
	
	function mf_execc() {
        $arg = func_get_args();
        if(is_array($arg[0])) {

            $arg = $arg[0];

        }

        while(list(,$i) = each($arg))
        {
            $exec_cmd .= " " . escapeshellarg( $i );
        }
        return  $exec_cmd;
    }

    

    function get_remote_addr()

    {

        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {

            $client_ip = $_SERVER['HTTP_CLIENT_IP'];

        }

        else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

        }

        else {

            $client_ip = $_SERVER['REMOTE_ADDR'];

        }

        

        return $client_ip;

    }

}

    /* -------------------------------------------------------------------------- */

    /* ::: 처리구분 설정                                                          */

    /* -------------------------------------------------------------------------- */

    $TRAN_CD_NOR_PAYMENT    = "00101000";   // 승인(일반, 에스크로)

    $TRAN_CD_NOR_MGR        = "00201000";   // 변경(일반, 에스크로)



    /* -------------------------------------------------------------------------- */

    /* ::: 쇼핑몰 지불 정보 설정                                                  */

    /* -------------------------------------------------------------------------- */

    //$g_gw_url    = "testgw.easypay.co.kr";               // Gateway URL ( test )

    $g_gw_url               = "gw.easypay.co.kr";      // Gateway URL ( real )

    $g_gw_port   = "80";                                           // 포트번호(변경불가)



    /* -------------------------------------------------------------------------- */

    /* ::: 지불 데이터 셋업 (업체에 맞게 수정)                                    */

    /* -------------------------------------------------------------------------- */

    /* ※ 주의 ※                                                                 */

    /* cert_file 변수 설정                                                        */

    /* - pg_cert.pem 파일이 있는 디렉토리의  절대 경로 설정                       */

    /* log_dir 변수 설정                                                          */

    /* - log 디렉토리 설정                                                        */

    /* log_level 변수 설정                                                        */

    /* - log 레벨 설정                                                            */

    /* -------------------------------------------------------------------------- */

    $g_home_dir   = "/opt/bitnami/apps/wordpress/htdocs/payment/easypay_web";
    $g_cert_file  = "/opt/bitnami/apps/wordpress/htdocs/payment/easypay_web/cert/pg_cert.pem";
    $g_log_dir    = "/opt/bitnami/apps/wordpress/htdocs/payment/easypay_web/log";

    $g_log_level  = "1";



    $g_mall_id   = $_POST["EP_mall_id"];              // [필수]몰아이디



    /* -------------------------------------------------------------------------- */

    /* ::: 웹페이 응답정보 설정                                                 */

    /* -------------------------------------------------------------------------- */

    $tr_cd            = $_POST["EP_tr_cd"];             // [필수]요청구분

    $trace_no         = $_POST["EP_trace_no"];          // [필수]추적고유번호

    $sessionkey       = $_POST["EP_sessionkey"];        // [필수]암호화키

    $encrypt_data     = $_POST["EP_encrypt_data"];      // [필수]암호화 데이타



    /* -------------------------------------------------------------------------- */

    /* ::: 배치결제 정보 설정                                                     */

    /* -------------------------------------------------------------------------- */

    $pay_type         = $_POST["EP_pay_type"];          // [필수]결제수단

    $tot_amt          = $_POST["EP_tot_amt"];           // [필수]총결제금액

    $curr_code        = $_POST["EP_currency"];          // [필수]통화코드

    $complex_yn       = "N";                                                        // [필수]복합결제유무(배치결제는 사용 안함)

    $escrow_yn        = "N";                                                        // [필수]에스크로 여부(배치결제는 사용 안함)



    $card_txtype      = $_POST["EP_card_txtype"];       // [필수]처리구분

    $req_type         = $_POST["EP_req_type"];          // [필수]결제종류

    $card_amt         = $_POST["EP_card_amt"];          // [필수]결제금액

    $wcc              = $_POST["EP_wcc"];               // [필수]WCC

    $card_no          = $_POST["EP_card_no"];           // [필수]배치키

    $install_period   = $_POST["EP_install_period"];    // [필수]할부개월

    $noint            = $_POST["EP_noint"];             // [필수]무이자여부



    /* -------------------------------------------------------------------------- */

    /* ::: 결제 주문 정보 설정                                                    */

    /* -------------------------------------------------------------------------- */

    $user_type        = $_POST["EP_user_type"];        // [선택]사용자구분구분[1:일반,2:회원]

    $order_no         = $_POST["EP_order_no"];         // [필수]주문번호

    $memb_user_no     = $_POST["EP_memb_user_no"];     // [선택]가맹점 고객일련번호

    $user_id          = $_POST["EP_user_id"];          // [선택]고객 ID

    $user_nm          = $_POST["EP_user_name"];        // [필수]고객명

    $user_mail        = $_POST["EP_user_mail"];        // [필수]고객 E-mail

    $user_phone1      = $_POST["EP_user_phone1"];      // [필수]가맹점 고객 연락처1

    $user_phone2      = $_POST["EP_user_phone2"];      // [선택]가맹점 고객 연락처2

    $user_addr        = $_POST["EP_user_addr"];        // [선택]가맹점 고객 주소

    $product_type     = $_POST["EP_product_type"];     // [필수]상품정보구분[0:실물,1:컨텐츠]

    $product_nm       = $_POST["EP_product_nm"];       // [필수]상품명

    $product_amt      = $_POST["EP_product_amt"];      // [필수]상품금액



    /* -------------------------------------------------------------------------- */

    /* ::: 변경관리 정보 설정                                                     */

    /* -------------------------------------------------------------------------- */

    $mgr_txtype       = $_POST["mgr_txtype"];       // [필수]거래구분

    $mgr_subtype      = $_POST["mgr_subtype"];      // [선택]변경세부구분

    $org_cno          = $_POST["org_cno"];          // [필수]원거래고유번호

    $mgr_amt          = $_POST["mgr_amt"];          // [선택]부분취소/환불요청 금액

    $mgr_rem_amt      = $_POST["mgr_rem_amt"];      // [선택]부분취소 잔액

    $mgr_bank_cd      = $_POST["mgr_bank_cd"];      // [선택]환불계좌 은행코드

    $mgr_account      = $_POST["mgr_account"];      // [선택]환불계좌 번호

    $mgr_depositor    = $_POST["mgr_depositor"];    // [선택]환불계좌 예금주명

    $mgr_socno        = $_POST["mgr_socno"];        // [선택]환불계좌 주민번호

    $mgr_telno        = $_POST["mgr_telno"];        // [선택]환불고객 연락처

    $deli_cd          = $_POST["deli_cd"];          // [선택]배송구분[자가:DE01,택배:DE02]

    $deli_corp_cd     = $_POST["deli_corp_cd"];     // [선택]택배사코드

    $deli_invoice     = $_POST["deli_invoice"];     // [선택]운송장 번호

    $deli_rcv_nm      = $_POST["deli_rcv_nm"];      // [선택]수령인 이름

    $deli_rcv_tel     = $_POST["deli_rcv_tel"];     // [선택]수령인 연락처

    //$req_ip           = $_POST["req_ip"];           // [필수]요청자 IP

    $req_id           = $_POST["req_id"];           // [선택]dycjdwk

    $mgr_msg          = $_POST["mgr_msg"];          // [선택]변경 사유



    /* -------------------------------------------------------------------------- */

    /* ::: IP 정보 설정                                                           */

    /* -------------------------------------------------------------------------- */

    $client_ip        = $_SERVER['REMOTE_ADDR'];         // [필수]결제고객 IP



    /* -------------------------------------------------------------------------- */

    /* ::: 전문                                                                   */

    /* -------------------------------------------------------------------------- */

    $mgr_data    = "";     // 변경정보

    $tx_req_data = "";     // 요청전문



    /* -------------------------------------------------------------------------- */

    /* ::: 결제 결과                                                              */

    /* -------------------------------------------------------------------------- */

    $bDBProc          = "";

    $res_cd           = "";

    $res_msg          = "";

    $r_order_no       = "";

    $r_complex_yn     = "";

    $r_msg_type       = "";     //거래구분

    $r_noti_type        = "";      //노티구분

    $r_cno            = "";     //PG거래번호

    $r_amount         = "";     //총 결제금액

    $r_auth_no        = "";     //승인번호

    $r_tran_date      = "";     //거래일시

    $r_pnt_auth_no    = "";     //포인트 승인 번호

    $r_pnt_tran_date  = "";     //포인트 승인 일시

    $r_cpon_auth_no   = "";     //쿠폰 승인 번호

    $r_cpon_tran_date = "";     //쿠폰 승인 일시

    $r_card_no        = "";     //카드번호

    $r_issuer_cd      = "";     //발급사코드

    $r_issuer_nm      = "";     //발급사명

    $r_acquirer_cd    = "";     //매입사코드

    $r_acquirer_nm    = "";     //매입사명

    $r_install_period = "";     //할부개월

    $r_noint          = "";     //무이자여부

    $r_bank_cd        = "";      //은행코드

    $r_bank_nm        = "";     //은행명

    $r_account_no     = "";     //계좌번호

    $r_deposit_nm     = "";     //입금자명

    $r_expire_date    = "";      //계좌사용 만료일

    $r_cash_res_cd    = "";     //현금영수증 결과코드

    $r_cash_res_msg   = "";     //현금영수증 결과메세지

    $r_cash_auth_no   = "";     //현금영수증 승인번호

    $r_cash_tran_date = "";     //현금영수증 승인일시

    $r_auth_id        = "";     //PhoneID

    $r_billid         = "";     //인증번호

    $r_mobile_no      = "";     //휴대폰번호

    $r_ars_no         = "";     //ARS 전화번호

    $r_cp_cd          = "";     //포인트사

    $r_used_pnt       = "";     //사용포인트

    $r_remain_pnt     = "";     //잔여한도

    $r_pay_pnt        = "";     //할인/발생포인트

    $r_accrue_pnt     = "";     //누적포인트

    $r_remain_cpon    = "";     //쿠폰잔액

    $r_used_cpon      = "";     //쿠폰 사용금액

    $r_mall_nm        = "";     //제휴사명칭

    $r_escrow_yn        = "";     //에스크로 사용유무

    $r_canc_acq_date  = "";     //매입취소일시

    $r_canc_date      = "";     //취소일시

    $r_refund_date    = "";     //환불예정일시



    /* -------------------------------------------------------------------------- */

    /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */

    /* -------------------------------------------------------------------------- */

    $easyPay = new EasyPay_Client;         // 전문처리용 Class (library에서 정의됨)

    $easyPay->clearup_msg();



    $easyPay->set_home_dir($g_home_dir);

    $easyPay->set_gw_url($g_gw_url);

    $easyPay->set_gw_port($g_gw_port);

    $easyPay->set_log_dir($g_log_dir);

    $easyPay->set_log_level($g_log_level);

    $easyPay->set_cert_file($g_cert_file);



    /* -------------------------------------------------------------------------- */

    /* ::: 승인요청(플러그인 암호화 전문 설정)                                    */

    /* -------------------------------------------------------------------------- */

    if( $TRAN_CD_NOR_PAYMENT == $tr_cd ) {



        /* ---------------------------------------------------------------------- */

        /* ::: 인증요청                                                           */

        /* ---------------------------------------------------------------------- */

        if( $pay_type == "81" ) {

            $easyPay->set_trace_no($trace_no);

            $easyPay->set_snd_key($sessionkey);

            $easyPay->set_enc_data($encrypt_data);

        }

        else {

        /* ---------------------------------------------------------------------- */

        /* ::: 승인요청                                                           */

        /* ---------------------------------------------------------------------- */

            // 결제 주문 정보 DATA

            $order_data = $easyPay->set_easypay_item("order_data");

            $easyPay->set_easypay_deli_us( $order_data, "user_type"     , $user_type     );

            $easyPay->set_easypay_deli_us( $order_data, "order_no"      , $order_no      );

            $easyPay->set_easypay_deli_us( $order_data, "memb_user_no"  , $memb_user_no  );

            $easyPay->set_easypay_deli_us( $order_data, "user_id"       , $user_id       );

            $easyPay->set_easypay_deli_us( $order_data, "user_nm"       , $user_nm       );

            $easyPay->set_easypay_deli_us( $order_data, "user_mail"     , $user_mail     );

            $easyPay->set_easypay_deli_us( $order_data, "user_phone1"   , $user_phone1   );

            $easyPay->set_easypay_deli_us( $order_data, "user_phone2"   , $user_phone2   );

            $easyPay->set_easypay_deli_us( $order_data, "user_addr"     , $user_addr     );

            $easyPay->set_easypay_deli_us( $order_data, "product_type"  , $product_type  );

            $easyPay->set_easypay_deli_us( $order_data, "product_nm"    , $product_nm    );

            $easyPay->set_easypay_deli_us( $order_data, "product_amt"   , $product_amt   );



            // 결제정보 DATA부

            $pay_data = $easyPay->set_easypay_item("pay_data");



            // 결제 공통 정보 DATA

            $common_data = $easyPay->set_easypay_item("common");



            $easyPay->set_easypay_deli_us( $common_data, "tot_amt"       , $tot_amt      );

            $easyPay->set_easypay_deli_us( $common_data, "currency"      , $curr_code    );

            $easyPay->set_easypay_deli_us( $common_data, "client_ip"     , $client_ip    );

            $easyPay->set_easypay_deli_us( $common_data, "cli_ver"       , "W8"          );

            $easyPay->set_easypay_deli_us( $common_data, "escrow_yn"     , $escrow_yn    );

            $easyPay->set_easypay_deli_us( $common_data, "complex_yn"    , $complex_yn   );

            $easyPay->set_easypay_deli_rs( $pay_data, $common_data);



            $card_data = $easyPay->set_easypay_item("card");



            $easyPay->set_easypay_deli_us( $card_data, "card_txtype"    , $card_txtype       );

            $easyPay->set_easypay_deli_us( $card_data, "req_type"       , $req_type          );

            $easyPay->set_easypay_deli_us( $card_data, "card_amt"       , $card_amt          );

            $easyPay->set_easypay_deli_us( $card_data, "card_no"        , $card_no           );

            $easyPay->set_easypay_deli_us( $card_data, "noint"          , $noint             );

            $easyPay->set_easypay_deli_us( $card_data, "wcc"            , $wcc               );

            $easyPay->set_easypay_deli_us( $card_data, "install_period" , $install_period    );



            $easyPay->set_easypay_deli_rs( $pay_data, $card_data );

        }



    /* -------------------------------------------------------------------------- */

    /* ::: 변경관리 요청                                                          */

    /* -------------------------------------------------------------------------- */

    }else if( $TRAN_CD_NOR_MGR == $tr_cd ) {



        $mgr_data = $easyPay->set_easypay_item("mgr_data");

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_txtype"      , $mgr_txtype       );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_subtype"     , $mgr_subtype      );

        $easyPay->set_easypay_deli_us( $mgr_data, "org_cno"         , $org_cno          );

        $easyPay->set_easypay_deli_us( $mgr_data, "order_no"        , $order_no         );

        $easyPay->set_easypay_deli_us( $mgr_data, "pay_type"        , $pay_type         );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_amt"         , $mgr_amt          );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_rem_amt"     , $mgr_rem_amt      );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_bank_cd"     , $mgr_bank_cd      );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_account"     , $mgr_account      );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_depositor"   , $mgr_depositor    );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_socno"       , $mgr_socno        );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_telno"       , $mgr_telno        );

        $easyPay->set_easypay_deli_us( $mgr_data, "deli_cd"         , $deli_cd          );

        $easyPay->set_easypay_deli_us( $mgr_data, "deli_corp_cd"    , $deli_corp_cd     );

        $easyPay->set_easypay_deli_us( $mgr_data, "deli_invoice"    , $deli_invoice     );

        $easyPay->set_easypay_deli_us( $mgr_data, "deli_rcv_nm"     , $deli_rcv_nm      );

        $easyPay->set_easypay_deli_us( $mgr_data, "deli_rcv_tel"    , $deli_rcv_tel     );

        $easyPay->set_easypay_deli_us( $mgr_data, "req_ip"          , $client_ip        );

        $easyPay->set_easypay_deli_us( $mgr_data, "req_id"          , $req_id           );

        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_msg"         , $mgr_msg          );

    }





    /* -------------------------------------------------------------------------- */

    /* ::: 실행                                                                   */

    /* -------------------------------------------------------------------------- */

    $opt = "option value";

    $easyPay->easypay_exec($g_mall_id, $tr_cd, $order_no, $client_ip, $opt);

    $res_cd  = $easyPay->_easypay_resdata["res_cd"];    // 응답코드

    $res_msg = $easyPay->_easypay_resdata["res_msg"];   // 응답메시지

    

    /* -------------------------------------------------------------------------- */

    /* ::: 결과 처리                                                              */

    /* -------------------------------------------------------------------------- */

    $r_cno             = $easyPay->_easypay_resdata[ "cno"             ];    // PG거래번호

    $r_amount          = $easyPay->_easypay_resdata[ "amount"          ];    //총 결제금액

    $r_order_no        = $easyPay->_easypay_resdata[ "order_no"        ];    //주문번호

    $r_noti_type       = $easyPay->_easypay_resdata[ "noti_type"       ];    //노티구분

    $r_auth_no         = $easyPay->_easypay_resdata[ "auth_no"         ];    //승인번호

    $r_tran_date       = $easyPay->_easypay_resdata[ "tran_date"       ];    //승인일시

    $r_pnt_auth_no     = $easyPay->_easypay_resdata[ "pnt_auth_no"     ];    //포인트승인번호

    $r_pnt_tran_date   = $easyPay->_easypay_resdata[ "pnt_tran_date"   ];    //포인트승인일시

    $r_cpon_auth_no    = $easyPay->_easypay_resdata[ "cpon_auth_no"    ];    //쿠폰승인번호

    $r_cpon_tran_date  = $easyPay->_easypay_resdata[ "cpon_tran_date"  ];    //쿠폰승인일시

    $r_card_no         = $easyPay->_easypay_resdata[ "card_no"         ];    //카드번호

    $r_issuer_cd       = $easyPay->_easypay_resdata[ "issuer_cd"       ];    //발급사코드

    $r_issuer_nm       = $easyPay->_easypay_resdata[ "issuer_nm"       ];    //발급사명

    $r_acquirer_cd     = $easyPay->_easypay_resdata[ "acquirer_cd"     ];    //매입사코드

    $r_acquirer_nm     = $easyPay->_easypay_resdata[ "acquirer_nm"     ];    //매입사명

    $r_install_period  = $easyPay->_easypay_resdata[ "install_period"  ];    //할부개월

    $r_noint           = $easyPay->_easypay_resdata[ "noint"           ];    //무이자여부

    $r_bank_cd         = $easyPay->_easypay_resdata[ "bank_cd"         ];    //은행코드

    $r_bank_nm         = $easyPay->_easypay_resdata[ "bank_nm"         ];    //은행명

    $r_account_no      = $easyPay->_easypay_resdata[ "account_no"      ];    //계좌번호

    $r_deposit_nm      = $easyPay->_easypay_resdata[ "deposit_nm"      ];    //입금자명

    $r_expire_date     = $easyPay->_easypay_resdata[ "expire_date"     ];    //계좌사용만료일

    $r_cash_res_cd     = $easyPay->_easypay_resdata[ "cash_res_cd"     ];    //현금영수증 결과코드

    $r_cash_res_msg    = $easyPay->_easypay_resdata[ "cash_res_msg"    ];    //현금영수증 결과메세지

    $r_cash_auth_no    = $easyPay->_easypay_resdata[ "cash_auth_no"    ];    //현금영수증 승인번호

    $r_cash_tran_date  = $easyPay->_easypay_resdata[ "cash_tran_date"  ];    //현금영수증 승인일시

    $r_auth_id         = $easyPay->_easypay_resdata[ "auth_id"         ];    //PhoneID

    $r_billid          = $easyPay->_easypay_resdata[ "billid"          ];    //인증번호

    $r_mobile_no       = $easyPay->_easypay_resdata[ "mobile_no"       ];    //휴대폰번호

    $r_ars_no          = $easyPay->_easypay_resdata[ "ars_no"          ];    //전화번호

    $r_cp_cd           = $easyPay->_easypay_resdata[ "cp_cd"           ];    //포인트사/쿠폰사

    $r_used_pnt        = $easyPay->_easypay_resdata[ "used_pnt"        ];    //사용포인트

    $r_remain_pnt      = $easyPay->_easypay_resdata[ "remain_pnt"      ];    //잔여한도

    $r_pay_pnt         = $easyPay->_easypay_resdata[ "pay_pnt"         ];    //할인/발생포인트

    $r_accrue_pnt      = $easyPay->_easypay_resdata[ "accrue_pnt"      ];    //누적포인트

    $r_remain_cpon     = $easyPay->_easypay_resdata[ "remain_cpon"     ];    //쿠폰잔액

    $r_used_cpon       = $easyPay->_easypay_resdata[ "used_cpon"       ];    //쿠폰 사용금액

    $r_mall_nm         = $easyPay->_easypay_resdata[ "mall_nm"         ];    //제휴사명칭

    $r_escrow_yn       = $easyPay->_easypay_resdata[ "escrow_yn"       ];    //에스크로 사용유무

    $r_complex_yn      = $easyPay->_easypay_resdata[ "complex_yn"      ];    //복합결제 유무

    $r_canc_acq_date   = $easyPay->_easypay_resdata[ "canc_acq_date"   ];    //매입취소일시

    $r_canc_date       = $easyPay->_easypay_resdata[ "canc_date"       ];    //취소일시

    $r_refund_date     = $easyPay->_easypay_resdata[ "refund_date"     ];    //환불예정일시



    /* -------------------------------------------------------------------------- */

    /* ::: 가맹점 DB 처리                                                         */

    /* -------------------------------------------------------------------------- */

    /* 응답코드(res_cd)가 "0000" 이면 정상승인 입니다.                            */

    /* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */

    /* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */

    /* -------------------------------------------------------------------------- */

    if ( $res_cd == "0000" )

    {

        $bDBProc = "true";     // DB처리 성공 시 "true", 실패 시 "false"

        if ( $bDBProc != "true" )

        {

            // 승인요청이 실패 시 아래 실행

            if( $TRAN_CD_NOR_PAYMENT == $tr_cd )

            {

                $easyPay->clearup_msg();



                $tr_cd = $TRAN_CD_NOR_MGR;

                $mgr_data = $easyPay->set_easypay_item("mgr_data");

                $easyPay->set_easypay_deli_us( $mgr_data, "mgr_txtype"      , "40"   );

                $easyPay->set_easypay_deli_us( $mgr_data, "org_cno"         , $r_cno     );

                $easyPay->set_easypay_deli_us( $mgr_data, "order_no"         , $order_no     );

                $easyPay->set_easypay_deli_us( $mgr_data, "req_ip"          , $client_ip );

                $easyPay->set_easypay_deli_us( $mgr_data, "req_id"          , "MALL_R_TRANS" );

                $easyPay->set_easypay_deli_us( $mgr_data, "mgr_msg"         , "DB 처리 실패로 망취소"  );



                $easyPay->easypay_exec($g_mall_id, $tr_cd, $order_no, $client_ip, $opt);

                $res_cd      = $easyPay->_easypay_resdata["res_cd"     ];    // 응답코드

                $res_msg     = $easyPay->_easypay_resdata["res_msg"    ];    // 응답메시지

                $r_cno       = $easyPay->_easypay_resdata["cno"        ];    // PG거래번호

                $r_canc_date = $easyPay->_easypay_resdata["canc_date"  ];    // 취소일시

            }

        }

    }

?>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="robots" content="noindex, nofollow">
<script src="https://www.gstatic.com/firebasejs/3.4.1/firebase.js"></script>
<script type="text/javascript">

    function f_submit(){

        var config = {
		apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
		authDomain: "jyg-custom.firebaseapp.com",
		databaseURL: "https://jyg-custom.firebaseio.com",
		storageBucket: "jyg-custom.appspot.com",
		messagingSenderId: "222150107123"
		};
		firebase.initializeApp(config);
		var user = firebase.auth().currentUser;
		var database = firebase.database();
		var firebase_flag = false;
		var firebase_flag = false;
		function link_firebase() {
			firebase.auth().onAuthStateChanged(function(user) {
			if (user) {
				if(firebase_flag==false) {
					var isAnonymous = user.isAnonymous;
					var user_id = user.uid;
					firebase_flag = true;
					alert(user_id);
					updateUserSubmit(user_id,'success','undecided','new');

					 setTimeout(function() {
						 window.top.location.href = "http://www.yookgak.com/complete.html"; 
					}, 3500);
				}
			} else {
				if(firebase_flag==true) {
					firebase_flag = false;
				}
			}
			});
		}
		function updateUserSubmit(uid,status,type,option) {
			firebase.database().ref().child('customers/'+uid+'/submits').once('value').then(function(snapshot) {
				var prefix = snapshot.val().prefix;
				writeUserSubmit(uid,status,type,option,prefix);
			});
		}
		function writeUserSubmit(uid,status,type,option,prefix) {
			if(pay_type=="81") {
				type = "batch";
			}else if(pay_type=="11") {
				type = "credit";
			}else if(pay_type=="21") {
				type = "transfer";
			}else if(pay_type=="31") {
				type = "mobile";
			}
		
			var postData = {
				trypay: status,
				type: type,
				option: option,
				prefix: prefix,
				result: {
					res_cd:res_cd,
					res_msg:res_msg,
					order_no:order_no,
					user_nm:user_nm,
					cno:cno,
					amount:amount,
					auth_no:auth_no,
					tran_date:tran_date,
					pnt_auth_no:pnt_auth_no,
					pnt_tran_date:pnt_tran_date,
					cpon_auth_no:cpon_auth_no,
					cpon_tran_date:cpon_tran_date,
					card_no:card_no,
					issuer_cd:issuer_cd,
					issuer_nm:issuer_nm,
					acquirer_cd:acquirer_cd,
					acquirer_nm:acquirer_nm,
					install_period:install_period,
					noint:noint,
					bank_cd:bank_cd,
					bank_nm:bank_nm,
					account_no:account_no,
					deposit_nm:deposit_nm,
					expire_date:expire_date,
					cash_res_cd:cash_res_cd,
					cash_res_msg:cash_res_msg,
					cash_auth_no:cash_auth_no,
					cash_tran_date:cash_tran_date,
					auth_id:auth_id,
					billid:billid,
					mobile_no:mobile_no,
					ars_no:ars_no,
					cp_cd:cp_cd,
					used_pnt:used_pnt,
					remain_pnt:remain_pnt,
					pay_pnt:pay_pnt,
					accrue_pnt:accrue_pnt,
					remain_cpon:remain_cpon,
					used_cpon:used_cpon,
					mall_nm:mall_nm,
					escrow_yn:escrow_yn,
					complex_yn:complex_yn,
					canc_acq_date:canc_acq_date,
					canc_date:canc_date,
					refund_date:refund_date,
					pay_type:pay_type
				}
			};
			var updates = {};
			updates['customers/'+uid+'/submits'] = postData;
			return firebase.database().ref().update(updates);
		}
		
var res_cd = document.getElementById('res_cd').value;
var res_msg = urldecode(document.getElementById('res_msg').value);
var order_no = document.getElementById('order_no').value;
var user_nm = urldecode(document.getElementById('user_nm').value);
var cno = document.getElementById('cno').value;
var amount = document.getElementById('amount').value;
var auth_no = document.getElementById('auth_no').value;
var tran_date = document.getElementById('tran_date').value;
var pnt_auth_no = document.getElementById('pnt_auth_no').value;
var pnt_tran_date = document.getElementById('pnt_tran_date').value;
var cpon_auth_no = document.getElementById('cpon_auth_no').value;
var cpon_tran_date = document.getElementById('cpon_tran_date').value;
var card_no = document.getElementById('card_no').value;
var issuer_cd = document.getElementById('issuer_cd').value;
var issuer_nm = urldecode(document.getElementById('issuer_nm').value);
var acquirer_cd = document.getElementById('acquirer_cd').value;
var acquirer_nm = urldecode(document.getElementById('acquirer_nm').value);
var install_period = document.getElementById('install_period').value;
var noint = document.getElementById('noint').value;
var bank_cd = document.getElementById('bank_cd').value;
var bank_nm = urldecode(document.getElementById('bank_nm').value);
var account_no = document.getElementById('account_no').value;
var deposit_nm = urldecode(document.getElementById('deposit_nm').value);
var expire_date = document.getElementById('expire_date').value;
var cash_res_cd = document.getElementById('cash_res_cd').value;
var cash_res_msg = urldecode(document.getElementById('cash_res_msg').value);
var cash_auth_no = document.getElementById('cash_auth_no').value;
var cash_tran_date = document.getElementById('cash_tran_date').value;
var auth_id = document.getElementById('auth_id').value;
var billid = document.getElementById('billid').value;
var mobile_no = document.getElementById('mobile_no').value;
var ars_no = document.getElementById('ars_no').value;
var cp_cd = document.getElementById('cp_cd').value;
var used_pnt = document.getElementById('used_pnt').value;
var remain_pnt = document.getElementById('remain_pnt').value;
var pay_pnt = document.getElementById('pay_pnt').value;
var accrue_pnt = document.getElementById('accrue_pnt').value;
var remain_cpon = document.getElementById('remain_cpon').value;
var used_cpon = document.getElementById('used_cpon').value;
var mall_nm = urldecode(document.getElementById('mall_nm').value);
var escrow_yn = document.getElementById('escrow_yn').value;
var complex_yn = document.getElementById('complex_yn').value;
var canc_acq_date = document.getElementById('canc_acq_date').value;
var canc_date = document.getElementById('canc_date').value;
var refund_date = document.getElementById('refund_date').value;
var pay_type = document.getElementById('pay_type').value;

var paymentCancelled = urldecode(res_cd + "등록이 취소되었습니다.\n장바구니로 돌아갑니다.\n" + res_msg);
		if(res_cd!="0000") {
			alert(paymentCancelled);
			window.top.location.href = "http://www.yookgak.com/cart.html"; 
		}else {
			link_firebase();
		}

    }
	function urldecode( str )
    {
        // 공백 문자인 + 를 처리하기 위해 +('%20') 을 공백으로 치환
        return decodeURIComponent((str + '').replace(/\+/g, '%20'));
	}

</script>



<body onload="f_submit();">
<p id="waitingText"></p>

  
    
    
<form name="frm" method="post" action="http://www.yookgak.com/complete.html">


    <input type="hidden" id="res_cd" name="res_cd"          value="<?=$res_cd?>">          <!-- 결과코드 //-->
    <input type="hidden" id="res_msg"  name="res_msg"         value="<?=$res_msg?>">         <!-- 결과메시지 //-->
    <input type="hidden" id="order_no" name="order_no"        value="<?=$order_no?>">        <!-- 주문번호 //-->
    <input type="hidden" id="user_nm" name="user_nm"         value="<?=$user_nm?>">         <!-- 구매자명  //-->
    <input type="hidden" id="cno" name="cno"              value="<?=$r_cno?>">             <!-- PG거래번호 //-->
    <input type="hidden" id="amount" name="amount"          value="<?=$r_amount?>">          <!-- 총 결제금액 //-->
    <input type="hidden" id="auth_no" name="auth_no"         value="<?=$r_auth_no?>">         <!-- 승인번호 //-->
    <input type="hidden" id="tran_date" name="tran_date"       value="<?=$r_tran_date?>">       <!-- 거래일시 //-->
    <input type="hidden" id="pnt_auth_no"  name="pnt_auth_no"     value="<?=$r_pnt_auth_no?>">     <!-- 포인트 승인 번호 //-->
    <input type="hidden" id="pnt_tran_date" name="pnt_tran_date"   value="<?=$r_pnt_tran_date?>">   <!-- 포인트 승인 일시 //-->
    <input type="hidden" id="cpon_auth_no" name="cpon_auth_no"    value="<?=$r_cpon_auth_no?>">    <!-- 쿠폰 승인 번호 //-->
    <input type="hidden" id="cpon_tran_date" name="cpon_tran_date"  value="<?=$r_cpon_tran_date?>">  <!-- 쿠폰 승인 일시 //-->
    <input type="hidden" id="card_no" name="card_no"         value="<?=$r_card_no?>">         <!-- 카드번호 //-->
    <input type="hidden" id="issuer_cd" name="issuer_cd"       value="<?=$r_issuer_cd?>">       <!-- 발급사코드 //-->
    <input type="hidden" id="issuer_nm" name="issuer_nm"       value="<?=$r_issuer_nm?>">       <!-- 발급사명 //-->
    <input type="hidden" id="acquirer_cd" name="acquirer_cd"     value="<?=$r_acquirer_cd?>">     <!-- 매입사코드 //-->
    <input type="hidden" id="acquirer_nm" name="acquirer_nm"     value="<?=$r_acquirer_nm?>">     <!-- 매입사명 //-->
    <input type="hidden" id="install_period" name="install_period"  value="<?=$r_install_period?>">  <!-- 할부개월 //-->
    <input type="hidden" id="noint" name="noint"           value="<?=$r_noint?>">           <!-- 무이자여부 //-->
    <input type="hidden" id="bank_cd"  name="bank_cd"         value="<?=$r_bank_cd?>">         <!-- 은행코드 //-->
    <input type="hidden" id="bank_nm"  name="bank_nm"         value="<?=$r_bank_nm?>">         <!-- 은행명 //-->
    <input type="hidden" id="account_no" name="account_no"      value="<?=$r_account_no?>">      <!-- 계좌번호 //-->
    <input type="hidden" id="deposit_nm"  name="deposit_nm"      value="<?=$r_deposit_nm?>">      <!-- 입금자명 //-->
    <input type="hidden" id="expire_date"  name="expire_date"     value="<?=$r_expire_date?>">     <!-- 계좌사용만료일시 //-->
    <input type="hidden" id="cash_res_cd" name="cash_res_cd"     value="<?=$r_cash_res_cd?>">     <!-- 현금영수증 결과코드 //-->
    <input type="hidden" id="cash_res_msg" name="cash_res_msg"    value="<?=$r_cash_res_msg?>">    <!-- 현금영수증 결과메세지 //-->
    <input type="hidden" id="cash_auth_no"  name="cash_auth_no"    value="<?=$r_cash_auth_no?>">    <!-- 현금영수증 승인번호 //-->
    <input type="hidden" id="cash_tran_date" name="cash_tran_date"  value="<?=$r_cash_tran_date?>">  <!-- 현금영수증 승인일시 //-->
    <input type="hidden" id="auth_id" name="auth_id"         value="<?=$r_auth_id?>">         <!-- PhoneID //-->
    <input type="hidden" id="billid" name="billid"          value="<?=$r_billid?>">          <!-- 인증번호 //-->
    <input type="hidden" id="mobile_no" name="mobile_no"       value="<?=$r_mobile_no?>">       <!-- 휴대폰번호 //-->
    <input type="hidden" id="ars_no" name="ars_no"          value="<?=$r_ars_no?>">          <!-- ARS 전화번호 //-->
    <input type="hidden" id="cp_cd" name="cp_cd"           value="<?=$r_cp_cd?>">           <!-- 포인트사 //-->
    <input type="hidden" id="used_pnt" name="used_pnt"        value="<?=$r_used_pnt?>">        <!-- 사용포인트 //-->
    <input type="hidden" id="remain_pnt" name="remain_pnt"      value="<?=$r_remain_pnt?>">      <!-- 잔여한도 //-->
    <input type="hidden" id="pay_pnt" name="pay_pnt"         value="<?=$r_pay_pnt?>">         <!-- 할인/발생포인트 //-->
    <input type="hidden" id="accrue_pnt" name="accrue_pnt"      value="<?=$r_accrue_pnt?>">      <!-- 누적포인트 //-->
    <input type="hidden" id="remain_cpon" name="remain_cpon"     value="<?=$r_remain_cpon?>">     <!-- 쿠폰잔액 //-->
    <input type="hidden" id="used_cpon"  name="used_cpon"       value="<?=$r_used_cpon?>">       <!-- 쿠폰 사용금액 //-->
    <input type="hidden" id="mall_nm" name="mall_nm"         value="<?=$r_mall_nm?>">         <!-- 제휴사명칭 //-->
    <input type="hidden" id="escrow_yn"  name="escrow_yn"       value="<?=$r_escrow_yn?>">       <!-- 에스크로 사용유무 //-->
    <input type="hidden" id="complex_yn" name="complex_yn"      value="<?=$r_complex_yn?>">      <!-- 복합결제 유무 //-->
    <input type="hidden" id="canc_acq_date" name="canc_acq_date"   value="<?=$r_canc_acq_date?>">   <!-- 매입취소일시 //-->
    <input type="hidden" id="canc_date"  name="canc_date"       value="<?=$r_canc_date?>">       <!-- 취소일시 //-->
    <input type="hidden" id="refund_date" name="refund_date"     value="<?=$r_refund_date?>">     <!-- 환불예정일시 //-->
    <input type="hidden" id="pay_type" name="pay_type"        value="<?=$pay_type?>">          <!-- 결제수단 //-->



</form>
<script>
		var waiting = urldecode("결제정보를 받고 있습니다. 잠시만 기다려주십시오.");
		document.getElementById("waitingText").innerHTML = waiting;
</script>

</body>

</html>