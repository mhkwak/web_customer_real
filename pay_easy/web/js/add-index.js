// JavaScript Document
var config = {
	apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
	authDomain: "jyg-custom.firebaseapp.com",
	databaseURL: "https://jyg-custom.firebaseio.com",
	storageBucket: "jyg-custom.appspot.com",
	messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
var customer = firebase.auth().currentUser;
var database = firebase.database();
var customers;
var customerParse = new Array();
var customersCount;
var cartParse = new Array();
var cartCount;
var orderParse = new Array();
var orderCount;
var orderList = new Array();
var orderListCount;
var lastUpdate;
var customID = 'OH9mKffOtxMax8lSZMiAzM5VpRJ2';
var goodsBox;
var goodReady;
var orderCountC = 0;
var orderCostC = 0;
var orderLastC = 0;
function getLastUpdate() {
	return lastUpdate;
}
function init() {
	lastUpdate = new Date().toString();
	goodReady = false;
	initViewFlag();
	loadFirebase();
	waitingFirebase();
	pager('hall');
}
function loadFirebase() {
	firebase.database().ref("json/goods_info").once("value").then(function(snapshot) {
		goodsBox = snapshot.val();
		goodReady = true;
	});
}
function waitingFirebase() {
	console.log("waitingFirebase");
	firebase.database().ref('customers').on('value', function(snapshot) {
		customers = snapshot.val();
		lastUpdate = new Date().toString();
		readFirebaseData();
	});
}
function readFirebaseData() {
	console.log("readFirebaseDate");
	customersCount = 0;
	orderListCount = 0;
	$.each(customers, function(uid, customerValue){
		if(uid==customID) {
			orderCountC = customerValue.order_records.current.count;
			orderCostC = customerValue.order_records.current.cost;
			orderLastC = customerValue.order_records.current.last;
		}
		customerParse[customersCount] = {};
		customerParse[customersCount].indexo = customersCount;
		customerParse[customersCount].uid = uid;
		customerParse[customersCount].username = customerValue.username;
		customerParse[customersCount].useremail = customerValue.useremail;
		customerParse[customersCount].userphone = customerValue.userphone;
		if(customerValue.rewards.points_history!=null){
		customerParse[customersCount].userstamp = customerValue.rewards.points_history[0].timestamp;
		}else {customerParse[customersCount].userstamp = "";}
		customersCount++;
	});
	console.log(customersCount);
}
/**
function orderSort(a, b) {
	if(a.userstamp === b.userstamp){
		if(a.indexo === b.indexo){ return 0; }
		return  a.indexo < b.indexo ? 1 : -1;
	} return  a.userstamp < b.userstamp ? 1 : -1;
}**/
function ViewingPhone(tel){
	var return_tel;
	if(tel.indexOf('-')!=-1) {
		return_tel = tel;
	}else {
		return_tel = tel.substring(0,3)+'-'+tel.substring(3,7)+'-'+tel.substring(7,11);
	}
	return return_tel;
}


function getPriceIndex(product,index) {
	var return_price = 0;
	if(!goodReady) {return 0;}
	$.each(goodsBox.goods_info, function(key, value){
		if(key==product) {
			var cDate = 0;
			var cPrice = 0;
			$.each(value.prices, function(dateKey, priceValue){
				if(dateKey>cDate) {
					cPrice = Number(priceValue)*6;
				}
			});
			if(value.options[index].price!=-1) {
				return_price = cPrice + Number(value.options[index].price);
			}
			else {
				return_price = cPrice;
			}
		}
	});
	console.log('priceIndex: '+product+" : "+return_price);
	return return_price;
}

var customOrder;
function countOrderGoods() {
	customOrder.order[0].count[0] = document.getElementById('porkbelly-fresh-1').value;
	customOrder.order[0].count[1] = document.getElementById('porkbelly-fresh-2').value;
	customOrder.order[0].count[2] = document.getElementById('porkbelly-fresh-3').value;
	customOrder.order[0].count[3] = document.getElementById('porkbelly-fresh-4').value;
	customOrder.order[0].count[4] = document.getElementById('porkbelly-fresh-5').value;
	
	customOrder.order[1].count[0] = document.getElementById('porkneck-fresh-1').value;
	customOrder.order[1].count[1] = document.getElementById('porkneck-fresh-2').value;
	customOrder.order[1].count[2] = document.getElementById('porkneck-fresh-3').value;
	customOrder.order[1].count[3] = document.getElementById('porkneck-fresh-4').value;
	customOrder.order[1].count[4] = document.getElementById('porkneck-fresh-5').value;
	
	customOrder.order[2].count[0] = document.getElementById('porkbelly-clean-1').value;
	customOrder.order[2].count[1] = document.getElementById('porkbelly-clean-2').value;
	customOrder.order[2].count[2] = document.getElementById('porkbelly-clean-3').value;
	customOrder.order[2].count[3] = document.getElementById('porkbelly-clean-4').value;
	customOrder.order[2].count[4] = document.getElementById('porkbelly-clean-5').value;
	
	customOrder.order[3].count[0] = document.getElementById('porkneck-clean-1').value;
	customOrder.order[3].count[1] = document.getElementById('porkneck-clean-2').value;
	customOrder.order[3].count[2] = document.getElementById('porkneck-clean-3').value;
	customOrder.order[3].count[3] = document.getElementById('porkneck-clean-4').value;
	customOrder.order[3].count[4] = document.getElementById('porkneck-clean-5').value;
	
	customOrder.order[4].count[0] = document.getElementById('porkbelly-sulfur-1').value;
	customOrder.order[4].count[1] = document.getElementById('porkbelly-sulfur-2').value;
	customOrder.order[4].count[2] = document.getElementById('porkbelly-sulfur-3').value;
	customOrder.order[4].count[3] = document.getElementById('porkbelly-sulfur-4').value;
	customOrder.order[4].count[4] = document.getElementById('porkbelly-sulfur-5').value;
	
	customOrder.order[5].count[0] = document.getElementById('porkneck-sulfur-1').value;
	customOrder.order[5].count[1] = document.getElementById('porkneck-sulfur-2').value;
	customOrder.order[5].count[2] = document.getElementById('porkneck-sulfur-3').value;
	customOrder.order[5].count[3] = document.getElementById('porkneck-sulfur-4').value;
	customOrder.order[5].count[4] = document.getElementById('porkneck-sulfur-5').value;
	
	customOrder.order[6].count[0] = document.getElementById('porkbelly-event-4').value;
	
	customOrder.order[7].count[0] = document.getElementById('porkbelly-event-6').value;
	
	customOrder.order[8].count[0] = document.getElementById('porkneck-event-4').value;
	
	customOrder.order[9].count[0] = document.getElementById('porkneck-event-6').value;
	
	customOrder.order[10].count[0] = document.getElementById('porkmix-event-6').value;
	
	
	customOrder.order[11].count[0] = document.getElementById('porkbelly-eventC-4').value;
	
	customOrder.order[12].count[0] = document.getElementById('porkbelly-eventC-6').value;
	
	customOrder.order[13].count[0] = document.getElementById('porkneck-eventC-4').value;
	
	customOrder.order[14].count[0] = document.getElementById('porkneck-eventC-6').value;
	
	customOrder.order[15].count[0] = document.getElementById('porkmix-eventC-6').value;
}
function KorDay(index) {
	if(index==0) {return ' (일)';}
	else if(index==1) {return ' (월)';}
	else if(index==2) {return ' (화)';}
	else if(index==3) {return ' (수)';}
	else if(index==4) {return ' (목)';}
	else if(index==5) {return ' (금)';}
	else if(index==6) {return ' (토)';}
}
function addNewOrder() {
	customOrder = {};
	customOrder.name = document.getElementById('cust_name').value;
	customOrder.phone = document.getElementById('cust_phone').value;
	customOrder.po = document.getElementById('cust_po').value;
	customOrder.address = document.getElementById('cust_addr').value;
	customOrder.room = document.getElementById('cust_room').value;
	customOrder.request = document.getElementById('cust_request').value;
	customOrder.shipping = document.getElementById('cust_shipping').value;
	var selectDate = new Date(customOrder.shipping);
	customOrder.shipping = customOrder.shipping + KorDay(selectDate.getDay());
	customOrder.order=new Array();
	customOrder.order[0] = {kor:"삼겹 일반",name:"porkbelly-fresh"};
	customOrder.order[1] = {kor:"목살 일반",name:"porkneck-fresh"};
	customOrder.order[2] = {kor:"삼겹 무항생제",name:"porkbelly-clean"};
	customOrder.order[3] = {kor:"목살 무항생제",name:"porkneck-clean"};
	customOrder.order[4] = {kor:"삼겹 유황",name:"porkbelly-sulfur"};
	customOrder.order[5] = {kor:"목살 유황",name:"porkneck-sulfur"};
	customOrder.order[6] = {kor:"김장 삼겹-4",name:"porkbelly-event-4"};
	customOrder.order[7] = {kor:"김장 삼겹-6",name:"porkbelly-event-6"};
	customOrder.order[8] = {kor:"김장 목살-4",name:"porkneck-event-4"};
	customOrder.order[9] = {kor:"김장 목살-6",name:"porkneck-event-6"};
	customOrder.order[10] = {kor:"김장 반반-6",name:"porkmix-event-6"};
	customOrder.order[11] = {kor:"무항 삼겹-4",name:"porkbelly-eventC-4"};
	customOrder.order[12] = {kor:"무항 삼겹-6",name:"porkbelly-eventC-6"};
	customOrder.order[13] = {kor:"무항 목살-4",name:"porkneck-eventC-4"};
	customOrder.order[14] = {kor:"무항 목살-6",name:"porkneck-eventC-6"};
	customOrder.order[15] = {kor:"무항 반반-6",name:"porkmix-eventC-6"};
	for(var i=0;i<16;i++) {
		customOrder.order[i].count = new Array();
		customOrder.order[i].price = new Array();
		if(i<6){
			for(var j=0;j<5;j++) {
				customOrder.order[i].price[j] = getPriceIndex(customOrder.order[i].name,j);
				customOrder.order[i].count[j] = 0;
			}
		}
		else {
			customOrder.order[i].price[0] = getPriceIndex(customOrder.order[i].name,0);
			customOrder.order[i].count[0] = 0;
		}
			
	}
	countOrderGoods();
	
	if(!formCheck()) {
		alert("오류! - 입력하신 정보를 확인해주세요.");
	}
	else {
		var shipFlag = 0;
		var printJson = {};
		var toodate = new Date();
		var goodSum = 0;
		if(orderCount<2) {goodSum = 2500;}
		printJson.goods = new Array();
		var tempC = 0;
		for(var i=0;i<16;i++) {
			if(i<6) {
				for(var j=0;j<5;j++) {
					for(var k=0;k<customOrder.order[i].count[j];k++) {
						shipFlag++;
						printJson.goods[tempC] = {};
						printJson.goods[tempC].indexo = tempC;
						printJson.goods[tempC].productID = customOrder.order[i].name;
						printJson.goods[tempC].productIndex = customOrder.order[i].price[j]/6;
						printJson.goods[tempC].productOption = String(Number(j)+1);
						printJson.goods[tempC].productPrice = String(customOrder.order[i].price[j]);
						printJson.goods[tempC].productWeight = 600;
						goodSum = goodSum + Number(printJson.goods[tempC].productPrice);
						tempC++;
					}
				}
			}else {
				for(var k=0;k<customOrder.order[i].count[0];k++) {
					shipFlag=shipFlag+2;
					printJson.goods[tempC] = {};
					printJson.goods[tempC].indexo = tempC;
					printJson.goods[tempC].productID = customOrder.order[i].name;
					printJson.goods[tempC].productIndex = customOrder.order[i].price[0]/6;
					printJson.goods[tempC].productOption = "1";
					printJson.goods[tempC].productPrice = String(customOrder.order[i].price[0]);
					if(i==6 || i==8 || i==11 || i==13) {
						printJson.goods[tempC].productWeight = 2400;
					}else {
						printJson.goods[tempC].productWeight = 3600;
					}
					goodSum = goodSum + Number(printJson.goods[tempC].productPrice);
					tempC++;
				}
			}
		}
		if(shipFlag<2) {
			printJson.totalPrice = Number(goodSum)+2500;
		}else {
			printJson.totalPrice = goodSum;
		}
				
		printJson.histories = new Array();
		printJson.histories[0] = {};
		printJson.histories[0].error = "none";
		printJson.histories[0].location = "undefined";
		printJson.histories[0].status = "waiting";
		printJson.histories[0].timestamp = toodate;
		
		printJson.informations = {};
		printJson.informations.address = customOrder.address;
		printJson.informations.customer = "";
		printJson.informations.customeremail = "help@yookgak.com";
		printJson.informations.customername = "정육각";
		printJson.informations.customerphone = "0218000658";
		printJson.informations.name = customOrder.name;
		printJson.informations.phone = customOrder.phone;
		printJson.informations.room = customOrder.room;
		printJson.informations.pointUsed = "0";
		printJson.informations.postcode = customOrder.po;
		printJson.informations.request = customOrder.request;
		//printJson.informations.shippingCode = "none";
		printJson.informations.shippingDate = customOrder.shipping;
		
		printJson.payment = {};
		printJson.payment.address = "new";
		printJson.payment.option = "0";
		printJson.payment.prefix = "direct";
		printJson.payment.price = "0";
		printJson.payment.trypay = "success";
		printJson.payment.type = "direct";
		
		
			////Firebase Write;
			var postData = printJson;
			var updates = {};
			updates['customers/'+customID+'/order_records/'+orderCountC] = postData;
			firebase.database().ref().update(updates);
		
			var postData1 = Number(orderCountC)+Number(1);
			var updates1 = {};
			updates1['customers/'+customID+'/order_records/current/count'] = postData1;
			firebase.database().ref().update(updates1);
			setTimeout(function() {
				alert("등록완료");
				location.reload();
			},2000);
		}
}
function formCheck() {
	if(!goodReady) {return false;}
	if(customOrder.name==null || customOrder.name=='') {return false;}
	if(customOrder.phone==null || customOrder.phone=='') {return false;}
	if(customOrder.po==null || customOrder.po=='') {return false;}
	if(customOrder.address==null || customOrder.address=='') {return false;}
	if(customOrder.room==null || customOrder.room=='') {return false;}
	return true;
}