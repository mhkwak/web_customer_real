// JavaScript Document
var viewFlag = {};
var orderFlag = {};
function initViewFlag() {
	viewFlag.pager = 'hall';
	viewFlag.date = false;
	viewFlag.name = false;
	viewFlag.cart = false;
	viewFlag.active = true;
	viewFlag.current = false;
	viewFlag.complete = false;
	
	orderFlag.view = 0;
	//0=  1<=   2
}
function checkViewFlag(name) {
	if(name=='pager'){ return viewFlag.pager; }
	else if(name=='date'){ return viewFlag.date; }
	else if(name=='name'){ return viewFlag.name; }
	else if(name=='cart'){ return viewFlag.cart; }
	else if(name=='active'){ return viewFlag.active; }
	else if(name=='current'){ return viewFlag.current; }
	else if(name=='complete'){ return viewFlag.complete; }
	return false;
}
function checkOrderFlag(name) {
	if(name=='view') { return orderFlag.view; }
	return false;
}
function bttn(btnKey) {
	if(btnKey==0){
		orderFlag.view++;
		if(orderFlag.view==3) {orderFlag.view=0;}
	}
	print_orderList();
}
function radio(radioKey) {
	if(radioKey==200) { viewFlag.date = !viewFlag.date; viewFlag.name=false;}
	else if(radioKey==100) { viewFlag.name = !viewFlag.name; viewFlag.date=false;}
	else if(radioKey==0) { viewFlag.cart = !viewFlag.cart;}
	else if(radioKey==1) { viewFlag.active = !viewFlag.active;}
	else if(radioKey==2) { viewFlag.current = !viewFlag.current;}
	else if(radioKey==3) { viewFlag.complete = !viewFlag.complete;}
	print_userList();
}

function pager(pagename) {
	if(pagename=='hall'){
		viewFlag.pager = 'hall';
		document.getElementById('page_title').innerHTML = "전체보기 ("+getLastUpdate()+")";
	}
	else if(pagename=='order'){
		viewFlag.pager = 'order';
		document.getElementById('page_title').innerHTML = "주문목록 ("+getLastUpdate()+")";
	}
	else if(pagename=='customer'){
		viewFlag.pager = 'customer';
		document.getElementById('page_title').innerHTML = "고객목록 ("+getLastUpdate()+")";
	}
}
function viewDetail(orderIndex) {
	console.log(orderIndex);
}
var goodsList = new Array();
var goodsListCount = 0;
var goodsCategory = new Array();
var goodsCategoryCount = 0;
function getGoodsListCount() {
	return goodsListCount;
}
function getgoodsCategoryCount() {
	return goodsCategoryCount;
}
function getGoodsCategory(i,j) {
	if(j==0) { return goodsCategory[i].count1; }
	if(j==1) { return goodsCategory[i].count2; }
	if(j==2) { return goodsCategory[i].count3; }
	if(j==3) { return goodsCategory[i].count4; }
	if(j==4) { return goodsCategory[i].count5; }
}
function getGoodsCategoryName(i) {
	return goodsCategory[i].category;
}
function initSum() {
	goodsListCount=18;
	goodsList[0] = {kor:"삼겹 일반",category:"porkbelly-fresh",name:"porkbelly-fresh",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[1] = {kor:"목살 일반",category:"porkneck-fresh",name:"porkneck-fresh",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[2] = {kor:"삼겹 무항생제",category:"porkbelly-clean",name:"porkbelly-clean",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[3] = {kor:"목살 무항생제",category:"porkneck-clean",name:"porkneck-clean",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[4] = {kor:"삼겹 패키지",category:"porkbelly-fresh",name:"porkbelly-gift",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[5] = {kor:"목살 패키지",category:"porkneck-fresh",name:"porkneck-gift",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[6] = {kor:"삼겹 유황",category:"porkbelly-sulfur",name:"porkbelly-sulfur",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[7] = {kor:"목살 유황",category:"porkneck-sulfur",name:"porkneck-sulfur",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	
	goodsList[8] = {kor:"김장 일반삼겹-4",category:"porkbelly-fresh",name:"porkbelly-event-4",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[9] = {kor:"김장 일반목살-4",category:"porkneck-fresh",name:"porkneck-event-6",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[10] = {kor:"김장 일반삼겹-6",category:"porkbelly-fresh",name:"porkbelly-event-4",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[11] = {kor:"김장 일반목살-6",category:"porkneck-fresh",name:"porkneck-event-6",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[12] = {kor:"김장 일반반반-6",category:"porkmix-fresh",name:"porkmix-event-6",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	
	goodsList[13] = {kor:"김장 무항삼겹-4",category:"porkbelly-clean",name:"porkbelly-eventC-4",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[14] = {kor:"김장 무항목살-4",category:"porkneck-clean",name:"porkneck-eventC-6",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[15] = {kor:"김장 무항삼겹-6",category:"porkbelly-clean",name:"porkbelly-eventC-4",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[16] = {kor:"김장 무항목살-6",category:"porkneck-clean",name:"porkneck-eventC-6",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
	goodsList[17] = {kor:"김장 무항반반-6",category:"porkmix-clean",name:"porkmix-eventC-6",count:0,c9:0,c10:0,c11:0,c12:0,c13:0,c14:0,c15:0,c16:0};
}
function initCategory() {
	goodsCategoryCount=4;
	goodsCategory[0] = {category:"porkbelly-fresh",count1:0,count2:0,count3:0,count4:0,count5:0};
	goodsCategory[1] = {category:"porkneck-fresh",count1:0,count2:0,count3:0,count4:0,count5:0};
	goodsCategory[2] = {category:"porkbelly-clean",count1:0,count2:0,count3:0,count4:0,count5:0};
	goodsCategory[3] = {category:"porkneck-clean",count1:0,count2:0,count3:0,count4:0,count5:0};
}
function getName(i) {
	return goodsList[i].name;
}
function getKor(i) {
	return goodsList[i].kor;
}
function getCategory(i) {
	return goodsList[i].category;
}
function addSum(product,count){
	//console.log("historyTimes "+historyTimes);
	for(var i=0;i<goodsListCount;i++) {
		if(goodsList[i].name==product) {
			goodsList[i].count = goodsList[i].count + Number(count);
			break;
		}
	}
}
function addSumC(product,poption,count,historyTimes,shippingTime){
	var shipDate = new Date(shippingTime);
	shipDate.setDate(shipDate.getDate()-1);
	var hisDate = new Date(historyTimes);
	var tooDate = new Date();
	if(shipDate.getFullYear() == tooDate.getFullYear() && shipDate.getMonth() == tooDate.getMonth() && shipDate.getDate() == tooDate.getDate()){}
	else{return;}
	var times='';
	if(hisDate.getFullYear() == tooDate.getFullYear() && hisDate.getMonth() == tooDate.getMonth() && hisDate.getDate() == tooDate.getDate()){
		times=Number(hisDate.getHours());
	}else {
		times=0;
	}
	/**
	var k=0;
	if(product=='porkbelly')
	for(var k=0;k<4;k++) {
		if(goodsCategory[k].category==product) {
			console.log('TT : '+product + " / " + poption + " = " + count);
			if(poption==1) { goodsCategory[k].count1 = goodsCategory[k].count1 + Number(count); }
			if(poption==2) { goodsCategory[k].count2 = goodsCategory[k].count2 + Number(count); }
			if(poption==3) { goodsCategory[k].count3 = goodsCategory[k].count3 + Number(count); }
			if(poption==4) { goodsCategory[k].count4 = goodsCategory[k].count4 + Number(count); }
			if(poption==5) { goodsCategory[k].count5 = goodsCategory[k].count5 + Number(count); }
		}
	}***/
	for(var i=0;i<goodsListCount;i++) {
		//alert(i+":"+goodsList[i].name+"+"+product + ":" + count);
		if(goodsList[i].name==product) {
			var eventFlag = true;
			for(var k=0;k<4;k++) {
				if(goodsList[i].category==goodsCategory[k].category) {
					eventFlag = false;
					console.log(product + ":" + count);
					if(poption==1) { goodsCategory[k].count1 = goodsCategory[k].count1 + Number(count); }
					if(poption==2) { goodsCategory[k].count2 = goodsCategory[k].count2 + Number(count); }
					if(poption==3) { goodsCategory[k].count3 = goodsCategory[k].count3 + Number(count); }
					if(poption==4) { goodsCategory[k].count4 = goodsCategory[k].count4 + Number(count); }
					if(poption==5) { goodsCategory[k].count5 = goodsCategory[k].count5 + Number(count); }
				}
			}
			if(eventFlag){
				var k=0;
				if(product=='porkbelly-event-4') {k=0; count = 4; }
				else if(product=='porkbelly-event-6') {k=0; count = 6; }
				else if(product=='porkneck-event-4') {k=1; count = 4; }
				else if(product=='porkneck-event-6') {k=1; count = 6; }
				else if(product=='porkbelly-eventC-4') {k=2; count = 4; }
				else if(product=='porkbelly-eventC-6') {k=2; count = 6; }
				else if(product=='porkneck-eventC-4') {k=3; count = 4; }
				else if(product=='porkneck-eventC-6') {k=3; count = 6; }
				else if(product=='porkmix-event-6') {k=-1; count = 3;}
				else if(product=='porkmix-eventC-6') {k=-2; count = 3;}
				if(k==-1) {
					if(poption==1) { goodsCategory[0].count1 = goodsCategory[0].count1 + Number(count); goodsCategory[1].count1 = goodsCategory[1].count1 + Number(count);}
					if(poption==2) { goodsCategory[0].count2 = goodsCategory[0].count2 + Number(count); goodsCategory[1].count2 = goodsCategory[1].count2 + Number(count);}
					if(poption==3) { goodsCategory[0].count3 = goodsCategory[0].count3 + Number(count); goodsCategory[1].count3 = goodsCategory[1].count3 + Number(count);}
					if(poption==4) { goodsCategory[0].count4 = goodsCategory[0].count4 + Number(count); goodsCategory[1].count4 = goodsCategory[1].count4 + Number(count);}
					if(poption==5) { goodsCategory[0].count5 = goodsCategory[0].count5 + Number(count); goodsCategory[1].count5 = goodsCategory[1].count5 + Number(count);}
				} else if(k==-2) {
					if(poption==1) { goodsCategory[2].count1 = goodsCategory[2].count1 + Number(count); goodsCategory[3].count1 = goodsCategory[3].count1 + Number(count);}
					if(poption==2) { goodsCategory[2].count2 = goodsCategory[2].count2 + Number(count); goodsCategory[3].count2 = goodsCategory[3].count2 + Number(count);}
					if(poption==3) { goodsCategory[2].count3 = goodsCategory[2].count3 + Number(count); goodsCategory[3].count3 = goodsCategory[3].count3 + Number(count);}
					if(poption==4) { goodsCategory[2].count4 = goodsCategory[2].count4 + Number(count); goodsCategory[3].count4 = goodsCategory[3].count4 + Number(count);}
					if(poption==5) { goodsCategory[2].count5 = goodsCategory[2].count5 + Number(count); goodsCategory[3].count5 = goodsCategory[3].count5 + Number(count);}
				} else {
					if(poption==1) { goodsCategory[k].count1 = goodsCategory[k].count1 + Number(count); }
					if(poption==2) { goodsCategory[k].count2 = goodsCategory[k].count2 + Number(count); }
					if(poption==3) { goodsCategory[k].count3 = goodsCategory[k].count3 + Number(count); }
					if(poption==4) { goodsCategory[k].count4 = goodsCategory[k].count4 + Number(count); }
					if(poption==5) { goodsCategory[k].count5 = goodsCategory[k].count5 + Number(count); }
				}
			}
			//alert(goodsList[i].name+"+"+product + ":" + count);
			//console.log(product + " times : "+i+" : "+ times);
			if(times<9 || times>15){goodsList[i].c9 = goodsList[i].c9 + Number(count);}
			else if(times==9){goodsList[i].c10 = goodsList[i].c10 + Number(count);}
			else if(times==10){goodsList[i].c11 = goodsList[i].c11 + Number(count);}
			else if(times==11){goodsList[i].c12 = goodsList[i].c12 + Number(count);}
			else if(times==12){goodsList[i].c13 = goodsList[i].c13 + Number(count);}
			else if(times==13){goodsList[i].c14 = goodsList[i].c14 + Number(count);}
			else if(times==14){goodsList[i].c15 = goodsList[i].c15 + Number(count);}
			else if(times==15){goodsList[i].c16 = goodsList[i].c16 + Number(count);}
			break;
		}
	}
}
function getSumC(product,index){
	var ri = 0;
	for(var i=0;i<goodsListCount;i++) {
		if(product==goodsList[i].name) {ri = i; break;}
	}
	var returnSum = 0;
	if(index==9){ returnSum = goodsList[ri].c9; }
	else if(index==10){ returnSum = goodsList[ri].c10; }
	else if(index==11){ returnSum = goodsList[ri].c11; }
	else if(index==12){ returnSum = goodsList[ri].c12; }
	else if(index==13){ returnSum = goodsList[ri].c13; }
	else if(index==14){ returnSum = goodsList[ri].c14; }
	else if(index==15){ returnSum = goodsList[ri].c15; }
	else if(index==16){ returnSum = goodsList[ri].c16; }
	return returnSum;
}
function getSum(product) {
	var returnSum = 0;
	for(var i=0;i<goodsListCount;i++) {
		if(product==goodsList[i].name) {returnSum = goodsList[i].count; break;}
	}
	return returnSum;
}