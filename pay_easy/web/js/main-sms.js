var signup_confirmed = false;
var phone_from = "18000658";
function certification_send() {
	var v_userName = document.getElementById('new_name').value;
	var v_userPhone = document.getElementById('new_phone').value;
	if (!$('#agree_afe').is(":checked")){
		Materialize.toast('나이를 확인해주세요.', 2000);
	}else if(!$('#agree_usage').is(":checked")) {
		Materialize.toast('이용약관에 동의해주세요.', 2000);
	}else if(!$('#agree_privacy').is(":checked")) {
		Materialize.toast('개인정보 수집약관에 동의해주세요.', 2000);
	}else if(v_userName.length<2) {
		Materialize.toast('이름을 입력해주세요.', 2000);
	}else if(v_userPhone.length<10 || v_userPhone.length>11) {
		Materialize.toast('전화번호를 확인해주세요.', 2000);
	}else {
		var smsHEAD = 'http://api.coolsms.co.kr/sendmsg?user=jeongyookgak&password=%200297a0d059a15401faaacd423e7a4491&enc=MD5&to=';
		var smsTAIL = '&from='+phone_from+'&text=';
		var sms_to=document.getElementById('new_phone').value;
		var sms_msg_start="[(주)정육각] 본인인증번호는 ";
		var sms_msg_end=" 입니다. 정확히 입력해주세요.";
		var smsAction = smsHEAD + sms_to + smsTAIL + sms_msg_start + createCertification() + sms_msg_end;
		
		var iframe = document.getElementById('smsFrame');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
		var sms_form=innerDoc.getElementById('sms_form');
		sms_form.action=smsAction;
		sms_form.submit();
		document.getElementById('certification_form').style.display = "block";
	}
}
function certification_resend() {
	var v_userPhone = document.getElementById('customer_phone').value;
	if(v_userPhone.length<10 || v_userPhone.length>11) {
		Materialize.toast('전화번호를 확인해주세요.', 2000);
	}else {
		var smsHEAD = 'http://api.coolsms.co.kr/sendmsg?user=jeongyookgak&password=%200297a0d059a15401faaacd423e7a4491&enc=MD5&to=';
		var smsTAIL = '&from='+phone_from+'&text=';
		var sms_to=document.getElementById('customer_phone').value;
		var sms_msg_start="[(주)정육각] 본인인증번호는 ";
		var sms_msg_end=" 입니다. 정확히 입력해주세요.";
		var smsAction = smsHEAD + sms_to + smsTAIL + sms_msg_start + createCertification() + sms_msg_end;
		
		var iframe = document.getElementById('smsFrame');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
		var sms_form=innerDoc.getElementById('sms_form');
		sms_form.action=smsAction;
		sms_form.submit();
		document.getElementById('certification_form').style.display = "block";
	}
}
function sms_send(v_userPhone,v_userMessage) {
	if(v_userPhone.length<10 || v_userPhone.length>11) {
		Materialize.toast('전화번호를 확인해주세요.', 2000);
	}else {
		var smsHEAD = 'http://api.coolsms.co.kr/sendmsg?user=jeongyookgak&password=%200297a0d059a15401faaacd423e7a4491&enc=MD5&to=';
		var smsTAIL = '&from=18000658&text=';
		var smsAction = smsHEAD + v_userPhone + smsTAIL + v_userMessage;
		
		var iframe = document.getElementById('smsFrame');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
		var sms_form=innerDoc.getElementById('sms_form');
		sms_form.action=smsAction;
		sms_form.submit();
	}
}
var certified_code='new';
function createCertification() {
	certified_code = Math.floor(Math.random() * (900000)) + 100000;
	return certified_code;
}
function certification_check() {
	if(certified_code=='new') {
		Materialize.toast('휴대전화번호를 인증해주세요.', 4000);
	}
	else if(certified_code == document.getElementById('new_phone_cert').value) {
		Materialize.toast('SMS 인증되었습니다.', 4000);
		signup_confirmed = true;
		move_signup_fb();
	}else {
		Materialize.toast('올바르지 않은 인증번호입니다.', 2000);
	}
}