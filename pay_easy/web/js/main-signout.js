function customerSignOutRequest() {
	customerSignOut();
	init();
}
function firebaseSignedCheck() {
		var exist = false;
		for (var i = 0; i < localStorage.length; i++)   {
			if(localStorage.key(i).indexOf('firebase:authUser')!=-1 ){
				if(localStorage.getItem(localStorage.key(i)).indexOf('apiKey')!=-1) {
					exist = true;
				}
			}
		}
		if(!exist){
			var firebaseIE = localStorage.getItem("FirebaseIE");
			if(firebaseIE!=null) {
				exist = true;
			}
		}
		return exist;
	}

function firebaseSignedID() {
	var uid;
	for (var i = 0; i < localStorage.length; i++)   {
		if(localStorage.key(i).indexOf('firebase:authUser')!=-1 ){
			var localVal = localStorage.getItem(localStorage.key(i));
			uid = localVal.substring(8,36);
			break;
		}
	}
	if(uid==null){
		var acustom = firebase.auth().currentUser;
		uid = acustom.uid;
	}
	if(uid==null){
		uid = localStorage.getItem("FirebaseIE");
	}
	return uid;
}
function firebaseSignedEmail() {
	var email;
	for (var i = 0; i < localStorage.length; i++)   {
		if(localStorage.key(i).indexOf('firebase:authUser')!=-1 ){
			var localVal = localStorage.getItem(localStorage.key(i));
			email = localVal.substring(82);
			var ind = email.indexOf(",");
			email = email.substring(0,ind-1);
			break;
		}
	}if(email==null) {
		var acustom = firebase.auth().currentUser;
		email = acustom.email;
		if(email==null){
			email='';
		}
	}
	return email;
}

function signin_failed(errorCode) {
	if(errorCode == 'auth/invalid-email') { alert("이메일주소가 올바르지 않습니다."); }
	else if(errorCode == 'auth/user-disabled') { alert("사용자 정보에 접근할 수 없습니다."); }
	else if(errorCode == 'auth/user-not-found') { alert("가입되지 않은 사용자입니다. 기존 고객님들께서는 새단장된 사이트에 새롭게 가입을 해주셔야 합니다."); }
	else if(errorCode == 'auth/wrong-password') { alert("비밀번호가 올바르지 않습니다."); }
	else{ alert("로그인 정보가 올바르지 않습니다."); }
}