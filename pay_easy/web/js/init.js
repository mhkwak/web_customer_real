(function($){
  $(function(){

    $('.button-collapse').sideNav();
	
	$(document).ready(function() {
    	//$('#product_option').material_select();
		$('.slider').slider('pause');
		// Start slider
		$('.slider').slider('start');
		// Next slide
		$('.slider').slider('next');
		// Previous slide
		$('.slider').slider('prev');
		$('.slider').slider({full_width: true});
		
		$('select').material_select();
		
		
		$('.modal-trigger').leanModal();
		$("#privacy").load("privacy.html"); 
		$("#usage").load("usage.html"); 
		//$('#about-us').load("http://corporate.kurly.com/main.html");
		$('.tooltipped').tooltip({delay: 50});
        
		
		$('.goods').hover(function(){
				$(this).css('outline','#a1887f solid 1px');
			},function(){
				$(this).css('outline','#a1887f solid 0px');
			});
		
		$('#agree_all').change(function() {
			if($(this).is(":checked")) {
				$('#agree_afe').attr("checked", true);;
				$('#agree_usage').attr("checked", true);;
				$('#agree_privacy').attr("checked", true);;
        	}else {
				$('#agree_afe').attr("checked", false);;
				$('#agree_usage').attr("checked", false);;
				$('#agree_privacy').attr("checked", false);;
			}
    	});
  	});
	
	

  }); // end of document ready
})(jQuery); // end of jQuery name space