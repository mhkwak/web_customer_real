// JavaScript Document
var config = {
	apiKey: "AIzaSyAlGHNNIvQRpxl0mDwZ_c2DOHIg0Fufc-0",
	authDomain: "jyg-custom.firebaseapp.com",
	databaseURL: "https://jyg-custom.firebaseio.com",
	storageBucket: "jyg-custom.appspot.com",
	messagingSenderId: "222150107123"
};
firebase.initializeApp(config);
var customer = firebase.auth().currentUser;
var database = firebase.database();
var customers;
var customerParse = new Array();
var customersCount;
var cartParse = new Array();
var cartCount;
var orderParse = new Array();
var orderCount;
var orderList = new Array();
var orderListCount;
var lastUpdate;
function getLastUpdate() {
	return lastUpdate;
}
function init() {
	console.log("init1");
	var v_userId;
	var v_userPw;
	v_userId = document.getElementById('v_userId').value;
	v_userPw = document.getElementById('v_userPw').value;
	v_userId = "factory_price@yookgak.com";
	v_userPw = "02030203";
	console.log("init");

	firebase.auth().signInWithEmailAndPassword(v_userId, v_userPw).then(function(user) {
		user.getToken().then(function(token) {
			document.getElementById('v_signIn').style.display = 'none';
	});
	}).catch(function(error) {
		var errorCode = error.code; var errorMessage = error.message;
		alert(errorCode + errorMessage);
	});
}
var firebase_flag = false;
firebase.auth().onAuthStateChanged(function(user) {
		if (user) {
			console.log("yay1");
			if(firebase_flag==false) {
				var isAnonymous = user.isAnonymous;
				var user_id = user.uid;
				firebase_flag = true;
				console.log("yay2" + user_id);
				if(user_id=='q4gHxuEuFNRBAcrexywejBBRHsI2') {
					initAdmin();
				}else {
					signOut();
				}
			}
		} else {
			if(firebase_flag==true) {
				firebase_flag = false;
			}
		}
	});
function initAdmin() {
	console.log("yay");
	lastUpdate = new Date().toString();
	initViewFlag();
	initCategory();
	initSum();
	waitingFirebase();
	pager('hall');
}
function waitingFirebase() {
	console.log("waitingFirebase");
	firebase.database().ref('customers').on('value', function(snapshot) {
		initSum();
		customers = snapshot.val();
		lastUpdate = new Date().toString();
		readFirebaseData();
	});
}
function readFirebaseData() {
	initCategory();
	console.log("readFirebaseDate");
	customersCount = 0;
	orderListCount = 0;
	$.each(customers, function(uid, customerValue){
		customerParse[customersCount] = {};
		customerParse[customersCount].indexo = customersCount;
		customerParse[customersCount].uid = uid;
		customerParse[customersCount].username = customerValue.username;
		customerParse[customersCount].useremail = customerValue.useremail;
		customerParse[customersCount].userphone = customerValue.userphone;
		//console.log(customerParse[customersCount].uid);
		if(customerValue.rewards.points_history!=null){
		customerParse[customersCount].userstamp = customerValue.rewards.points_history[0].timestamp;
		}else {customerParse[customersCount].userstamp = "";}
		readCartData(customerValue,customersCount);
		readOrderData(uid,customerValue,customersCount);
		customersCount++;
	});
	console.log(customersCount);
	print_orderList();
	print_userList();
	print_summary();
	print_realtime();
}
function orderSort(a, b) {
	if(a.userstamp === b.userstamp){
		if(a.indexo === b.indexo){ return 0; }
		return  a.indexo < b.indexo ? 1 : -1;
	} return  a.userstamp < b.userstamp ? 1 : -1;
}
function orderSortR(a, b) {
	if(a.userstamp === b.userstamp){
		if(a.indexo === b.indexo){ return 0; }
		return  a.indexo > b.indexo ? 1 : -1;
	} return  a.userstamp > b.userstamp ? 1 : -1;
}
function nameSort(a, b) {
	if(a.username === b.username){
		if(a.useremail === b.useremail){ return 0; }
		return  a.useremail > b.useremail ? 1 : -1;
	} return  a.username > b.username ? 1 : -1;
}

function orderListSort(a, b) {
	if(a.shippingTime === b.shippingTime){
		if(a.historyTime === b.historyTime){ return 0; }
		return  a.historyTime < b.historyTime ? 1 : -1;
	} return  a.shippingTime < b.shippingTime ? 1 : -1;
}
function readCartData(customerValue,customersIndex) {
	cartCount = 0;
	var productTypes = 0;
	var pushError = false;
	cartParse[customersIndex] = {};
	cartParse[customersIndex].productKey = new Array();
	cartParse[customersIndex].productOption = new Array();
	cartParse[customersIndex].productQuantity = new Array();
	$.each(customerValue.carts, function(productKey, productValue){
		if(productKey=='current') {
			if(productValue.customer_name!=null){
				pushError = true;
			}
		}else {
			$.each(productValue, function(productOption, detail){
				if(detail!=null){
					cartCount = cartCount + Number(detail.count);
					cartParse[customersIndex].productKey[productTypes] = productKey;
					cartParse[customersIndex].productOption[productTypes] = productOption;
					cartParse[customersIndex].productQuantity[productTypes] = detail.count;
					productTypes++;
				}else {
					console.log("error productOption: " + productOption);
				}
			});
		}
	});
	cartParse[customersIndex].pushError = pushError;
	cartParse[customersIndex].productTypes = productTypes;
	cartParse[customersIndex].currentCartCount = cartCount;
}
function readOrderData(uid,customerValue,customersIndex) {
	orderCount = 0;
	orderParse[customersIndex] = {};
	orderParse[customersIndex].order_records = customerValue.order_records;
	orderParse[customersIndex].orderCount = customerValue.order_records.current.count;
	var historyCountA = 0;
	var historyCountB = 0;
	var historyCountC = 0;
	var goodsFlag = false;
	var goodsCount = 0;
	$.each(customerValue.order_records, function(orderKey, orderValue){
		if(orderKey=='current') {
		}else {
			orderList[orderListCount] = {};
			orderList[orderListCount].customer = {};
			orderList[orderListCount].customer.name = customerValue.username;
			orderList[orderListCount].customer.email = customerValue.useremail;
			orderList[orderListCount].customer.phone = customerValue.userphone;
			orderList[orderListCount].customer.uid = uid;
			orderList[orderListCount].histories = orderValue.histories;
			orderList[orderListCount].goods = orderValue.goods;
			orderList[orderListCount].informations = orderValue.informations;
			orderList[orderListCount].payment = orderValue.payment;
			orderList[orderListCount].totalPrice = orderValue.totalPrice;
			orderList[orderListCount].historyTime = orderValue.histories[0].timestamp;
			orderList[orderListCount].shippingTime = orderValue.informations.shippingDate;
			var dayFlag = isToday(orderList[orderListCount].shippingTime);
			var historyFlag = 'none';
			$.each(orderValue.histories, function(historyKey, historyValue){
				if(historyValue!=null){
					if(historyValue.status=="delivered") {
						historyFlag = 'complete';
					}
					else if(historyFlag=='none') {
						if(historyValue.status!="ordered" && historyValue.status!="waiting") {
							historyFlag = 'current';
						}
					}
				}
			});
			if(historyFlag=='complete') { historyCountC++; }
			else if(historyFlag=='current') { historyCountB++; }
			else {historyCountA++;}
			var goodsCC = 0;
			$.each(orderValue.goods, function(goodsKey, goodsValue){
				if(goodsValue.productWeight==-1) { goodsFlag = true; }
				if(!historyFlag){ goodsCount++; }
				if(dayFlag){ addSum(goodsValue.productID,1); }
				addSumC(goodsValue.productID,goodsValue.productOption,1,orderList[orderListCount].historyTime,orderList[orderListCount].shippingTime);
				goodsCC ++;
			});
			orderList[orderListCount].goodsCC = goodsCC;
			orderListCount++;
		}
	});
	orderParse[customersIndex].activeCount = historyCountA;
	orderParse[customersIndex].currentCount = historyCountB;
	orderParse[customersIndex].completeCount = historyCountC;
	orderParse[customersIndex].activeGoodsCount = goodsCount;
	orderParse[customersIndex].unweighted = goodsFlag;
}
function compareTime(flag,shippingTime) {
	var return_Flag = true;
	var todays = new Date();
	todays.setDate(todays.getDate()+1);
	var tyear = todays.getFullYear();
	var tmonth = todays.getMonth()+1;
	if(tmonth<10) {tmonth = '0'+tmonth;}
	var tday = todays.getDate();
	if(tday<10) {tday = '0'+tday;}
	todays = tyear+'-'+tmonth+'-'+tday;
	shippingTime = shippingTime.substring(0,10);
	if(todays==shippingTime){
		if(flag==0) {return_Flag =  true;}
		else if(flag==1) {return_Flag =  true;}
		else {return_Flag =  true;}
	}
	else if(todays<shippingTime){
		if(flag==0) {return_Flag =  false;}
		else if(flag==1) {return_Flag =  true;}
		else {return return_Flag = true;}
	}else {
		if(flag==0) {return_Flag = false;}
		else if(flag==1) {return_Flag = false;}
		else {return_Flag = true;}
	}
	return return_Flag;
}
function isToday(shippingTime) {
	var todays = new Date();
	todays.setDate(todays.getDate()+1);
	var tyear = todays.getFullYear();
	var tmonth = todays.getMonth()+1;
	if(tmonth<10) {tmonth = '0'+tmonth;}
	var tday = todays.getDate();
	if(tday<10) {tday = '0'+tday;}
	todays = tyear+'-'+tmonth+'-'+tday;
	shippingTime = shippingTime.substring(0,10);
	if(todays==shippingTime){return true;}
	return false;
}
function print_summary() {	
	var cardHeadTop = '<thead><tr>';
	var cardHeadMid = '<td>상품</td>';
	var cardHeadBot = '</tr></thead>';
	var tTime = new Date().getHours();
	for(var i=9;i<17;i++) {
		var ttt=i;
		if(i<10) {ttt='0'+i;}
		if(i==tTime){cardHeadMid = cardHeadMid + '<td style="background:rgb(150,200,200); text-align:center">'+ttt+'시</td>';}
		else {cardHeadMid = cardHeadMid + '<td style="text-align:center">'+ttt+'시</td>';}
	}cardHeadMid = cardHeadMid + '<td style="text-align:center">합계</td>';
	var cardBody = '';
	var sum_table = document.getElementById('sum_table');
	
	var cardBodyTop = '<tbody>';
	var cardBodyMid = '';
	for(var j=0;j<getGoodsListCount();j++) {
		var pName = getName(j);
		var pKor = getKor(j);
		cardBodyMid = cardBodyMid + '<tr>'+'<td>'+pKor+'</td>';
		for(var i=9;i<17;i++) {
			var printValueC = getSumC(pName,i);
			if(printValueC==0) {printValueC = '.';}
			if(i==tTime){cardBodyMid = cardBodyMid + '<td style="background:rgb(150,200,200); text-align:center">'+printValueC+'</td>';}
			else {cardBodyMid = cardBodyMid + '<td style="text-align:center">'+printValueC+'</td>';}
		}
		cardBodyMid = cardBodyMid + '<td style="font-size:24px; color:#FF6D00; text-align:center">'+getSum(pName)+'</td>';
		cardBodyMid = cardBodyMid + '</tr>';
	}

	var cardBodyBot = '</tbody>';
	cardBody = cardBodyTop + cardBodyMid + cardBodyBot;
	sum_table.innerHTML = cardHeadTop+cardHeadMid+cardHeadBot+cardBody;
}
function print_realtime() {
	var realtime_table = document.getElementById('realtime_table');
	var cardHeadTop = '<thead><tr>';
	var cardHeadMid = '<td>상품</td><td>옵션 1</td><td>옵션 2</td><td>옵션 3</td><td>옵션 4</td><td>옵션 5</td>';
	var cardHeadBot = '</tr></thead>';
	var cardBodyTop = '<tbody>';
	var cardBody = '';
	var cardBodyBot = '</tbody>';
	
	for(var i=0;i<4;i++) {
		cardBody = cardBody + '<tr>';
		cardBody = cardBody + '<td>' + getGoodsCategoryName(i) + '</td>';
		for(var j=0;j<5;j++) {
			var ddd = getGoodsCategory(i,j);
			cardBody = cardBody + '<td>' + ddd + '</td>';
		}
		cardBody = cardBody + '</tr>';
	}
	
	realtime_table.innerHTML = cardHeadTop+cardHeadMid+cardHeadBot+cardBodyTop+cardBody+cardBodyBot;
}
function print_orderList() {
	orderList.sort(orderListSort);
	var order_list_table = document.getElementById('order_list_table');
	var tableDate = '';
	if(checkOrderFlag('view')==0) {tableDate = '<td onClick="bttn(0)" class="center-align pointer">도착희망<br><span class="red-text">내일</span></td>';}
	else if(checkOrderFlag('view')==1) {tableDate = '<td onClick="bttn(0)" class="center-align pointer">도착희망<br><span class="blue-text">내일 및 이후</span></td>';}
	else {tableDate = '<td onClick="bttn(0)" class="center-align pointer">도착희망<br><span>전체</span></td>';}
	var tableHead='<thead><tr><td>주문시간</td><td>구매자정보</td><td>받는사람정보</td>'+tableDate+'<td>주문구성</td></tr></thead>';
	var tableBodyStart = '<tbody>';
	var tableBody = '';
	var tableBodyEnd = '</tbody>';
	
	for(var i=0;i<orderListCount;i++) {
		var viewQ = true;
		var viewF = checkOrderFlag('view');
		if(!compareTime(viewF,orderList[i].shippingTime)) {viewQ=false;}
		if(viewQ) {
			if(isToday(orderList[i].shippingTime)){
				tableBody = tableBody + '<tr style="cursor:pointer; background:rgb(245,206,162)" onClick="viewDetail('+i+')">';
			}else {tableBody = tableBody + '<tr style="cursor:pointer" onClick="viewDetail('+i+')">';}
			if(orderList[i].histories[0].timestamp==null){
				tableBody = tableBody + '<td>' + '예전주문' + '<br>';
			}else {
				tableBody = tableBody + '<td>' + orderList[i].histories[0].timestamp.substring(0,16) + '<br>';
			}
			tableBody = tableBody + '' + orderList[i].customer.uid.substring(0,10)+'<br>' + orderList[i].customer.uid.substring(11) + '</td>';
			tableBody = tableBody + '<td>' + orderList[i].customer.name + ' ';
			tableBody = tableBody + '' + ViewingPhone(orderList[i].customer.phone) + '<br>';
			tableBody = tableBody + '' + orderList[i].customer.email + '</td>';
			tableBody = tableBody + '<td>' + orderList[i].informations.name + ' ';
			tableBody = tableBody + '' + ViewingPhone(orderList[i].informations.phone) + '<br>';
			tableBody = tableBody + 'PO: ' + orderList[i].informations.postcode + '<br>';
			tableBody = tableBody + '' + orderList[i].informations.address + '<br>';
			tableBody = tableBody + '' + orderList[i].informations.room + '</td>';
			tableBody = tableBody + '<td>' + orderList[i].informations.shippingDate + '</td>';
			tableBody = tableBody + '<td class="center-align">' + orderList[i].goodsCC + '</td>';
			tableBody = tableBody + '</tr>';
		}
	}
	
	order_list_table.innerHTML = tableHead + tableBodyStart + tableBody + tableBodyEnd;
}
function print_userList() {
	if(checkViewFlag("name")) {customerParse.sort(nameSort);}
	else if(checkViewFlag("date")) {customerParse.sort(orderSortR);}
	else {customerParse.sort(orderSort);}
	var user_list_table = document.getElementById('user_list_table');
	var tableDate;
	var tableName;
	var tableCart;
	var tableActive;
	var tableCurrent;
	var tableComplete;
	if(checkViewFlag('date')){tableDate = '<td style="background:#D5D5D5" id="radio_date" onClick="radio(200)">UID&ID (날짜▴)</td>';}
	else {tableDate = '<td id="radio_date" onClick="radio(200)">UID&ID (날짜▼)</td>';}
	if(checkViewFlag('name')){tableName = '<td style="background:#D5D5D5" id="radio_name" onClick="radio(100)">이름 ▴</td>';}
	else {tableName = '<td id="radio_name" onClick="radio(100)">이름</td>';}
	if(checkViewFlag('cart')){tableCart = '<td style="background:#D5D5D5" id="radio_cart" onClick="radio(0)">장바구니</td>';}
	else {tableCart = '<td id="radio_cart" onClick="radio(0)">장바구니</td>';}
	if(checkViewFlag('active')){tableActive = '<td style="background:#D5D5D5" id="radio_active" onClick="radio(1)">활성</td>';}
	else {tableActive = '<td id="radio_active" onClick="radio(1)">활성</td>';}
	if(checkViewFlag('current')){tableCurrent = '<td style="background:#D5D5D5" id="radio_current" onClick="radio(2)">진행</td>';}
	else {tableCurrent = '<td id="radio_current" onClick="radio(2)">진행</td>';}
	if(checkViewFlag('complete')){tableComplete = '<td style="background:#D5D5D5" id="radio_complete" onClick="radio(3)">완료</td>';}
	else {tableComplete = '<td id="radio_complete" onClick="radio(3)">완료</td>';}
	var tableHead = '<thead><tr><td>title</td><td id="thead_count"></td><td colspan="4">필터</td></tr>'+
		tableDate+tableName+tableCart+tableActive+tableCurrent+tableComplete;
	var tableBodyStart = '<tbody>';
	var tableBody = '';
	var tableBodyEnd = '</tbody>';
	for(var i=0;i<customersCount;i++) {
		var viewQ = true;
		if(checkViewFlag('cart')) {
			if(cartParse[customerParse[i].indexo].currentCartCount==0 && !cartParse[customerParse[i].indexo].pushError){viewQ = false;}
		}if(checkViewFlag('active')) {
			if(orderParse[customerParse[i].indexo].activeCount==0){viewQ = false;}
		}if(checkViewFlag('current')) {
			if(orderParse[customerParse[i].indexo].currentCount==0){viewQ = false;}
		}if(checkViewFlag('complete')) {
			if(orderParse[customerParse[i].indexo].completeCount==0){viewQ = false;}
		}
		if(viewQ){
			tableBody = tableBody + '<tr>';
			tableBody = tableBody + '<td>' + customerParse[i].userstamp + '<br>';
			tableBody = tableBody + '' + customerParse[i].uid + '<br>';
			tableBody = tableBody + '' + customerParse[i].useremail + '</td>';
			tableBody = tableBody + '<td>' + customerParse[i].username + '<br>';
			tableBody = tableBody + '' + ViewingPhone(customerParse[i].userphone) + '</td>';

			tableBody = tableBody + ViewingCartCount(cartParse[customerParse[i].indexo].currentCartCount,cartParse[customerParse[i].indexo].pushError);
			tableBody = tableBody + ViewingGoodsCount(orderParse[customerParse[i].indexo]);
			tableBody = tableBody + '</tr>';
		}
	}
	user_list_table.innerHTML = tableHead + tableBodyStart + tableBody + tableBodyEnd;
	if(document.getElementById('thead_count')!=null) {
		document.getElementById('thead_count').innerHTML = '회원수: ' + customersCount;
	}
}




function ViewingPhone(tel){
	var return_tel;
	if(tel.indexOf('-')!=-1) {
		return_tel = tel;
	}else {
		return_tel = tel.substring(0,3)+'-'+tel.substring(3,7)+'-'+tel.substring(7,11);
	}
	return return_tel;
}
function ViewingCartCount(count,pushError){
	var return_count;
	if(count==0) {
		return_count = '<td style="font-size:20px">-';
	}else {
		return_count = '<td class="red-text" style="font-size:20px">'+count;
	}
	if(pushError){
		return_count = return_count + '<span class="red-text">오류</span>';
	}else {
		return_count = return_count + '</td>';
	}
	return return_count;
}
function ViewingGoodsCount(ord){
	var active = ord.activeCount;
	var current = ord.currentCount;
	var complete = ord.completeCount;
	var return_count='<td style="font-size:20px" class="red=text">';
	if(active>0){ return_count = return_count + active+'</td>';}
	else { return_count = return_count + '-</td>'; }
	
	return_count = return_count + '<td style="font-size:20px" class="blue-text">';
	if(current>0){ return_count = return_count + current+'</td>';}
	else { return_count = return_count + '-</td>'; }
	
	return_count = return_count + '<td style="font-size:20px">';
	if(complete>0){ return_count = return_count + complete+'</td>';}
	else { return_count = return_count + '-</td>'; }
	
	return return_count;
}